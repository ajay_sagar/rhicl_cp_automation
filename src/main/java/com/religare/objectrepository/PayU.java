package com.religare.objectrepository;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class PayU {

	WebDriverCommonLib clib = new WebDriverCommonLib();

	@FindBy(name = "ccard_number")
	WebElement cardNumber;

	@FindBy(name = "cname_on_card")
	WebElement nameOnCard;

	@FindBy(name = "ccvv_number")
	WebElement ccvvNumber;

	@FindBy(id = "cexpiry_date_month")
	WebElement cardExpiryMonthDropDown;

	@FindBy(id = "cexpiry_date_year")
	WebElement cardExpiryYearDropDown;

	@FindBy(name = "pay_button")
	WebElement payBtn;

	ExcelLib elib = new ExcelLib();
	Logger log = Logger.getLogger("devpinoyLogger");

	// public void gateway(String srcname,String destname, String srcname1,String
	// destname1 ) throws Throwable {
	public void gateway() throws Throwable {
		try {

			cardNumber.sendKeys("5123456789012346");
			log.debug("Entering the credit card number");
			nameOnCard.sendKeys("test");
			log.debug("Entering the Card Name ");
			System.out.println("Entering the credit card number");
			nameOnCard.sendKeys("test");
			log.debug("Entering the Card Name ");
			System.out.println("Entering the Card Name ");
			ccvvNumber.sendKeys("123");
			log.debug("Entering the CVV Number ");
			System.out.println("Entering the CVV Number ");
			clib.selectByValue(cardExpiryMonthDropDown, "03");
			log.debug("Selecting the Month drop");
			System.out.println("Selecting the Month drop");
			clib.selectByValue(cardExpiryYearDropDown, "2023");
			log.debug("Selecting the Year drop");
			System.out.println("Selecting the Year drop");
		} catch (Exception e) {

			e.getMessage();
		}
		try {

			ccvvNumber.sendKeys("123");
			log.debug("Entering the CVV Number ");
			System.out.println("Entering the CVV Number ");
		} catch (Exception e) {

			e.getMessage();

		}
		Thread.sleep(4000);

		//clib.Passscreenshot(srcname, destname);
		Thread.sleep(15000);
		
	}
		
		public void thankyou() {
		payBtn.click();
		log.debug("Clicking on the pay button ");
		System.out.println("Clicking on the pay button ");
		}
	}
