package com.religare.objectrepository;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class GmailConfig {
	JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;

	WebDriverCommonLib clib = new WebDriverCommonLib();
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();

	@FindBy(xpath = "//div[@class='gmail-nav__nav-links-wrap']/a[2]")
	WebElement signin;

	@FindBy(id = "identifierId")
	WebElement emailid;

	@FindBy(xpath = "//div[@class='qhFLie']/div")
	WebElement Next;

	@FindBy(xpath = "(//div[@class='Xb9hP'])[1]/input")
	WebElement Password;

	@FindBy(xpath = "(//div[@class='xT'])[1]/div/span[1]")
	WebElement firstemail;

	@FindBy(xpath = "//div[@data-tooltip='Show trimmed content']")
	WebElement trimmedcontent;

	@FindBy(xpath = "//div[@class='im']/div/table/tbody/tr[3]/td/table/tbody/tr[5]/td/a")
	WebElement buynow;

	public void gmaillogin() throws Throwable {

		String str = BaseClass.driver.getWindowHandle();
		//BaseClass.driver.get("https://www.google.com/gmail/about/");
		BaseClass.driver.navigate().to("https://www.google.com/gmail/about/");
		signin.click();
		String email = elib.getExcelData("MedicalHistory", 14, 1);
		String password = elib.getExcelData("MedicalHistory", 15, 1);
		emailid.click();
		emailid.sendKeys(email);
		log.debug("Entering email id is " + email );
		Next.click();
		Thread.sleep(10000);
		Password.click();
		Password.sendKeys(password);
		log.debug("Entering password is " + password);
		Next.click();

	}

	public void verifymail() throws InterruptedException {
		clib.waitForPageToLoad();
		firstemail.click();
		trimmedcontent.click();
		 String str1 = BaseClass.driver.getWindowHandle();
	        System.out.println(str1);
		jse.executeScript("arguments[0].scrollIntoView();", buynow);
		buynow.click();
		log.debug("Clicking buy Now button in mail");
		clib.waitForPageToLoad();
		Thread.sleep(6000);
        
	}

}
