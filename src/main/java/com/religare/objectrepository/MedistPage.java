package com.religare.objectrepository;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;

import com.religare.genericlib.WebDriverCommonLib;

public class MedistPage {
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();
	WebDriverCommonLib clib = new WebDriverCommonLib();
	JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;

	@FindBy(id = "label-id1-0")
	WebElement option1;

	@FindBy(id = "label-id2-0")
	WebElement option2;

	@FindBy(id = "label-id3-0")
	WebElement option3;

	@FindBy(id = "label-id4-0")
	WebElement option4;

	@FindBy(id = "label-id5-0")
	WebElement option5;

	@FindBy(id = "label-id6-0")
	WebElement option6;

	@FindBy(id = "label-id7-0")
	WebElement option7;

	@FindBy(id = "label-id8-0")
	WebElement option8;

	@FindBy(id = "label-id9-0")
	WebElement option9;

	@FindBy(id = "label-id10-0")
	WebElement option10;

	@FindBy(id = "label-id11-0")
	WebElement option11;

	@FindBy(id = "label-id12-0")
	WebElement option12;

	@FindBy(id = "validTermCondition-1")
	WebElement validTermCondition;

	@FindBy(id = "SICREDITCARD")
	WebElement SICREDITCARD;

	@FindBy(id = "submit_medical_history")
	WebElement submitbutton;

	@FindBy(id = "submit_medical_history1")
	WebElement caresubmitbutton;

	@FindBy(id = "enhancemedical")
	WebElement enhancesubmitbutton;

	@FindBy(id = "joytomorrowmedical")
	WebElement submitjoytomorrow;

	@FindBy(id = "joymedical")
	WebElement submitjoytoday;

	@FindBy(id = "label-id2-1")
	WebElement option2Yesexplore;

	@FindBy(id = "medicalhistory")
	WebElement submitsecurebutton;

	@FindBy(id = "TripStartIndia")
	WebElement TripStartIndia;

	@FindBy(id = "ASU")
	WebElement ASU;

	@FindBy(id = "studentmedical")
	WebElement submitstudentexplorebutton;

	@FindBy(id = "validTermCondition-1")
	WebElement disclaimer1AssurecheckBox;

	@FindBy(id = "recivedSms")
	WebElement receiveSmsCheckBoxAssure;

	@FindBy(id = "SICREDITCARD")
	WebElement sicretCardChheckBOxAssure;

	@FindBy(xpath = "//h3[contains(text(), 'Health Questionnaire')]")
	WebElement healthQuestionarrieText;

	@FindBy(xpath = "//div[@class='middleContainerIn']/h3")
	WebElement lifeStyelQuestionsText;

	/*
	 * @FindBy(id = "studentmedical") WebElement submitstudentexplorebutton;
	 */
	public void careMedicalhistory() throws IOException, InterruptedException {

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-500);");

		String prm2 = BaseClass.driver.findElement(By.id("enhancePremiumResultOne")).getAttribute("innerHTML");

		// log.debug("The Enhance Premium Resultone on Screen3 is " + prm2);

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");

		option1.click();

		option2.click();

		try {
			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {

			// System.out.println(e.getMessage());

		}
		option3.click();

		option4.click();

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");
		log.debug("Selecting the options");
		validTermCondition.click();
		log.debug("Enabling the valid terms and conditions");
		SICREDITCARD.click();
		log.debug("Enabling the Sicreditcard option");
		// clib.Passscreenshot("CareMedical", "CareMedicalPage");

	/*	caresubmitbutton.click();
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);*/
		// clib.Passscreenshot("CareProposalSummary", "CareProposalSummaryPage");

	}
	
	public void careProposalsummary() throws InterruptedException {
		
		caresubmitbutton.click();
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);
		
		
		
	}

	public void carefreedomMedicalhistory() throws IOException, InterruptedException {

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
		option1.click();

		option2.click();

		try {

			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
		option3.click();

		option4.click();
		log.debug("Selecting the options");
		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");
		validTermCondition.click();
		log.debug("Enabling the valid terms and conditions");
		SICREDITCARD.click();
		log.debug("Enabling the Sicreditcard option");
		// clib.Passscreenshot("CareFreedomMedical", "CareFreedomMedicalPage");

		/*
		 * submitbutton.click(); log.debug("Clicking on  the submitbutton");
		 * Thread.sleep(5000);
		 */
		// clib.Passscreenshot("CareFreedomProposalSummary",
		// "CareFreedomProposalSummaryPage");

	}

	public void carefreedomProposalsummary() throws InterruptedException {

		submitbutton.click();
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);

	}

	public void assureMedicalhistory() throws IOException, InterruptedException {

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-250);");

		// String prm2 =
		// BaseClass.driver.findElement(By.id("assurecare")).getAttribute("innerHTML");
		// log.debug("The Assure Premium on Screen3 is " + prm2);
	//	((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
		jse.executeScript("arguments[0].scrollIntoView(true);", option1);
		option1.click();
		option2.click();
		try {
			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
		option3.click();

		option4.click();
		try {
			BaseClass.driver.findElement(By.id("label-subid1-0")).click();

			BaseClass.driver.findElement(By.id("label-subid2-0")).click();

			BaseClass.driver.findElement(By.id("label-subid3-0")).click();

			BaseClass.driver.findElement(By.id("label-subid4-0")).click();

			Thread.sleep(1500);

			BaseClass.driver.findElement(By.id("label-subid5-0")).click();

			Thread.sleep(1500);

			// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
			BaseClass.driver.findElement(By.id("label-subid6-0")).click();

			Thread.sleep(1500);

			BaseClass.driver.findElement(By.id("label-subid7-0")).click();

			Thread.sleep(1500);

			BaseClass.driver.findElement(By.id("label-subid8-0")).click();

			Thread.sleep(1500);

			BaseClass.driver.findElement(By.id("label-subid9-0")).click();

			try {

				BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

			} catch (Exception e) {
				// System.out.println(e.getMessage());
			}
		} catch (Exception e) {
		}
		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
		// option5.click();
		option6.click();

		option7.click();

		option8.click();

		option9.click();

		try {
			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
		option10.click();

		option11.click();

		option12.click();
		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,1000);");

		BaseClass.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		log.debug("Selecting the options");
		validTermCondition.click();
		log.debug("Enabling the valid terms and conditions");
		SICREDITCARD.click();
		// clib.Passscreenshot("AssureMedical", "AssureMedicalHistoryPage");
		log.debug("Enabling the SICREDITCARD option ");

	}

	public void assuremedicalhistorysubmit() throws InterruptedException {
		jse.executeScript("arguments[0].scrollIntoView(true);", submitbutton);
		submitbutton.click();
		// clib.Passscreenshot("AssureProposalSummary", "AssureProposalSummaryPage");
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);

	}

	public void enchanceMedicalHistory() throws Throwable {

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-250);");

		String prm2 = BaseClass.driver.findElement(By.id("enhancePremiumResultOne")).getAttribute("innerHTML");
		// log.debug("The Enhance Premium Resultone on Screen3 is " + prm2);

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,500);");

		Thread.sleep(2000);

		option1.click();

		option2.click();

		try {

			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {

			// System.out.println(e.getMessage());
		}
		option3.click();

		option4.click();

		option5.click();
		log.debug("Selecting the options");
		validTermCondition.click();
		//clib.Passscreenshot("EnhanceMedicalhistory", "EnhanceMedicalhistoryPage");
		log.debug("Enabling the valid terms and conditions");
		SICREDITCARD.click();
		//clib.Passscreenshot("EnhanceMedicalhistory", "EnhanceMedicalhistoryPage");
		log.debug("Enabling the Sicreditcard option");

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");

//		enhancesubmitbutton.click();
//		clib.Passscreenshot("EnhanceProposalSummary", "EnhanceProposalSummaryPage");
//		log.debug("Clicking on  the submitbutton");
//		Thread.sleep(5000);

	}
	
	public void enhanceProposalsummary() throws InterruptedException {
		
		enhancesubmitbutton.click();
		//clib.Passscreenshot("EnhanceProposalSummary", "EnhanceProposalSummaryPage");
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);

		
		
	}

	public void joyMedicalHistory() throws Throwable {
		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-250);");
		// String prm2 =
		// BaseClass.driver.findElement(By.id("joyprimiumone")).getAttribute("innerHTML");
		// log.debug("The Joy Premium on Screen3 is " + prm2);

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");

		option1.click();

		option2.click();

		try {

			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {

			// System.out.println(e.getMessage());
		}
		option3.click();

		option4.click();

		option5.click();

		Thread.sleep(1000);

		option6.click();

		option7.click();

		option8.click();

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");

		Thread.sleep(1500);
		log.debug("Selecting the options");
		validTermCondition.click();
		log.debug("Enabling the valid terms and conditions");
		SICREDITCARD.click();
		//clib.Passscreenshot("JoymedicalHistory", "JoymedicalHistoryPage");
		log.debug("Enabling the Sicreditcard option");
		
	}
		
		public void joyProposalsummary() {
			
			
		try {

			submitjoytomorrow.click();

		} catch (Exception e) {
		}
		try {

			submitjoytoday.click();
			Thread.sleep(5000);
		//	clib.Passscreenshot("JoyProposalSummary", "JoyProposalSummaryPage");

		} catch (Exception e) {

		}
		log.debug("Clicking on  the submitbutton");
	}

	public void studenteEploreMedicalHistory() throws Throwable {

		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-350);");
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", option1);

		Thread.sleep(3000);

		String prmyh = BaseClass.driver.findElement(By.id("enhancePremiumResultRroposalOne")).getAttribute("innerHTML");

		// log.debug("The Enhance Premium Resultone on Screen2 is " + prmyh);

		option1.click();

		option2.click();
		try {

			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {

			// System.out.println(e.getMessage());
		}
		try {

			option5.click();

			option6.click();

			option7.click();

			option8.click();

		} catch (Exception e) {
		}
		// log.debug("Selecting the options" );
		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");

		String a = "Yes";

		String Ped = elib.getExcelData("StudentExplorepolicyplan", 6, 1);

		if (a.equals(Ped)) {

			BaseClass.driver.findElement(By.id("label-id1-1")).click();

			BaseClass.driver.findElement(By.xpath("//*[@id=\"id1\"]/div[7]/div/div[2]/div/label")).click();

		} else {

			BaseClass.driver.findElement(By.id("label-id1-0")).click();

		}
		option2.click();
		log.debug("Selecting the options");
		validTermCondition.click();
		log.debug("Enabling the valid terms and conditions");
		TripStartIndia.click();

		ASU.click();
		log.debug("Enabling the Sicreditcard option");

		//clib.Passscreenshot("StudentExploreMedical", "StudentExploreMedicalPage");
		/*submitstudentexplorebutton.click();
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);*/
		//clib.Passscreenshot("StudentExploreProposalSummary", "StudentExploreProposalSummaryPage");

	}
	
	public void studentexploreProposalsummary() throws InterruptedException {
		
		submitstudentexplorebutton.click();
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);
	}

	public void secureMedicalHistory() throws InterruptedException, IOException {

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-250);");

		String prm2 = BaseClass.driver.findElement(By.id("secure_premium_val")).getAttribute("innerHTML");

		// log.debug("The Secure Premium on Screen3 is " + prm2);

		option1.click();

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,500);");

		option2.click();

		try {

			BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

		} catch (Exception e) {

			// System.out.println(e.getMessage());
		}
		option3.click();

		option4.click();

		option5.click();

		option6.click();
		log.debug("Selecting the options");
		validTermCondition.click();
		log.debug("Enabling the valid terms and conditions");
		SICREDITCARD.click();
		//clib.Passscreenshot("SecureMedicalHistory", "SecureMedicalHistoryPage");
		log.debug("Enabling the Sicreditcard option");
		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");

//		Thread.sleep(1500);
//
//		submitsecurebutton.click();
//		log.debug("Clicking on  the submitbutton");
//		Thread.sleep(5000);
//		//clib.Passscreenshot("SecureProposalSummary", "SecureProposalSummaryPage");

	}
	
	public void secureProposalsummary() throws InterruptedException {
		
		Thread.sleep(1500);

		submitsecurebutton.click();
		log.debug("Clicking on  the submitbutton");
		Thread.sleep(5000);
		
		
	}

	public void getCurrentURLProposalSummaryAssure() throws Throwable {

		String currentURl = BaseClass.driver.getCurrentUrl();
		if (currentURl.contains(""))
			elib.setExcelData3("Assure", 201, 3, currentURl);
		log.debug("Proposal Summary URL" + currentURl);

	}

	public void checkDisclaimerCheckBoxAvailableInMedicalHistoryAssure() throws Throwable {

		Boolean checkDisclaimer1 = disclaimer1AssurecheckBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 202, 3, checkDisclaimer1);
		log.debug("CheckDisclaimber is present " + checkDisclaimer1);
		Boolean smsCheckBox = receiveSmsCheckBoxAssure.isDisplayed();
		elib.setExcelDataBoolean("Assure", 203, 3, smsCheckBox);
		log.debug("smsCheckBox is present " + smsCheckBox);
		Boolean sicreCardCheckBox = sicretCardChheckBOxAssure.isDisplayed();
		elib.setExcelDataBoolean("Assure", 204, 3, sicreCardCheckBox);
		log.debug("sicreCardCheckBox is present " + sicreCardCheckBox);

	}

	public void getMedicalText() throws Throwable {

		String lifeStyleTEXT = lifeStyelQuestionsText.getText();
		elib.setExcelData3("Assure", 186, 3, lifeStyleTEXT);
		System.out.println(lifeStyleTEXT);
		log.debug("lifeStyleQuestions is present" + lifeStyleTEXT);

	}

	public void getHealthQuestionText() throws Throwable {

		String healthQuestionText = healthQuestionarrieText.getText();
		elib.setExcelData3("Assure", 187, 3, healthQuestionText);
		log.debug("Health questions are" + healthQuestionText);
		// System.out.println(healthQuestionText);

	}

	public void ge12QuestionsTextAssure() throws Throwable {
		int j = 188;
		List<WebElement> list = BaseClass.driver.findElements(
				By.xpath("//div[@class='middleMedicalHistory']/div[@class='medicalHistoryQuestionMain']/div/div/span"));

		System.out.println(list.size());

		for (int i = 0; i < list.size(); i++) {
			// System.out.println(list.get(i).getText());
			String questions = (list.get(i).getText());
			elib.setExcelData3("Assure", j, 3, questions);
			j++;
			log.debug("All the questions of Medical history" + questions);

		}
		// public void StudentexploreMedicalHistory() throws Throwable {
		//
		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,-250);");
		// Thread.sleep(1000);
		// String prmyh =
		// BaseClass.driver.findElement(By.id("enhancePremiumResultRroposalOne")).getAttribute("innerHTML");
		// // log.debug("The Enhance Premium Resultone on Screen2 is " + prmyh);
		// option3.click();
		// option4.click();
		// try {
		// BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();
		//
		// } catch (Exception e) {
		// System.out.println(e.getMessage());
		// }
		// try {
		// option5.click();
		// option6.click();
		// option7.click();
		// option8.click();}catch(Exception e) {}
		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,750);");
		// String a = "Yes";
		// String Ped = elib.getExcelData("StudentExplorepolicyplan", 6, 1);
		// if(a.equals(Ped)) {
		// BaseClass.driver.findElement(By.id("label-id1-1")).click();
		// BaseClass.driver.findElement(By.xpath("//*[@id=\"id1\"]/div[7]/div/div[2]/div/label")).click();
		// }else {
		// BaseClass.driver.findElement(By.id("label-id1-0")).click();
		//
		// }
		// option2.click();
		// validTermCondition.click();
		// TripStartIndia.click();
		// ASU.click();
		// submitstudentexplorebutton.click();
		// }

	}
}
