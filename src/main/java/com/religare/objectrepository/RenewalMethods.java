package com.religare.objectrepository;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class RenewalMethods {
	JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

	ExcelLib elib = new ExcelLib();
	WebDriverCommonLib clib = new WebDriverCommonLib();

	@FindBy(xpath = "//a[text()='Policy Renewal']")
	WebElement Clickonrenewal;
	@FindBy(xpath = "/html/body/header/ul/li[4]/a")
	WebElement Clickonrenewal1;

	@FindBy(id = "policynumber")
	WebElement policynumber;

	@FindBy(id = "alternative_number")
	WebElement altrnumber;
	@FindBy(id = "dob")
	WebElement dobw;

	@FindBy(id = "submit_insured_details")
	WebElement Medicalhistorysubmit;

	@FindBy(id = "#renew_policy_submit")
	WebElement letsrenew;

	@FindBy(xpath = "//button[@class='btn btn-primary next-step']")
	WebElement updgardesi;

	@FindBy(xpath = "(//div[@class='modal-footer'])[1]/a")
	WebElement renewmypolicy;

	Logger log = Logger.getLogger("devpinoyLogger");

	public void clickRenewals() throws Throwable {

		// WebDriverCommonLib clib = new WebDriverCommonLib();
		Thread.sleep(6000);
		clib.waitForPageToLoad();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;
		js.executeScript("arguments[0].scrollIntoView();", Clickonrenewal);

		Clickonrenewal.click();
		Set<String> set = BaseClass.driver.getWindowHandles();

		Iterator<String> it = set.iterator();

		String parentWin = it.next();

		String childWin = it.next();

		BaseClass.driver.switchTo().window(childWin);
		clib.waitForPageToLoad();
	}

	public void renewalFirstpage() throws Throwable {

		Boolean pol = policynumber.isDisplayed();
		Boolean dob = dobw.isDisplayed();
		Boolean altrnum = altrnumber.isDisplayed();

		System.out.println("Verifying the PolicyNumber Text Flied is Present " + pol);
		log.debug("Verifying the PolicyNumber Text Flied is Present " + pol);
		System.out.println("Verifying the DOB Calender is Present " + dob);
		log.debug("Verifying the DOB Calender is Present " + dob);

		System.out.println("Verifying the Alternate Number is Present " + altrnum);
		log.debug("Verifying the Alternate Number is Present" + altrnum);

		String policyno = elib.getExcelData("Renewal", 0, 1);
		String dobs = elib.getExcelData("Renewal", 1, 1);
		// WebDriverCommonLib clib = new WebDriverCommonLib();
		String altrmobile = elib.getExcelData("Renewal", 2, 1);
		policynumber.sendKeys(policyno);
		log.debug("Entering the Policy Number as " + policyno);
		dobw.sendKeys(dobs);
		log.debug("Entering the DOB as " + dobs);
		Thread.sleep(4000);
		altrnumber.click();
		altrnumber.sendKeys(altrmobile);
		log.debug("Entering the ALternate Number as " + altrmobile);

	}

	public void renewalFirstpagenext() throws InterruptedException {
		letsrenew.click();
		log.debug("Clicking on the Renew Button");
		// clib.waitForPageToLoad();
		Thread.sleep(7000);

	}

	public void Renewpagedashboard() throws InterruptedException {
		clib.waitForPageToLoad();
		Thread.sleep(1500);
		try {
			Alert al = BaseClass.driver.switchTo().alert();

			WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

			wait.until(ExpectedConditions.alertIsPresent());
			Thread.sleep(5000);
			al.accept();
		} catch (Exception e) {
		}
		Thread.sleep(5000);
		String expurl = BaseClass.driver.getCurrentUrl();
		String acturl = "rhicluat.religarehealthinsurance.com/proposalcp/renew/care_new";
		if (expurl.contains(acturl)) {
			System.out.println("Veried that user is getting redirected to Existing Policy summary page");
			log.debug("Veried that user is getting redirected to Existing Policy summary page");

		} else {
			System.out.println("Veried failed to redirected to Existing Policy summary page");
			log.debug("Veried failed to redirected to Existing Policy summary page");
		}

		Boolean quickreview = BaseClass.driver.findElement(By.id("quick_renew")).isDisplayed();
		log.debug("Checking the Quick Review Button is present " + quickreview);
		Boolean ExistingSI = BaseClass.driver.findElement(By.id("continueWithExistingSuminsured")).isDisplayed();
		log.debug("Checking the Existing SI Button is present " + ExistingSI);
		Thread.sleep(1000);
		Boolean SIupgration = BaseClass.driver.findElement(By.xpath("//button[@class='btn btn-primary next-step']"))
				.isDisplayed();
		log.debug("Checking the SI upgradation Button is present " + SIupgration);
		System.out.println("Verifying the Quick Review option is Present " + quickreview);
		System.out.println("Verifying the Existing SI Option is Present " + ExistingSI);
		System.out.println("Verifying the Upgradation option is Present " + SIupgration);

		String SI = BaseClass.driver.findElement(By.className("old_sum_insured")).getText();
		log.debug("Capturing the SI as " + SI);
		String tensure = BaseClass.driver.findElement(By.className("old_tenure")).getText();
		log.debug("Capturing the tensure as " + tensure);
		String addon = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Capturing the Addon as " + addon);
		String premium = BaseClass.driver.findElement(By.id("sub_total_amount_excluding_addons")).getText();
		log.debug("Capturing the premium value  " + premium);
		String totalpremium = BaseClass.driver.findElement(By.className("old_total_value")).getAttribute("innerHTML");
		log.debug("Capturing the total Premium as" + totalpremium);

	}

	public void quickrenew() throws InterruptedException {
		BaseClass.driver.findElement(By.id("quick_renew")).click();
		log.debug("Cliking on the quick renew button");
		clib.waitForPageToLoad();
		String exurl = BaseClass.driver.getCurrentUrl();
		String Acurl = "test.payu.in";
		// "https://test.payu.in/_payment_options?mihpayid=d6f9729d7781fc63826154476189b213ea2a05313412fe82fb3fd3f290e3cffa";
		if (exurl.contains(Acurl)) {
			System.out.println("Veried that user is getting redirected to Payment page");

			log.debug("Verfied that user is getting redirected to Payment page");
		} else {
			System.out.println("Veried failed to redirected to Payment page");
			log.debug("Verfied failed to redirected to Payment page");
		}
	}

	public void continueRenewalfirstpagenextautoRenewal() throws Throwable {
		Thread.sleep(3000);

		/*
		 * Clickonrenewal1.click(); Set<String> set =
		 * BaseClass.driver.getWindowHandles();
		 * 
		 * Iterator<String> it = set.iterator();
		 * 
		 * String parentWin = it.next();
		 * 
		 * String childWin = it.next();
		 * 
		 * BaseClass.driver.switchTo().window(childWin);
		 */

		/*
		 * clib.waitForPageToLoad(); Boolean pol =
		 * BaseClass.driver.findElement(By.id("policynumber")).isDisplayed(); Boolean
		 * dob = BaseClass.driver.findElement(By.id("policynumber")).isDisplayed();
		 * 
		 * System.out.println("Verifying the PolicyNumber Text Flied is Present " +
		 * pol); log.debug("Verifying the PolicyNumber Text Flied is Present " + pol);
		 * System.out.println("Verifying the DOB Calender is Present " + dob);
		 * log.debug("Verifying the DOB Calender is Present " + dob);
		 * 
		 * String policyno = elib.getExcelData("Renewal", 0, 1); String dobs =
		 * elib.getExcelData("Renewal", 1, 1); // WebDriverCommonLib clib = new
		 * WebDriverCommonLib(); policynumber.sendKeys(policyno);
		 * log.debug("Entering the Policy Number as " + policyno); dobw.sendKeys(dobs);
		 * log.debug("Entering the DOB as " + dobs); Thread.sleep(1000);
		 * letsrenew.click(); log.debug("Clicking on the Renew Button");
		 */
		clib.waitForPageToLoad();
		try {
			Alert al = BaseClass.driver.switchTo().alert();

			WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

			wait.until(ExpectedConditions.alertIsPresent());
			Thread.sleep(2000);
			al.accept();
		} catch (Exception e) {
		}
		// clib.waitForPageToLoad();
		// clib.waitForPageToLoad();
		// Thread.sleep(5000);

	}

	public void continueSIautoRenewal() throws Throwable {
		Thread.sleep(3000);
		try {
			updgardesi.click();
			log.debug("Clicking on the continue With Existing Suminsured Button");
		} catch (Exception e) {
		}
	clib.waitForPageToLoad();

	String exurl = BaseClass.driver.getCurrentUrl();
	String Acurl = "https://rhicluat.religarehealthinsurance.com/proposalcp/renew/care_new.php?mode=INIT&policynum=MTAxOTkwMTE=&prd_type=HEALTH&premiumAmt=&zRRD=W3siY3VzdF9pZCI6IjUwNDgzODE2IiwibmNiIjoiKzAwMDAwMDAwLjAwIiwidF9uY2JzIjoiKzAwMDAwMDAwLjAwIn1d&careType=carev2&addonsData=WyJFVkVSWURBWUNBUkUiLCJDQVJFV0lUSE5DQiJd";if(exurl.equals(Acurl))
	{
		System.out.println("veried that user is redirecting to upgrade with add ons page");
		log.debug("veried that user is redirecting to upgrade with add ons page");
	}else
	{
		System.out.println("Veried failed to redirected to upgrade with add ons page");
		log.debug("Veried failed to redirected to upgrade with add ons page");
	}

	}

	public void continueAddonautoRenewal() throws Throwable {

		String addon = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Verifying the Addon values as " + addon);

		// BaseClass.driver.findElement(By.id("continueWithExistingSuminsured")).click();
		// log.debug("Clicking on the continue with existing suminsured ");
		Thread.sleep(3000);
		boolean Nextbutton = BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[2]/button"))
				.isDisplayed();
		boolean backbutton = BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[1]/button"))
				.isDisplayed();
		System.out.println("Verifying the Nextbutton is present in the upgrated addons page " + Nextbutton);
		log.debug("Verifying the Nextbutton is present in the upgrated addons page " + Nextbutton);
		System.out.println("Verifying the Backbutton is present in the upgrated addons page " + backbutton);
		log.debug("Verifying the backbutton is present in the upgrated addons page " + backbutton);
		BaseClass.driver.findElement(By.id("NCBS")).click();
		String addon1 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Selecting the NCB addon,than the Addon total value is  " + addon1);
		BaseClass.driver.findElement(By.id("EVERYDAYCARE")).click();
		log.debug("Clickin on the Every day care addon option  ");
		String addon2 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Selecting the Everday addon,than the Addon total value is  " + addon2);
		BaseClass.driver.findElement(By.xpath("//a[@data-topclass='everyday_care']")).click();
		log.debug("Clicking on the Know more option ");
		clib.waitForPageToLoad();
		boolean cross = BaseClass.driver.findElement(By.xpath("//*[@id=\"infoPopup\"]/div[2]/div/button"))
				.isDisplayed();
		log.debug("Verifying the cross button is present as " + cross);
		BaseClass.driver.findElement(By.xpath("//*[@id=\"infoPopup\"]/div[2]/div/button")).click();
		log.debug("clicking on the cross button");
		clib.waitForPageToLoad();

	}

	public void continueTenureautoRenewal() throws Throwable {
		BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[2]/button")).click();
		log.debug("clicking on the Next button");
		clib.waitForPageToLoad();
		String prem = BaseClass.driver.findElement(By.xpath("//strong[@class='old_total_value']"))
				.getAttribute("innerHTML");
		log.debug("Total premium for tensure 1 is " + prem);
		Thread.sleep(1500);
		BaseClass.driver.findElement(By.xpath("//input[@id='year2']")).click();
		log.debug("Selecting the Tensure 2 option ");
		Thread.sleep(3500);
		String pre1 = BaseClass.driver.findElement(By.xpath("//strong[@class='old_total_value']"))
				.getAttribute("innerHTML");
		log.debug("Total premium for tensure 2 is " + pre1);
		Thread.sleep(2000);
	}

	public void continueInsureddetailsautoRenewal() throws Throwable {
		String pre1 = BaseClass.driver.findElement(By.xpath("//strong[@class='old_total_value']"))
				.getAttribute("innerHTML");

		BaseClass.driver.findElement(By.xpath("//*[@id=\"step3\"]/div/ul/li[2]/button")).click();
		clib.waitForPageToLoad();
		log.debug("Clicking on the Next Button");
		clib.waitForPageToLoad();
		String expmsg = BaseClass.driver.findElement(By.className("tab_desc")).getText();
		String Actmsg = "If you don't want to make any changes you can proceed to renew your policy";
		if (expmsg.equals(Actmsg)) {
			System.out.println("Verified that user is redirected to Proposal & Insured details page");
			log.debug("Verified that user is redirected to Proposal & Insured details page");
		} else {
			System.out.println("Verification failed to redirected to Proposal & Insured details page");
			log.debug("Verified that user is redirected to Proposal & Insured details page");

		}
		log.debug("Total premium on the proposal and insured details page is  " + pre1);
		try {
			BaseClass.driver.findElementByXPath("//*[@id=\"goodHealthDeclaration\"]/div[3]/div/div[2]/input").click();
		} catch (Exception e) {

		}
		// BaseClass.driver.findElement(By.id("declarationAutoSI
		// declarationNoGHD")).click();
		BaseClass.driver.findElement(By.id("SICREDITCARD")).click();
		log.debug("Clicking on the Sicredit card option");
		BaseClass.driver.findElement(By.id("validTermCondition-1")).click();
		log.debug("Clicking on the valid terms and condition option");
		/*
		 * try { BaseClass.driver.findElement(By.
		 * xpath("//input[@class ='declarationAutoSI declarationNoGHD']")).click(); }
		 * catch (NoSuchElementException e) {
		 * 
		 * }
		 */
	}
	public void Medicalhistoryanswer() {
		WebElement ans=BaseClass.driver.findElementByXPath("//*[@id=\"display_terms\"]/div/div[3]/div[2]/div[1]/div[2]/input");
		ans.click();
		WebElement termsandcondition=BaseClass.driver.findElementByXPath("//*[@id=\"validTermCondition-1\"]");
		termsandcondition.click();
	}

	public void continueautoRenewalMedicalhistorysubmit() throws InterruptedException {
		js.executeScript("arguments[0].scrollIntoView();", Medicalhistorysubmit);
		Medicalhistorysubmit.click();
		log.debug("Clicking on the Submit Button");
		clib.waitForPageToLoad();

	}

	public void Renewnewpolicy() {
		// js.executeScript("window.scrollBy(0,1000)");
		js.executeScript("arguments[0].scrollIntoView();", renewmypolicy);
		renewmypolicy.click();
		log.debug("Clicking on renewmypolicy");
	}

	public void upgraautoRenewalAddons() throws Throwable {

		Thread.sleep(5000);

		/*
		 * Clickonrenewal1.click(); Set<String> set =
		 * BaseClass.driver.getWindowHandles();
		 * 
		 * Iterator<String> it = set.iterator();
		 * 
		 * String parentWin = it.next();
		 * 
		 * String childWin = it.next();
		 * 
		 * BaseClass.driver.switchTo().window(childWin);
		 */

		clib.waitForPageToLoad();
		/*
		 * Boolean pol =
		 * BaseClass.driver.findElement(By.id("policynumber")).isDisplayed(); Boolean
		 * dob = BaseClass.driver.findElement(By.id("policynumber")).isDisplayed();
		 * 
		 * System.out.println("Verifying the PolicyNumber Text Flied is Present " +
		 * pol); log.debug("Verifying the PolicyNumber Text Flied is Present " + pol);
		 * System.out.println("Verifying the DOB Calender is Present " + dob);
		 * log.debug("Verifying the DOB Calender is Present " + dob);
		 * 
		 * String policyno = elib.getExcelData("Renewal", 0, 1); String dobs =
		 * elib.getExcelData("Renewal", 1, 1); // WebDriverCommonLib clib = new
		 * WebDriverCommonLib(); policynumber.sendKeys(policyno);
		 * log.debug("Entering the Policy Number as " + policyno); dobw.sendKeys(dobs);
		 * log.debug("Entering the DOB as " + dobs); Thread.sleep(1000);
		 * letsrenew.click(); log.debug("Clicking on the Renew Button");
		 */
		clib.waitForPageToLoad();
		try {
			Alert al = BaseClass.driver.switchTo().alert();

			WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

			wait.until(ExpectedConditions.alertIsPresent());
			Thread.sleep(5000);
			al.accept();
		} catch (Exception e) {
		}
		clib.waitForPageToLoad();
		Thread.sleep(5000);
		try {
			BaseClass.driver.findElement(By.xpath("//input[@id='premium1']")).click();
			log.debug("Clicking on the upgrade Button");
		} catch (Exception e) {
		}
		try {
			updgardesi.click();
			log.debug("Clicking on the continue With Existing Suminsured Button");
		} catch (Exception e) {

		}

		clib.waitForPageToLoad();
		String exptit = BaseClass.driver.getCurrentUrl();
		String acttit = "https://rhicluat.religarehealthinsurance.com/proposalcp/renew/care_new.php?mode=INIT&policynum";
		if (exptit.contains(acttit)) {
			System.out.println(
					"Verified that User is redirected to add on selection page once upgraded s.i. is selected");
			log.debug("Verified that User is redirected to add on selection page once upgraded s.i. is selected");
		} else {
			System.out
					.println("Verification failed  redirected to add on selection page once upgraded s.i. is selected");
			log.debug("Verification failed  redirected to add on selection page once upgraded s.i. is selected");
		}

		String prem1 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Total Addons on the addon page is " + prem1);
		Thread.sleep(3000);
		boolean Nextbutton = BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[2]/button"))
				.isDisplayed();
		boolean backbutton = BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[1]/button"))
				.isDisplayed();
		log.debug("Verifying the Backbutton is present " + backbutton);
		log.debug("Verfiying that Next button is present " + Nextbutton);
		BaseClass.driver.findElement(By.xpath("//input[@id='NCBS']")).click();
		log.debug("Selecting the tool tip of NCBS ");
		String addon1 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Selecting the NCB addon,than the Addon total value is  " + addon1);
		BaseClass.driver.findElement(By.id("EVERYDAYCARE")).click();
		String addon2 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		log.debug("Selecting the Everday addon,than the Addon total value is  " + addon2);
		BaseClass.driver.findElement(By.xpath("//a[@data-topclass='everyday_care']")).click();
		log.debug("Clicking on the Know more option ");
		clib.waitForPageToLoad();
		boolean cross = BaseClass.driver.findElement(By.xpath("//*[@id=\"infoPopup\"]/div[2]/div/button"))
				.isDisplayed();
		log.debug("Verifying the cross button is present as " + cross);
		BaseClass.driver.findElement(By.xpath("//*[@id=\"infoPopup\"]/div[2]/div/button")).click();
		log.debug("clicking on the cross button");
		clib.waitForPageToLoad();
		Thread.sleep(3000);

	}

	public void upgraautoRenewalTenure() throws InterruptedException {
		BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[2]/button")).click();
		log.debug("clicking on the Next button");
		String prem1 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		clib.waitForPageToLoad();
		String prem = BaseClass.driver.findElement(By.className("old_total_value")).getAttribute("innerHTML");
		log.debug("Total premium for tensure 1 is " + prem);
		Thread.sleep(3000);
		BaseClass.driver.findElement(By.xpath("//input[@id='year2']")).click();
		log.debug("Selecting the Tensure 2 option ");
		Thread.sleep(3000);

	}

	public void upgraautoRenewalInsureddetails() throws InterruptedException {
		String prem1 = BaseClass.driver.findElement(By.className("add_on_total")).getText();
		String prem2 = BaseClass.driver.findElement(By.className("old_total_value")).getAttribute("innerHTML");
		log.debug("Total premium for tensure 2 is " + prem2);
		BaseClass.driver.findElement(By.xpath("//*[@id=\"step2\"]/div[1]/ul/li[2]/button")).isDisplayed();
		clib.waitForPageToLoad();
		log.debug("Clicking on the Next Button");
		clib.waitForPageToLoad();
		String expmsg = BaseClass.driver.findElement(By.className("tab_desc")).getText();
		String Actmsg = "If you don't want to make any changes you can proceed to renew your policy";
		if (expmsg.equals(Actmsg)) {
			System.out.println("Verified that user is redirected to Proposal & Insured details page");
			log.debug("Verified that user is redirected to Proposal & Insured details page");
		} else {
			System.out.println("Verification failed to redirected to Proposal & Insured details page");
			log.debug("Verified that user is redirected to Proposal & Insured details page");

		}
		log.debug("Total premium on the proposal and insured details page is " + prem1);
		Thread.sleep(5000);
		WebElement tenurenext = BaseClass.driver
				.findElement(By.xpath("(//ul[@class='list-inline text-center'])[3]/li[2]/button"));
		js.executeScript("arguments[0].scrollIntoView();", tenurenext);
		Thread.sleep(3000);
		tenurenext.click();
		log.debug("Clicking on the Next button");
		clib.waitForPageToLoad();
		Thread.sleep(3000);
		BaseClass.driver.findElement(By.xpath("//input[@class='declarationAutoSI declarationNoGHD']")).click();
		BaseClass.driver.findElement(By.id("SICREDITCARD")).click();
		log.debug("Clicking on the Sicredit card option");
		BaseClass.driver.findElement(By.id("validTermCondition-1")).click();
		log.debug("Clicking on the valid terms and condition option");
	}

	public void upgraautoRenewalMedicalhistorysubmit() {
		BaseClass.driver.findElement(By.id("submit_insured_details")).click();
		log.debug("Clicking on the Submit Button");
		clib.waitForPageToLoad();

	}
	/*
	 * public void payment() {
	 * BaseClass.driver.findElement(By.id("SubmitPayment")).click();
	 * log.debug("Clicking on the Renew my policy"); clib.waitForPageToLoad(); }
	 */
}
