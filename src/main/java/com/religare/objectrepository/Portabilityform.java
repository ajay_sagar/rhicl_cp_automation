package com.religare.objectrepository;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class Portabilityform {

	ExcelLib elib = new ExcelLib();
	WebDriverCommonLib clib = new WebDriverCommonLib();
	JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;
	Logger log = Logger.getLogger("devpinoyLogger");

	@FindBy(xpath = "(//h4[@class='panel-title'])[1]/div")
	WebElement ProposersDetails;

	@FindBy(xpath = "(//h4[@class='panel-title'])[2]/div")
	WebElement ExistingInsurancePolicyDetails;

	@FindBy(xpath = "(//h4[@class='panel-title'])[3]/div")
	WebElement PreviousInsurancePolicyDetails;

	@FindBy(xpath = "(//h4[@class='panel-title'])[4]/div")
	WebElement PreviousYearPolicyDocuments;

	@FindBy(xpath = "(//h4[@class='panel-title'])[5]/div")
	WebElement RenewalNotice;

	@FindBy(xpath = "(//h4[@class='panel-title'])[6]/div")
	WebElement Claims;

	@FindBy(id = "firstName")

	WebElement firstname;

	@FindBy(id = "lastname")

	WebElement lastname;

	WebElement DOB;

	@FindBy(xpath = "//div[@class='ui-datepicker-title']/select[1]")

	WebElement month;

	@FindBy(xpath = "//div[@class='ui-datepicker-title']/select[2]")

	WebElement year;

	@FindBy(xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr/td/a[text()='2']")

	WebElement day;

	@FindBy(id = "email")
	WebElement emailid;

	@FindBy(id = "dob")
	WebElement Dob;

	@FindBy(id = "address1")
	WebElement address1;

	@FindBy(id = "address2")
	WebElement address2;

	@FindBy(id = "p_landmark")
	WebElement landmark;

	@FindBy(id = "ValidPinCode")
	WebElement pincode;

	@FindBy(id = "ValidStateName")
	WebElement state;

	@FindBy(xpath = "//div[@class='select_box sel_city ie9']/select")
	WebElement Select;

	@FindBy(xpath = "(//div[@class='panel-body'])[1]/div/button")
	WebElement firstSave;

	@FindBy(xpath = "(//div[@class='row'])[1]/div[6]/input")
	WebElement memberattachmentdate;

	@FindBy(id = "pm_height_feet_1")
	WebElement height;

	@FindBy(id = "pm_height_inch_1")
	WebElement inch;

	@FindBy(id = "pm_weight_1")
	WebElement weight;

	@FindBy(id = "prev_policy_date")
	WebElement firstInsurancepolicydate;

	@FindBy(xpath = "(//div[@class='panel-body'])[2]/div/button")
	WebElement secondsave;

	@FindBy(id = "prev_policy_date")
	WebElement firstinsurancepolicystarted;

	@FindBy(xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr/td/a[text()='12']")

	WebElement firstinsuranceday;

	@FindBy(id = "p_policy_no")
	WebElement expiringPolicyNumber;

	@FindBy(id = "policy_start_datepicker")
	WebElement policystartdate;

	@FindBy(id = "policy_end_datepicker")
	WebElement policyEnddate;

	@FindBy(xpath = "(//div[@class='select_box ie9'])[6]/select")
	WebElement insureName;

	@FindBy(xpath = ".//*[@id='savethird']")
	WebElement thirdSave;

	@FindBy(xpath = ".//*[@id='files']")
	WebElement browseprevoiuspolicy;

	@FindBy(xpath = ".//*[@id='savefourth']")
	WebElement fourthSave;

	@FindBy(xpath = "(//div[@class='dropzone text-center'])[2]/input")
	WebElement browserenewalnotice;

	@FindBy(xpath = ".//*[@id='savefifth']")
	WebElement fifthSave;

	@FindBy(xpath = ".//*[@id='savesixth']")
	WebElement sixthSave;

	@FindBy(xpath = ".//*[@id='lhc_need_help_close']")
	WebElement chatclose;

	@FindBy(xpath = "//div[@class='text-center']/button")
	WebElement next;

	@FindBy(id = "firstcollapseclose")
	WebElement healthanswer1;

	@FindBy(id = "secondcollapseclose")
	WebElement healthanswer2;

	@FindBy(id = "thirdcollapseclose")
	WebElement healthanswer3;

	@FindBy(id = "fourthcollapseclose")
	WebElement healthanswer4;

	@FindBy(xpath = "//div[@class='customInputContainer'][1]/input")
	WebElement standinginstruction;

	@FindBy(id = "submitform")
	WebElement healthquestionsnext;

	@FindBy(xpath = "//div[@class='btn_cont']/button[1]")
	WebElement Confirmpolicy;

	@FindBy(xpath = "(//div[@class='row'])[4]/div[2]/p[2]/input")
	WebElement pancard;

	@FindBy(id = "paymentGenerator")
	WebElement ProceedtoPay;

	public void verifyheaders() {

		clib.waitForPageToLoad();

		if (ProposersDetails.isDisplayed() && ExistingInsurancePolicyDetails.isDisplayed()
				&& PreviousInsurancePolicyDetails.isDisplayed() && PreviousYearPolicyDocuments.isDisplayed()
				&& RenewalNotice.isDisplayed() && Claims.isDisplayed())

		{
			System.out.println(
					"Proposer's Details,Existing Insurance Policy Details,Previous Insurance Policy Details,PreviousYearPolicy Documents,Renewal Notice & Claims headers are displayed");
		} else {

			System.out.println("fail");

		}
		log.debug(
				"Proposer's Details,Existing Insurance Policy Details,Previous Insurance Policy Details,PreviousYearPolicy Documents,Renewal Notice & Claims headers are displayed");

	}

	public void fillproposersdetails() throws Throwable {

		String firstName = elib.getExcelData("Portability", 26, 1);

		String lastName = elib.getExcelData("Portability", 27, 1);

		String day = elib.getExcelData("Portability", 29, 3);
		String Month = elib.getExcelData("Portability", 29, 1);

		String Year = elib.getExcelData("Portability", 29, 2);
		String emailId = elib.getExcelData("Portability", 28, 1);
		String addressOne = elib.getExcelData("Portability", 30, 1);

		String addressTwo = elib.getExcelData("Portability", 31, 1);

		String Landmark = elib.getExcelData("Portability", 32, 1);

		String Pincode = elib.getExcelData("Portability", 33, 1);

		String city = elib.getExcelData("Portability", 30, 1);
		firstSave.click();
		//Thread.sleep(1000);
		// clib.errormessagescreenshot("Proposerdetilsfirstname");
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

		Boolean is_valid = (Boolean) js.executeScript("return arguments[0].checkValidity();", firstname);
		String message = (String) js.executeScript("return arguments[0].validationMessage;", firstname);
		log.debug("Validation message is displyed for Firstname" + message);
		// String str = firstname.getText();
		// String str1 = firstname.getAttribute("value");
		System.out.println(message);
		clib.Passscreenshot("Errormessageinfirstname", "Carefreedomerrormessageinfirstname");
		firstname.click();
		firstname.sendKeys(firstName);
		log.debug("Entering FirstName as " + firstName);
		firstSave.click();
		Thread.sleep(1000);
		Boolean is_valid1 = (Boolean) js.executeScript("return arguments[0].checkValidity();", lastname);
		String message1 = (String) js.executeScript("return arguments[0].validationMessage;", lastname);
		log.debug("Validation message is displyed for Lastname" + message1);
		clib.Passscreenshot("Errormessageinlastname", "Eerrormessageinlastname");
		lastname.click();
		lastname.sendKeys(lastName);
		firstSave.click();
		Thread.sleep(1000);
		log.debug("Entering LastName as " + lastName);
		Boolean is_valid2 = (Boolean) js.executeScript("return arguments[0].checkValidity();", emailid);
		String message2 = (String) js.executeScript("return arguments[0].validationMessage;", emailid);
		log.debug("Validation message is displyed for emailid" + message2);
		clib.Passscreenshot("Errormessageinemailid", "Eerrormessageinemailid");
		emailid.click();
		emailid.sendKeys(emailId);
		log.debug("Entering Emailid  as " + emailId);
		firstSave.click();
		Boolean is_valid7 = (Boolean) js.executeScript("return arguments[0].checkValidity();", Dob);
		String message7 = (String) js.executeScript("return arguments[0].validationMessage;", Dob);
		log.debug("Validation message is displyed for address1" + message7);
		clib.Passscreenshot("Errormessageincalander", "Eerrormessageincalander");
		Dob.click();
		clib.select(month, Month);
		log.debug("Entering Month as " + Month);
		Thread.sleep(1000);
		clib.select(year, Year);
		log.debug("Entering Year as " + Year);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering date as " + day);
		firstSave.click();
		Thread.sleep(1000);
		Boolean is_valid3 = (Boolean) js.executeScript("return arguments[0].checkValidity();", address1);
		String message3 = (String) js.executeScript("return arguments[0].validationMessage;", address1);
		log.debug("Validation message is displyed for address1" + message3);
		clib.Passscreenshot("Errormessageinaddress1", "Eerrormessageinaddress1");
		address1.click();
		address1.sendKeys(addressOne);
		log.debug("Entering FirstAddress  as " + addressOne);
		firstSave.click();
		Thread.sleep(1000);
		Boolean is_valid4 = (Boolean) js.executeScript("return arguments[0].checkValidity();", address2);
		String message4 = (String) js.executeScript("return arguments[0].validationMessage;", address2);
		log.debug("Validation message is displyed for Firstname" + message4);
		clib.Passscreenshot("Errormessageinaddress2", "Eerrormessageinaddress2");
		address2.click();
		address2.sendKeys(addressTwo);
		log.debug("Entering SecondAddress  as " + addressTwo);
		firstSave.click();
		Thread.sleep(1000);
		Boolean is_valid5 = (Boolean) js.executeScript("return arguments[0].checkValidity();", landmark);
		String message5 = (String) js.executeScript("return arguments[0].validationMessage;", landmark);
		log.debug("Validation message is displyed for landmark" + message5);
		clib.Passscreenshot("Errormessageinlandmark", "Errormessageinlandmark");
		landmark.click();
		landmark.sendKeys(Landmark);
		log.debug("Entering Landmark  as " + landmark);
		Thread.sleep(5000);
		firstSave.click();
		Thread.sleep(1000);
		Boolean is_valid6 = (Boolean) js.executeScript("return arguments[0].checkValidity();", pincode);
		String message6 = (String) js.executeScript("return arguments[0].validationMessage;", pincode);
		log.debug("Validation message is displyed for Pincode" + message6);
		clib.Passscreenshot("Errormessageinpincode", "Errormessageinpincode");
		pincode.click();
		pincode.sendKeys(Pincode);
		log.debug("Entering Pincode as " + Pincode);
		Thread.sleep(5000);
		// clib.screenshotalert("ProposersDetailsPage");
		// firstSave.click();

	}
	public void proposersdetailssave() {

		firstSave.click();

	}

	public void existinginsurancePolicyCarefreedomsinglemember() throws Throwable {
		String Relation = elib.getExcelData("Portability", 49, 1);
		String Weight = elib.getExcelData("Portability", 38, 1);

		String Height = elib.getExcelData("Portability", 36, 1);

		String Inches = elib.getExcelData("Portability", 37, 1);
		String Year = elib.getExcelData("Portability", 82, 2);
		String Month = elib.getExcelData("Portability", 82, 1);
		String day = elib.getExcelData("Portability", 82, 3);
		String FirstName = elib.getExcelData("Portability", 61, 1);
		String DOBYear = elib.getExcelData("Portability", 55, 2);
		String DOBMonth = elib.getExcelData("Portability", 55, 1);
		String DOBday = elib.getExcelData("Portability", 55, 3);
		// secondsave.click();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

		/*
		 * Boolean is_valid = (Boolean)
		 * js.executeScript("return arguments[0].checkValidity();",memberattachmentdate)
		 * ; String message = (String)
		 * js.executeScript("return arguments[0].validationMessage;",
		 * memberattachmentdate);
		 */
		/*
		 * Alert al = BaseClass.driver.switchTo().alert(); String errorMessagePopUp =
		 * al.getText(); log.debug("Alert message is displayed " + errorMessagePopUp);
		 * Thread.sleep(2000);
		 * clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
		 * al.accept();
		 */
		// log.debug("Validation message is displyed"+ message);
		Thread.sleep(5000);
		WebElement relation = BaseClass.driver.findElement(By.id("selfport1"));
		clib.selectByValue(relation, Relation);
		log.debug("For memberone In existinginsurance policy Selecting Relation as " + Relation);

		WebElement firstname = BaseClass.driver.findElement(By.xpath("//input[@id='pm_first_name_1']"));
		firstname.click();
		firstname.sendKeys(FirstName);
		log.debug("For memberone In existinginsurance policy Entering FirstName as " + FirstName);
		Thread.sleep(3000);

		WebElement DOB = BaseClass.driver.findElement(By.id("pm_dob_1"));
		DOB.click();
		js.executeScript("arguments[0].scrollIntoView(true);", year);
		clib.select(year, DOBYear);
		log.debug("For memberone In existinginsurance policy Selecting DOB year as " + DOBYear);
		Thread.sleep(3000);
		clib.select(month, DOBMonth);
		log.debug("For memberone In existinginsurance policy Selecting DOB month as " + DOBMonth);
		Thread.sleep(2000);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(DOBday) + "']")).click();
		log.debug("For memberone In existinginsurance policy Selecting DOB date as " + DOBday);

		// ExistingInsurancePolicyDetails.click();
		memberattachmentdate.click();
		Thread.sleep(3000);
		clib.selectByValue(year, Year);
		log.debug("In memberattachmentdate Selecting year as " + Year);
		Thread.sleep(2000);
		clib.select(month, Month);
		log.debug("In memberattachmentdate Selecting month as " + Month);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("In memberattachmentdate Selecting day as " + Month);
		// day.click();
		clib.select(height, Height);
		log.debug("For memberone Entering Height as " + Height);
		clib.select(inch, Inches);
		log.debug("For memberone Entering Inch as " + Inches);
		weight.click();
		weight.sendKeys(Weight);
		log.debug("For memberone Entering weight as " + Weight);
		Thread.sleep(2000);
		// clib.screenshotalert("ExistinginsurancePolicydetails");
		secondsave.click();

		Thread.sleep(3000);
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		log.debug("Alert message is displayed for exsitinginsurancepolicy " + errorMessagePopUp);
		Thread.sleep(2000);
		// clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
		al.accept();

	}

	public void existinginsurancePolicyCarefreedomtwomember() throws Throwable {

		// WebElement membertwo=BaseClass.driver.findElement(By.id("added_member_2'"));
		// membertwo.click();
		String Relation = elib.getExcelData("Portability", 50, 1);
		String Weight = elib.getExcelData("Portability", 69, 1);

		String Height = elib.getExcelData("Portability", 67, 1);

		String Inches = elib.getExcelData("Portability", 68, 1);
		String Year = elib.getExcelData("Portability", 82, 2);
		String Month = elib.getExcelData("Portability", 82, 1);
		String day = elib.getExcelData("Portability", 82, 3);
		String FirstName = elib.getExcelData("Portability", 62, 1);
		String DOBYear = elib.getExcelData("Portability", 56, 2);
		String DOBMonth = elib.getExcelData("Portability", 56, 1);
		String DOBday = elib.getExcelData("Portability", 56, 3);
		// secondsave.click();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

		/*
		 * Boolean is_valid = (Boolean)
		 * js.executeScript("return arguments[0].checkValidity();",memberattachmentdate)
		 * ; String message = (String)
		 * js.executeScript("return arguments[0].validationMessage;",
		 * memberattachmentdate);
		 */
		/*
		 * Alert al = BaseClass.driver.switchTo().alert(); String errorMessagePopUp =
		 * al.getText(); log.debug("Alert message is displayed " + errorMessagePopUp);
		 * Thread.sleep(2000);
		 * clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
		 * al.accept();
		 */
		// log.debug("Validation message is displyed"+ message);

		// ExistingInsurancePolicyDetails.click();
		Thread.sleep(3000);
		WebElement relation = BaseClass.driver.findElement(By.id("selfport2"));
		clib.selectByValue(relation, Relation);
		log.debug("For membertwo Selecting Relation as " + Relation);
		// day.click();
		WebElement firstname = BaseClass.driver.findElement(By.id("pm_first_name_2"));
		firstname.click();
		firstname.sendKeys(FirstName);
		log.debug("For membertwo Entering Firstname as " + FirstName);
		WebElement DOB = BaseClass.driver.findElement(By.id("pm_dob_2"));
		DOB.click();
		js.executeScript("arguments[0].scrollIntoView(true);", year);
		clib.select(year, DOBYear);
		log.debug("For membertwo Selecting DOB Year as " + DOBYear);
		Thread.sleep(3000);
		clib.select(month, DOBMonth);
		log.debug("For membertwo Selecting DOB Month as " + DOBMonth);
		Thread.sleep(2000);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(DOBday) + "']")).click();
		log.debug("For membertwo Selecting DOB date as " + DOBday);
		Thread.sleep(3000);
		WebElement memberattachmentdate = BaseClass.driver.findElement(By.id("pm_enroll_date_2"));
		memberattachmentdate.click();
		Thread.sleep(3000);
		clib.selectByValue(year, Year);
		log.debug("In memberattachmentdate Selecting Year as " + Year);
		Thread.sleep(2000);
		clib.select(month, Month);
		log.debug("In memberattachmentdate Selecting Month as " + Month);	
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("In memberattachmentdate Selecting day as " + day);	
		// day.click();
		WebElement height = BaseClass.driver.findElement(By.id("pm_height_feet_2"));
		clib.select(height, Height);
		log.debug("For membertwo Entering Height as " + Height);
		Thread.sleep(2000);
		WebElement Inch = BaseClass.driver.findElement(By.id("pm_height_inch_2"));
		clib.select(Inch, Inches);
		log.debug("For membertwo Entering Inch as " + Inches);
		Thread.sleep(2000);
		WebElement weight = BaseClass.driver.findElement(By.id("pm_weight_2"));
		weight.click();
		weight.sendKeys(Weight);
		log.debug("For membertwo Entering Inch as " + Weight);
		Thread.sleep(2000);
		// clib.screenshotalert("ExistinginsurancePolicydetails");
		//secondsave.click();
		Thread.sleep(9000);

	}

	public void existinginsurancePolicySave() {

		secondsave.click();
	}

	public void existinginsurancePolicyCaresinglemember() throws Throwable {

		String Relation = elib.getExcelData("Portability",104, 1);
		String Weight = elib.getExcelData("Portability", 38, 1);

		String Height = elib.getExcelData("Portability", 36, 1);

		String Inches = elib.getExcelData("Portability", 37, 1);
		String Year = elib.getExcelData("Portability", 82, 2);
		String Month = elib.getExcelData("Portability", 82, 1);
		String day = elib.getExcelData("Portability", 82, 3);
		String FirstName = elib.getExcelData("Portability", 61, 1);
		String DOBYear = elib.getExcelData("Portability", 110, 2);
		String DOBMonth = elib.getExcelData("Portability", 110, 1);
		String DOBday = elib.getExcelData("Portability", 110, 3);
		// secondsave.click();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

		/*
		 * Boolean is_valid = (Boolean)
		 * js.executeScript("return arguments[0].checkValidity();",memberattachmentdate)
		 * ; String message = (String)
		 * js.executeScript("return arguments[0].validationMessage;",
		 * memberattachmentdate);
		 */
		/*
		 * Alert al = BaseClass.driver.switchTo().alert(); String errorMessagePopUp =
		 * al.getText(); log.debug("Alert message is displayed " + errorMessagePopUp);
		 * Thread.sleep(2000);
		 * clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
		 * al.accept();
		 */
		// log.debug("Validation message is displyed"+ message);
		Thread.sleep(5000);
		WebElement relation = BaseClass.driver.findElement(By.id("selfport1"));
		clib.selectByValue(relation, Relation);
		log.debug("For memberone In existinginsurance policy Selecting Relation as " + Relation);

		WebElement firstname = BaseClass.driver.findElement(By.xpath("//input[@id='pm_first_name_1']"));
		firstname.click();
		if(Relation.equals("SELF"))
		firstname.clear();
		firstname.sendKeys(FirstName);
		log.debug("For memberone In existinginsurance policy Entering FirstName as " + FirstName);
		Thread.sleep(3000);

		WebElement DOB = BaseClass.driver.findElement(By.id("pm_dob_1"));
		DOB.click();
		js.executeScript("arguments[0].scrollIntoView(true);", year);
		clib.select(year, DOBYear);
		log.debug("For memberone In existinginsurance policy Selecting DOB year as " + DOBYear);
		Thread.sleep(3000);
		clib.select(month, DOBMonth);
		log.debug("For memberone In existinginsurance policy Selecting DOB month as " + DOBMonth);
		Thread.sleep(2000);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(DOBday) + "']")).click();
		log.debug("For memberone In existinginsurance policy Selecting DOB date as " + DOBday);

		// ExistingInsurancePolicyDetails.click();
		memberattachmentdate.click();
		Thread.sleep(3000);
		clib.selectByValue(year, Year);
		log.debug("In memberattachmentdate Selecting year as " + Year);
		Thread.sleep(2000);
		clib.select(month, Month);
		log.debug("In memberattachmentdate Selecting month as " + Month);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("In memberattachmentdate Selecting day as " + Month);
		// day.click();
		clib.select(height, Height);
		log.debug("For memberone Entering Height as " + Height);
		clib.select(inch, Inches);
		log.debug("For memberone Entering Inch as " + Inches);
		weight.click();
		weight.sendKeys(Weight);
		log.debug("For memberone Entering weight as " + Weight);
		Thread.sleep(2000);
		// clib.screenshotalert("ExistinginsurancePolicydetails");
		secondsave.click();

		Thread.sleep(3000);
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		log.debug("Alert message is displayed for exsitinginsurancepolicy " + errorMessagePopUp);
		Thread.sleep(2000);
		// clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
		al.accept();

	}

	public void existinginsurancePolicyCaretwomember() throws Throwable {


		String Weight = elib.getExcelData("Portability", 38, 1);

		String Height = elib.getExcelData("Portability", 36, 1);

		String Inches = elib.getExcelData("Portability", 37, 1);
		String day = elib.getExcelData("Portability", 35, 1);
		secondsave.click();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		log.debug("Alert message is displayed " + errorMessagePopUp);
		Thread.sleep(2000);
		clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
		al.accept();
		// log.debug("Validation message is displyed"+ message);
		ExistingInsurancePolicyDetails.click();
		memberattachmentdate.click();
		Thread.sleep(3000);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		// day.click();
		clib.select(height, Height);
		log.debug("Entering Height as " + Height);
		clib.select(inch, Inches);
		log.debug("Entering Inch as " + Inches);
		weight.click();
		weight.sendKeys(Weight);
		log.debug("Entering Inch as " + Weight);
		Thread.sleep(2000);
		clib.screenshotalert("ExistinginsurancePolicydetails");
		secondsave.click();
		Thread.sleep(9000);

	}

	public void existinginsurancePolicyCaremembertwo() throws Throwable {

		// WebElement membertwo=BaseClass.driver.findElement(By.id("added_member_2'"));
				// membertwo.click();
				String Relation = elib.getExcelData("Portability", 105, 1);
				String Weight = elib.getExcelData("Portability", 69, 1);

				String Height = elib.getExcelData("Portability", 67, 1);

				String Inches = elib.getExcelData("Portability", 68, 1);
				String Year = elib.getExcelData("Portability", 118, 2);
				String Month = elib.getExcelData("Portability", 118, 1);
				String day = elib.getExcelData("Portability", 118, 3);
				String FirstName = elib.getExcelData("Portability", 62, 1);
				String DOBYear = elib.getExcelData("Portability",111, 2);
				String DOBMonth = elib.getExcelData("Portability",111, 1);
				String DOBday = elib.getExcelData("Portability",111, 3);
				// secondsave.click();
				JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

				/*
				 * Boolean is_valid = (Boolean)
				 * js.executeScript("return arguments[0].checkValidity();",memberattachmentdate)
				 * ; String message = (String)
				 * js.executeScript("return arguments[0].validationMessage;",
				 * memberattachmentdate);
				 */
				/*
				 * Alert al = BaseClass.driver.switchTo().alert(); String errorMessagePopUp =
				 * al.getText(); log.debug("Alert message is displayed " + errorMessagePopUp);
				 * Thread.sleep(2000);
				 * clib.screenshotalert("ExistinginsurancePolicydetailsErrormessage");
				 * al.accept();
				 */
				// log.debug("Validation message is displyed"+ message);

				// ExistingInsurancePolicyDetails.click();
				Thread.sleep(3000);
				WebElement relation = BaseClass.driver.findElement(By.id("selfport2"));
				clib.selectByValue(relation, Relation);
				log.debug("For membertwo Selecting Relation as " + Relation);
				// day.click();
				WebElement firstname = BaseClass.driver.findElement(By.id("pm_first_name_2"));
				firstname.click();
				firstname.sendKeys(FirstName);
				log.debug("For membertwo Entering Firstname as " + FirstName);
				WebElement DOB = BaseClass.driver.findElement(By.id("pm_dob_2"));
				DOB.click();
				js.executeScript("arguments[0].scrollIntoView(true);", year);
				clib.select(year, DOBYear);
				log.debug("For membertwo Selecting DOB Year as " + DOBYear);
				Thread.sleep(3000);
				clib.select(month, DOBMonth);
				log.debug("For membertwo Selecting DOB Month as " + DOBMonth);
				Thread.sleep(2000);
				BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(DOBday) + "']")).click();
				log.debug("For membertwo Selecting DOB date as " + DOBday);
				Thread.sleep(3000);
				WebElement memberattachmentdate = BaseClass.driver.findElement(By.id("pm_enroll_date_2"));
				memberattachmentdate.click();
				Thread.sleep(3000);
				clib.selectByValue(year, Year);
				log.debug("In memberattachmentdate Selecting Year as " + Year);
				Thread.sleep(2000);
				clib.select(month, Month);
				log.debug("In memberattachmentdate Selecting Month as " + Month);	
				BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
				log.debug("In memberattachmentdate Selecting day as " + day);	
				// day.click();
				WebElement height = BaseClass.driver.findElement(By.id("pm_height_feet_2"));
				clib.select(height, Height);
				log.debug("For membertwo Entering Height as " + Height);
				Thread.sleep(2000);
				WebElement Inch = BaseClass.driver.findElement(By.id("pm_height_inch_2"));
				clib.select(Inch, Inches);
				log.debug("For membertwo Entering Inch as " + Inches);
				Thread.sleep(2000);
				WebElement weight = BaseClass.driver.findElement(By.id("pm_weight_2"));
				weight.click();
				weight.sendKeys(Weight);
				log.debug("For membertwo Entering Inch as " + Weight);
				Thread.sleep(2000);
				// clib.screenshotalert("ExistinginsurancePolicydetails");
				//secondsave.click();
				Thread.sleep(9000);

	}


	public void previousinsurancepolicyforcare() throws Throwable {
		String Year = elib.getExcelData("Portability", 117, 2);
		String Month = elib.getExcelData("Portability",117, 1);
		String day = elib.getExcelData("Portability", 117, 3);
		Thread.sleep(9000);
		thirdSave.click();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;
		Boolean is_valid = (Boolean) js.executeScript("return arguments[0].checkValidity();",
				firstinsurancepolicystarted);
		String message = (String) js.executeScript("return arguments[0].validationMessage;",
				firstinsurancepolicystarted);
		log.debug("Validation message for firstinsurancepolicystarted " + message );
		System.out.println(firstinsurancepolicystarted.isDisplayed());
		Thread.sleep(9000);
		firstinsurancepolicystarted.click();
		clib.selectByValue(year, Year);
		log.debug("In Previousinsurance policy selecting Year as " + Year);
		Thread.sleep(2000);
		clib.select(month, Month);
		log.debug("In Previousinsurance policy selecting Month as " + Month);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("In Previousinsurance policy selecting date as " + day);
		// firstinsuranceday.click();
		String Expiringpolicynumber = elib.getExcelData("Portability",116, 1);
		expiringPolicyNumber.click();
		expiringPolicyNumber.sendKeys(Expiringpolicynumber);
		log.debug("Entering Expiringpolicy number as " + Expiringpolicynumber);

		policystartdate.click();
		String PolicyStartDateMonth = elib.getExcelData("Portability",119, 1);
		String PolicyStartDateYear = elib.getExcelData("Portability",119, 2);
		String PolicyStartDateDay = elib.getExcelData("Portability",119, 3);
		clib.selectByValue(year, PolicyStartDateYear);
		log.debug("Selecting Policystartdate Year as " + PolicyStartDateYear);
		Thread.sleep(2000);
		clib.select(month, PolicyStartDateMonth);
		log.debug("Selecting Policystartdate Month as " + PolicyStartDateMonth);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(PolicyStartDateDay) + "']")).click();
		log.debug("Entering PolicyStartdate date  as " + PolicyStartDateDay);
		
		//firstinsuranceday.click();
		policyEnddate.click();
		String PolicyEndDateMonth = elib.getExcelData("Portability",120, 1);
		String PolicyEndDateYear = elib.getExcelData("Portability",120, 2);
		String PolicyEndDateDay = elib.getExcelData("Portability",120, 3);
		clib.selectByValue(year, PolicyEndDateYear);
		log.debug("Selecting Policystartdate Year as " + PolicyEndDateYear);
		Thread.sleep(2000);
		clib.select(month, PolicyEndDateMonth);
		log.debug("Selecting Policystartdate Month as " + PolicyEndDateMonth);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(PolicyEndDateDay) + "']")).click();
		log.debug("Entering PolicyEnddate  as " + PolicyEndDateDay);
		//firstinsuranceday.click();
		insureName.click();
		String InsureName = elib.getExcelData("Portability", 41, 1);
		clib.select(insureName, InsureName);
		log.debug("Entering InsureName as " + InsureName);
		Thread.sleep(4000);
		// clib.screenshotalert("previousinsurancepolicyPage");
		/*
		 * thirdSave.click(); BaseClass.driver.manage().timeouts().implicitlyWait(5,
		 * TimeUnit.SECONDS);
		 */

	}
	
	public void previousinsurancepolicy() throws Throwable {
		String Year = elib.getExcelData("Portability", 40, 2);
		String Month = elib.getExcelData("Portability", 40, 1);
		String day = elib.getExcelData("Portability", 40, 3);
		Thread.sleep(9000);
		thirdSave.click();
		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;
		Boolean is_valid = (Boolean) js.executeScript("return arguments[0].checkValidity();",
				firstinsurancepolicystarted);
		String message = (String) js.executeScript("return arguments[0].validationMessage;",
				firstinsurancepolicystarted);
		System.out.println(firstinsurancepolicystarted.isDisplayed());
		Thread.sleep(9000);
		firstinsurancepolicystarted.click();
		clib.selectByValue(year, Year);
		log.debug("In Previousinsurance policy selecting Year as " + Year);
		Thread.sleep(2000);
		clib.select(month, Month);
		log.debug("In Previousinsurance policy selecting Month as " + Month);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("In Previousinsurance policy selecting date as " + day);
		// firstinsuranceday.click();
		String Expiringpolicynumber = elib.getExcelData("Portability", 39, 1);
		expiringPolicyNumber.click();
		expiringPolicyNumber.sendKeys(Expiringpolicynumber);
		log.debug("Entering Expiringpolicy number as " + Expiringpolicynumber);

		policystartdate.click();
		String PolicyStartDateMonth = elib.getExcelData("Portability", 83, 1);
		String PolicyStartDateYear = elib.getExcelData("Portability", 83, 2);
		String PolicyStartDateDay = elib.getExcelData("Portability", 83, 3);
		clib.selectByValue(year, PolicyStartDateYear);
		log.debug("Selecting Policystartdate Year as " + PolicyStartDateYear);
		Thread.sleep(2000);
		clib.select(month, PolicyStartDateMonth);
		log.debug("Selecting Policystartdate Month as " + PolicyStartDateMonth);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(PolicyStartDateDay) + "']")).click();
		log.debug("Entering PolicyStartdate date  as " + PolicyStartDateDay);
		
		firstinsuranceday.click();
		policyEnddate.click();
		String PolicyEndDateMonth = elib.getExcelData("Portability", 84, 1);
		String PolicyEndDateYear = elib.getExcelData("Portability", 84, 2);
		String PolicyEndDateDay = elib.getExcelData("Portability", 84, 3);
		clib.selectByValue(year, PolicyEndDateYear);
		log.debug("Selecting Policystartdate Year as " + PolicyEndDateYear);
		Thread.sleep(2000);
		clib.select(month, PolicyEndDateMonth);
		log.debug("Selecting Policystartdate Month as " + PolicyEndDateMonth);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(PolicyEndDateDay) + "']")).click();
		log.debug("Entering PolicyEnddate  as " + PolicyEndDateDay);
		firstinsuranceday.click();
		insureName.click();
		String InsureName = elib.getExcelData("Portability", 41, 1);
		clib.select(insureName, InsureName);
		log.debug("Entering InsureName as " + InsureName);
		Thread.sleep(4000);
		// clib.screenshotalert("previousinsurancepolicyPage");
		/*
		 * thirdSave.click(); BaseClass.driver.manage().timeouts().implicitlyWait(5,
		 * TimeUnit.SECONDS);
		 */

	}

	public void previousinsurancepolicysave() {

		thirdSave.click();

		BaseClass.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	public void Previousyearpolicy() throws Throwable {
		Thread.sleep(6000);
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", fourthSave);
		Thread.sleep(2000);
		fourthSave.click();
		Thread.sleep(4000);
		clib.screenshotalert("PreviousyearpolicyErrormessage");
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		log.debug("Previouspolicy Alert message is displayed " + errorMessagePopUp);
		Thread.sleep(2000);

		al.accept();
		browseprevoiuspolicy.sendKeys("D:\\test_output\\SampleImage.JPG");
		log.debug("Previouspolicy document is uploaded");

		Thread.sleep(6000);
		// BaseClass.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		JavascriptExecutor je1 = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", fourthSave);
		//clib.screenshotalert("previousinsurancepolicyPage");
		Thread.sleep(2000);
		

	}
	public void previouspolicysave() {
		
		fourthSave.click();
	}

	public void Renewalnotice() throws InterruptedException, IOException, AWTException {
		Thread.sleep(5000);
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", fifthSave);
		Thread.sleep(2000);
		fifthSave.click();
		Thread.sleep(4000);
		clib.screenshotalert("RenewalnoticeErrormessage");
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		log.debug("For Renewal Alert message is displayed " + errorMessagePopUp);
		Thread.sleep(2000);

		al.accept();

		browserenewalnotice.sendKeys("D:\\test_output\\sample.Pdf");
		log.debug("Renewalnotice is uploaded");

		Thread.sleep(2000);
		JavascriptExecutor je1 = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", fifthSave);
		//clib.screenshotalert("RenewalnoticePage");
		Thread.sleep(2000);
		
	}
	public void renewalnoticesave() {
		fifthSave.click();

	}

	public void Claims() throws InterruptedException, IOException, AWTException {
		Thread.sleep(2000);
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		Thread.sleep(2000);
	}
	public void claimsave() throws InterruptedException {
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", sixthSave);
		Thread.sleep(4000);
		//clib.screenshotalert("ClaimsPage");
		sixthSave.click();
		log.debug("Clams is saved");

	}

	public void Next() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", next);
		Thread.sleep(4000);
		next.click();
		log.debug("Redirected to Medical History page");

	}

	public void Healthquestions() throws IOException, AWTException, InterruptedException {
		healthquestionsnext.click();
		Thread.sleep(2000);
		clib.screenshotalert("RenewalnoticeErrormessage");
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		log.debug("Alert message is displayed " + errorMessagePopUp);
		Thread.sleep(2000);

		al.accept();
		healthanswer1.click();
		healthanswer2.click();
		healthanswer3.click();
		healthanswer4.click();
		Thread.sleep(3000);
		js.executeScript("arguments[0].scrollIntoView();", standinginstruction);
		standinginstruction.click();
		Thread.sleep(2000);
		log.debug("Answered all the questions");
		// clib.screenshotalert("MedicalHistoryPage");

	}

	public void SubmitHealthquestions() throws IOException, AWTException {

		healthquestionsnext.click();
		log.debug("Redirected to Proposal Summary Page");
		// clib.Passscreenshot("ProposalSummary", "ProposalSummaryPage");
	}

	public void SubmitPolicy() throws InterruptedException, IOException, AWTException {
		clib.waitForPageToLoad();
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", Confirmpolicy);

		// ((JavascriptExecutor)
		// BaseClass.driver).executeScript("arguments[0].click();",Confirmpolicy);
		Thread.sleep(5000);
		clib.waitForPageToLoad();

	}

	public void Proposalsummary() throws Throwable {
		String Pancard =elib.getExcelData("Portability",85,1);
		js.executeScript("arguments[0].scrollIntoView(true);", pancard);
		Thread.sleep(2000);
		pancard.click();
		pancard.sendKeys("FADPS2176H");
		log.debug("Pancared is entered");
		
	}
	public void confirmPolicy() throws InterruptedException {

		Confirmpolicy.click();
		log.debug("Redirected to Proposalsummary2");

	}

	public void Proceedtopay() throws InterruptedException {

		js.executeScript("arguments[0].scrollIntoView(true);", ProceedtoPay);

		// ((JavascriptExecutor)
		// BaseClass.driver).executeScript("arguments[0].click();",ProceedtoPay);
		ProceedtoPay.click();
		log.debug(" Redirected to payment page");

	}

}
