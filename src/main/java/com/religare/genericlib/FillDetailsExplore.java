package com.religare.genericlib;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FillDetailsExplore {

	WebDriverCommonLib clib = new WebDriverCommonLib();

	Logger log = Logger.getLogger("devpinoyLogger");
	// WebDriverWait wait = new WebDriverWait(BaseClass.driver, 10);
	ExcelLib elib = new ExcelLib();

	WebDriverWait wait = new WebDriverWait(BaseClass.driver, 10);

	JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;

	@FindBy(id = "ValidPanCard")

	WebElement panCard;

	// @FindBy(xpath ="//div[@id='proposer_details_divid']/div[1]/div[1]/label[1]")

	@FindBy(className = "maleIcon ")

	WebElement maleIcon;

	// @FindBy(xpath="//label[@class = 'femaleIcon' and @for =
	// 'ValidTitle-2']")//label[@class='maleIcon']

	// @FindBy(xpath =".//*[@id='proposer_details_divid']/div[1]/div[1]/label[2]")

	// @FindBy (xpath =
	// "//div[@id='proposer_details_divid']/div[1]/div[1]/label[2]")

	@FindBy(className = "femaleIcon ")

	WebElement femaleIcon;

	@FindBy(id = "enhanceproposal")
	WebElement EnchanceNext;

	@FindBy(id = "joytomorrowproposal")
	WebElement joyTomorrow;

	@FindBy(id = "joyproposal") // studentinsured
	WebElement joyToday;

	@FindBy(id = "proposal")
	WebElement SecureNext;

	@FindBy(id = "studentproposal")
	WebElement StudentexploreNext;

	/////////////////////////////////////////////////////////
	@FindBy(id = "assurecare")
	WebElement TotalassurePremium;

	@FindBy(id = "enhancePremiumResultOne")
	WebElement TotalPremium;

	@FindBy(id = "travelPremium")
	WebElement TotalExplorePremium;

	@FindBy(id = "joyprimiumone")
	WebElement TotaljoyPremium;

	@FindBy(id = "secure_premium_val")
	WebElement TotalsecurePremium;

	@FindBy(id = "enhancePremiumResultRroposalOne")
	WebElement TotalstudentPremium;

	@FindBy(id = "enhanceinsured")
	WebElement Nextenchanceinsuredscreen;

	@FindBy(id = "joytomorrowinsured")
	WebElement Nextjoytomorrowinsuredscreen;

	@FindBy(id = "joyinsured")
	WebElement Nextjoytodayinsuredscreen;

	@FindBy(id = "studentinsured")
	WebElement studentInsuredNext;

	@FindBy(name = "DOB")
	WebElement dob;
	@FindBy(id = "ValidFName")
	public
	WebElement fillFirstNameTextBox;

	@FindBy(id = "ValidLName")
	public WebElement fillLastNameTextBox;

	@FindBy(id = "datepicker")
	public WebElement datePicker;

	

	// @FindBy(xpath = "//a[@class='ui-state-default' and text()='20']")
	//
	// WebElement pickDate;

	@FindBy(id = "ValidEmail")
	public WebElement emailTextBox;

	@FindBy(id = "NomineeName")
	public WebElement nomineeNameTextBox;

	@FindBy(id = "nomineeRelation")
	public WebElement nomineeRelationDropDown;

	@FindBy(id = "ValidAddressOne")
	public WebElement validAddressOneTextBox;

	@FindBy(id = "ValidAddressTwo")
	public WebElement validAdressTwoTextBox;

	@FindBy(id = "landmark")
	public WebElement landmarkTextBox;

	@FindBy(id = "ValidPinCode")
	public WebElement validpinCodeTextBox;

	@FindBy(id = "ValidCityName")
	public WebElement validCityNameDropDown;

	@FindBy(id = "submit_proposer_details")
	public WebElement submitBtn;

	@FindBy(name = "ValidMobileNumber")
	public WebElement validMobileNumber;

	@FindBy(id = "ValidStateName")
	public WebElement validStateNameTextbox;

	@FindBy(className = "ui-datepicker-month")
	WebElement monthPicker;

	@FindBy(className = "ui-datepicker-year")
	WebElement yearPicker;

	@FindBy(id = "ValidStateName")
	WebElement stateName;

	@FindBy(id = "errordisplay_proposer")
	WebElement errorMsgKinFil;

	@FindBy(id = "submit_insured_details")
	WebElement submitInsuredDetails;

	@FindBy(id = "ValidTitleError")
	WebElement genderError;

	@FindBy(id = "ValidFNameError")
	WebElement firstNameError;

	@FindBy(id = "ValidLNameError")
	WebElement lastNameError;

	@FindBy(id = "datepickerError")
	WebElement datePickerError;

	@FindBy(id = "ValidEmailError")
	WebElement emailError;

	@FindBy(id = "NomineeNameError")
	WebElement nomineeNameError;

	@FindBy(id = "ValidLandmarkError")
	WebElement landmarkError;

	@FindBy(id = "ValidPinCodeError")
	WebElement pinCodeError;

	@FindBy(id = "ValidAddressOneError")
	WebElement addressError;

	@FindBy(xpath = "relationCd-1")
	WebElement relationCDDropDown1;

	@FindBy(id = "titleCd-1")
	WebElement titleCD1;

	@FindBy(id = "firstNamecd-1")
	WebElement firstName1;

	@FindBy(id = "lastNamecd-1")
	WebElement lastName1;

	@FindBy(id = "datepickerCD-1")
	WebElement datePicker1;

	@FindBy(id = "heightFeet-1")
	WebElement heightFeet1;

	@FindBy(id = "heightInches-1")
	WebElement heightInches1;

	@FindBy(id = "weight-1")
	WebElement weight1;

	@FindBy(xpath = "//select[@id='relationCd-1']/option[@selected = 'selected']")
	WebElement selfPrimary;

	@FindBy(xpath = "//select[@class = 'styled']")
	WebElement relationSelect;

	@FindBy(xpath = "//select[@id='titleCd-1']/option")
	WebElement title1;

	@FindBy(xpath = "//select[@class = 'styled']/option")
	WebElement relationProposal;

	@FindBy(xpath = "//span[@class = 'stepTxt']")
	WebElement medHistProp;

	@FindBy(xpath = "//select[@class = 'styled']/option[1]")
	WebElement relationProposalByDefault;

	@FindBy(id = "lastNamecd-1Error")
	WebElement lastNamecdErrorInsured;

	@FindBy(id = "firstNamecd-1Error")
	WebElement firstNameErrorInsured;

	@FindBy(id = "datepickerCD-1Error")
	WebElement datePickerErrorInsured;

	@FindBy(id = "errordisplayJoyTommorrow1")
	WebElement kindlyFillErrorInsured;

	@FindBy(xpath = "//select[@class = 'styled ErrorField']")
	WebElement relationSelectError;

	public void Nextjoytodayinsuredscreen() {
		Nextjoytodayinsuredscreen.click();
		log.debug("Clicking on the Next button");
	}

	public void Nextjoytomorrowinsuredscreen() {
		Nextjoytomorrowinsuredscreen.click();
		log.debug("Clicking on the next button");
	}

	public void Nextenchanceinsuredscreen() {
		Nextenchanceinsuredscreen.click();
		log.debug("Clicking on the next button");
	}

	public void Submitenchance() {
		wait.until(ExpectedConditions.visibilityOf(EnchanceNext));
		// clib.waitForElementPresent(EnchanceNext);
		clib.waitForPageToLoad();
		EnchanceNext.click();
		log.debug("Clicking on the next button");

	}

	public void submitjoytomorrow() {
		wait.until(ExpectedConditions.visibilityOf(joyTomorrow));
		// clib.waitForElementPresent(joyTomorrow);
		clib.waitForPageToLoad();
		joyTomorrow.click();
		log.debug("clicking on the submit button");
	}

	public void submitjoytoday() {
		wait.until(ExpectedConditions.visibilityOf(joyToday));
		// clib.waitForElementPresent(joyToday);
		clib.waitForPageToLoad();
		joyToday.click();
		log.debug("clicking on the submit button");
	}

	public void sumbitsecure() {
		wait.until(ExpectedConditions.visibilityOf(SecureNext));
		// clib.waitForElementPresent(SecureNext);
		clib.waitForPageToLoad();
		SecureNext.click();
		log.debug("clicking on the submit button");
	}

	public void submitstudentexplore() {
		wait.until(ExpectedConditions.visibilityOf(StudentexploreNext));
		// clib.waitForElementPresent(StudentexploreNext);
		clib.waitForPageToLoad();
		StudentexploreNext.click();
		log.debug("clicking on the submit button");

	}

	public void submitsecure() {
		wait.until(ExpectedConditions.visibilityOf(SecureNext));
		// clib.waitForElementPresent(SecureNext);
		clib.waitForPageToLoad();
		SecureNext.click();
		log.debug("clicking on the submit button");
	}

	public void totalpremiumassure() throws Throwable {
		Thread.sleep(1500);
		String prmscreen2 = TotalassurePremium.getAttribute("innerHTML");
		// log.debug("The total primium one is " + prmscreen2);
	}

	public void totalpremium() throws Throwable {
		Thread.sleep(1500);
		String prmscreen2 = TotalPremium.getAttribute("innerHTML");
		// log.debug("The total primium one is " + prmscreen2);
	}

	public void totaljoypremium() throws Throwable {
		Thread.sleep(1500);
		String prmscreen2 = TotaljoyPremium.getAttribute("innerHTML");
		// log.debug("The total primium one is " + prmscreen2);
	}

	public void totalsecurepremium() throws InterruptedException {
		Thread.sleep(1500);
		String prmscreen2 = TotalsecurePremium.getAttribute("innerHTML");
		// log.debug("The total primium one is " + prmscreen2);
	}

	public void totalstudentpremium() throws Throwable {
		Thread.sleep(1500);
		String prmscreen2 = TotalstudentPremium.getAttribute("innerHTML");
		// log.debug("The total primium one is " + prmscreen2);
	}

	public void fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation() throws Throwable {

		String genderIcon = elib.getExcelData("fillExploreDetails", 0, 1);

		String firstName = elib.getExcelData("fillExploreDetails", 1, 1);

		String lastName = elib.getExcelData("fillExploreDetails", 2, 1);

		String day = elib.getExcelData("fillExploreDetails", 3, 1);
		String month = elib.getExcelData("fillExploreDetails", 3, 2);

		String year = elib.getExcelData("fillExploreDetails", 3, 3);
		String emailId = elib.getExcelData("fillExploreDetails", 4, 1);

		String nomineeName = elib.getExcelData("fillExploreDetails", 5, 1);

		String nomineeRelation = elib.getExcelData("fillExploreDetails", 6, 1);
		// wait.until(ExpectedConditions.visibilityOf(maleIcon));
		Thread.sleep(2000);

		if (genderIcon.equals("male")) {

			maleIcon.click();
		}

		else if (genderIcon.equalsIgnoreCase("female")) {

			femaleIcon.click();
		}
		log.debug("selecting the Gender as " + genderIcon);
		fillFirstNameTextBox.sendKeys(firstName);
		log.debug("Entering the first1 name as " + firstName);
		fillLastNameTextBox.sendKeys(lastName);
		log.debug("Entering the last1 name as " + lastName);

		dob.click();
		String DOB = elib.getExcelData("fillExploreDetails", 3, 1);
		dob.sendKeys(DOB);
		emailTextBox.click();
		log.debug("Entering the date of birth ");

		/*
		 * dob.click();
		 * 
		 * String dob1 = elib.getExcelData("InsuredDetails", 39, 1); dob.sendKeys(dob1);
		 * String value = dob.getAttribute("value"); //elib.setExcelData("Assure", 91,
		 * 2, value); // emailTextBox.click(); dob.clear();
		 * log.debug("Entering the date of birth "); emailTextBox.click();
		 */

		/*
		 * BaseClass.driver.findElement(By.id("datepicker")).click(); WebElement dp3 =
		 * BaseClass.driver.findElement(By.className("ui-datepicker-month")); Select d3
		 * = new Select(dp3); d3.selectByVisibleText(month);
		 * 
		 * WebElement dp4 =
		 * BaseClass.driver.findElement(By.className("ui-datepicker-year")); Select d4 =
		 * new Select(dp4); d4.selectByVisibleText(year);
		 * 
		 * BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day)
		 * + "']")).click(); log.debug("Entering the date of birth ");
		 */

		emailTextBox.sendKeys(emailId);
		log.debug("entering the emailID1 as " + emailId);
		nomineeNameTextBox.sendKeys(nomineeName);
		log.debug("entering the NoimeeName1 as " + nomineeName);
		if (nomineeRelation.equalsIgnoreCase("wife")) {

			jse.executeScript("arguments[0].value='WIFE'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("mother")) {

			jse.executeScript("arguments[0].value='MOTH'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("daughter")) {

			jse.executeScript("arguments[0].value='UDTR'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("son")) {

			jse.executeScript("arguments[0].value='SONM'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("father")) {

			jse.executeScript("arguments[0].value='FATH'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("bother")) {

			jse.executeScript("arguments[0].value='BOTH'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("sister")) {

			jse.executeScript("arguments[0].value='SIST'", nomineeRelationDropDown);
		}
		log.debug("Selecting the Relation as " + nomineeRelation);
	}

	public void fillDetailsAddress() throws Throwable {

		String addressOne = elib.getExcelData("fillExploreDetails", 7, 1);

		String addressTwo = elib.getExcelData("fillExploreDetails", 8, 1);

		String landmark = elib.getExcelData("fillExploreDetails", 9, 1);

		String pincode = elib.getExcelData("fillExploreDetails", 10, 1);

		String city = elib.getExcelData("fillExploreDetails", 11, 1);

		validAddressOneTextBox.sendKeys(addressOne);
		log.debug("entering the Addressone as " + addressOne);
		validAdressTwoTextBox.sendKeys(addressTwo);
		log.debug("entering the Addresstwo as " + addressTwo);
		landmarkTextBox.sendKeys(landmark);
		log.debug("entering the landmark as " + landmark);
		validpinCodeTextBox.sendKeys(pincode);

		log.debug("entering the pincode as " + pincode);
		Thread.sleep(2500);
		List<WebElement> Ele = BaseClass.driver.findElements(By.xpath("//Select[@id='ValidCityName']/option"));
		for (WebElement t : Ele) {

			System.out.println(t.getText());

			if (t.getText().equals(city)) {
				System.out.println(t.getText());
				t.click();

			}

			else {
				System.out.println("Not found");
			}
		}

		/*
		 * if (city.equalsIgnoreCase("Bangalore North")) {
		 * 
		 * jse.executeScript("arguments[0].value='Bangalore North'",
		 * validCityNameDropDown); }
		 * 
		 * else if (city.equalsIgnoreCase("Bangalore")) {
		 * 
		 * jse.executeScript("arguments[0].value='Bangalore'", validCityNameDropDown); }
		 * 
		 * else if (city.equalsIgnoreCase("BANGALORE NORTH")) {
		 * 
		 * jse.executeScript("arguments[0].value='BANGALORE NORTH'",
		 * validCityNameDropDown); }
		 */
		log.debug("Entering the city as " + city);
	}

	public void nextClick() throws InterruptedException {
		Thread.sleep(3000);
		// wait.until(ExpectedConditions.visibilityOf(submitBtn));
		// clib.waitForElementPresent(submitBtn);
		clib.waitForPageToLoad();
		submitBtn.click();
		log.debug("clicking on the submit button");
	}

	
	
	
	@FindBy(id = "passport-1")
	WebElement passport1;
	@FindBy(id = "passport-2")
	WebElement passport2;
	@FindBy(id = "passport-3")
	WebElement passport3;
	@FindBy(id = "passport-4")
	WebElement passport4;
	@FindBy(id = "passport-5")
	WebElement passport5;
	@FindBy(id = "passport-6")
	WebElement passport6;
	
	@FindBy(id = "relationCd-1")
	WebElement relation1;
	@FindBy(id = "relationCd-2")
	WebElement relation2;
	@FindBy(id = "relationCd-3")
	WebElement relation3;
	@FindBy(id = "relationCd-4")
	WebElement relation4;
	@FindBy(id = "relationCd-5")
	WebElement relation5;
	@FindBy(id = "relationCd-6")
	WebElement relation6;
	
	@FindBy(id = "titleCd-1")
	WebElement tite1;
	@FindBy(id = "titleCd-2")
	WebElement title2;
	@FindBy(id = "titleCd-3")
	WebElement title3;
	@FindBy(id = "titleCd-4")
	WebElement title4;
	@FindBy(id = "titleCd-5")
	WebElement title5;
	@FindBy(id = "titleCd-6")
	WebElement title6;
	
	@FindBy(id = "firstNamecd-1")
	WebElement firstname1;
	@FindBy(id = "firstNamecd-2")
	WebElement firstname2;
	@FindBy(id = "firstNamecd-3")
	WebElement firstname3;
	@FindBy(id = "firstNamecd-4")
	WebElement firstname4;
	@FindBy(id = "firstNamecd-5")
	WebElement firstname5;
	@FindBy(id = "firstNamecd-6")
	WebElement firstname6;
	
	@FindBy(id = "lastNamecd-1")
	WebElement lastname1;
	@FindBy(id = "lastNamecd-2")
	WebElement lastname2;
	@FindBy(id = "lastNamecd-3")
	WebElement lastname3;
	@FindBy(id = "lastNamecd-4")
	WebElement lastname4;
	@FindBy(id = "lastNamecd-5")
	WebElement lastname5;
	@FindBy(id = "lastNamecd-6")
	WebElement lastname6;
	
	@FindBy(id = "datepickerCD-1")
	WebElement datecal1;
	@FindBy(id = "datepickerCD-2")
	WebElement datecal2;
	@FindBy(id = "datepickerCD-3")
	WebElement datecal3;
	@FindBy(id = "datepickerCD-4")
	WebElement datecal4;
	@FindBy(id = "datepickerCD-5")
	WebElement datecal5;
	@FindBy(id = "datepickerCD-6")
	WebElement datecal6;
	
	
	@FindBy(id = "citizenshipCd-1")
	WebElement citizenship1;
	@FindBy(id = "citizenshipCd-2")
	WebElement citizenship2;
	@FindBy(id = "citizenshipCd-3")
	WebElement citizenship3;
	@FindBy(id = "citizenshipCd-4")
	WebElement citizenship4;
	@FindBy(id = "citizenshipCd-5")
	WebElement citizenship5;
	@FindBy(id = "citizenshipCd-6")
	WebElement citizenship6;
	

	@FindBy(xpath = "//select[@class ='ui-datepicker-month']")
	WebElement monthPickerDropDown;

	@FindBy(xpath = "//select[@class ='ui-datepicker-year']")
	WebElement yearPickerDropDown;
	
	
	
	public void insuredMember1() throws Throwable {
       
        
    
        
		String passport = elib.getExcelData("InsuredDetails", 0, 1);

		passport1.sendKeys(passport);
		log.debug("Entering the passportNumber as" + passport);

	}

	public void insuredMember2() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 1, 1);

		String firstName = elib.getExcelData("InsuredDetails", 22, 1);

		String lastName = elib.getExcelData("InsuredDetails", 29, 1);

		String day = elib.getExcelData("InsuredDetails", 36, 1);
		// String month = elib.getExcelData("InsuredDetails", 36, 2);
		String year = elib.getExcelData("InsuredDetails", 36, 3);
		String month = "Jan";
		String relation = elib.getExcelData("InsuredDetails", 15, 1);
		// String weight = elib.getExcelData("InsuredDetails", 43, 1);
		// String height = elib.getExcelData("InsuredDetails", 50, 1);
		// String inches = elib.getExcelData("InsuredDetails", 57, 1);
		//
		WebElement relation2 = BaseClass.driver.findElement(By.id("relationCd-2"));

		Select sel = new Select(relation2);

		sel.selectByValue(relation);
		log.debug("Selecting the relation2 as" + relation);
		BaseClass.driver.findElement(By.id("firstNamecd-2")).sendKeys(firstName);

		clib.waitForPageToLoad();
		log.debug("Entering the firstName2 " + firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-2")).sendKeys(lastName);
		log.debug("Entering the lastName2 " + lastName);

		BaseClass.driver.findElement(By.id("datepickerCD-2")).click();
		WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
		Select d3 = new Select(dp3);
		d3.selectByVisibleText(month);

		WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
		Select d4 = new Select(dp4);
		d4.selectByVisibleText(year);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the date of birth ");
		clib.waitForPageToLoad();
		BaseClass.driver.findElement(By.id("passport-2")).sendKeys(passport);
		log.debug("Entering the passport2 number " + passport);

	}

	public void insuredMember3() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 2, 1);

		String firstName = elib.getExcelData("InsuredDetails", 23, 1);

		String lastName = elib.getExcelData("InsuredDetails", 30, 1);

		String day = elib.getExcelData("InsuredDetails", 37, 1);
		String month = elib.getExcelData("InsuredDetails", 37, 2);
		String year = elib.getExcelData("InsuredDetails", 37, 3);
		String relation = elib.getExcelData("InsuredDetails", 16, 1);

		WebElement relation3 = BaseClass.driver.findElement(By.id("relationCd-3"));

		Select sel = new Select(relation3);

		sel.selectByValue(relation);
		log.debug("Selecting the relation3 as" + relation);
		BaseClass.driver.findElement(By.id("firstNamecd-3")).sendKeys(firstName);
		log.debug("Entering the firstName3 " + firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-3")).sendKeys(lastName);
		log.debug("Entering the lastname3 as " + lastName);
Thread.sleep(3000);
		BaseClass.driver.findElement(By.id("datepickerCD-3")).click();
		WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
		Select d3 = new Select(dp3);
		d3.selectByVisibleText(month);

		WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
		Select d4 = new Select(dp4);
		d4.selectByVisibleText(year);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the date of birth ");

		BaseClass.driver.findElement(By.id("passport-3")).sendKeys(passport);
		log.debug("Entering the passport Number3 as " + passport);

	}

	public void insuredMember4() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 3, 1);

		String firstName = elib.getExcelData("InsuredDetails", 24, 1);

		String lastName = elib.getExcelData("InsuredDetails", 31, 1);

		String day = elib.getExcelData("InsuredDetails", 38, 1);
		String month = elib.getExcelData("InsuredDetails", 38, 2);
		String year = elib.getExcelData("InsuredDetails", 38, 3);
		String relation = elib.getExcelData("InsuredDetails", 17, 1);

		WebElement relation4 = BaseClass.driver.findElement(By.id("relationCd-4"));

		Select sel = new Select(relation4);

		sel.selectByValue(relation);
		log.debug("Selecting the relation4 as" + relation);
		BaseClass.driver.findElement(By.id("firstNamecd-4")).sendKeys(firstName);
		log.debug("Entering the firstName4 as " + firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-4")).sendKeys(lastName);
		log.debug("Entering the lastName4 as " + lastName);
		Thread.sleep(3000);
		BaseClass.driver.findElement(By.id("datepickerCD-4")).click();
		WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
		Select d3 = new Select(dp3);
		d3.selectByVisibleText(month);

		WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
		Select d4 = new Select(dp4);
		d4.selectByVisibleText(year);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the date of birth ");

		BaseClass.driver.findElement(By.id("passport-4")).sendKeys(passport);
		log.debug("Entering the passport number4 as " + passport);

	}

	public void insuredMember5() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 4, 1);

		String firstName = elib.getExcelData("InsuredDetails", 25, 1);

		String lastName = elib.getExcelData("InsuredDetails", 32, 1);

		String day = elib.getExcelData("InsuredDetails", 39, 1);
		String month = elib.getExcelData("InsuredDetails", 39, 2);
		String year = elib.getExcelData("InsuredDetails", 39, 3);
		String relation = elib.getExcelData("InsuredDetails", 18, 1);

		WebElement relation5 = BaseClass.driver.findElement(By.id("relationCd-5"));

		Select sel = new Select(relation5);

		sel.selectByValue(relation);
		log.debug("Selecting the relation5 as" + relation);
		BaseClass.driver.findElement(By.id("firstNamecd-5")).sendKeys(firstName);
		log.debug("Entering the firstName5 as " + firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-5")).sendKeys(lastName);
		log.debug("Entering the lastName5 as " + lastName);
		Thread.sleep(3000);
		BaseClass.driver.findElement(By.id("datepickerCD-5")).click();
		WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
		Select d3 = new Select(dp3);
		d3.selectByVisibleText(month);

		WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
		Select d4 = new Select(dp4);
		d4.selectByVisibleText(year);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the date of birth ");

		BaseClass.driver.findElement(By.id("passport-5")).sendKeys(passport);
		log.debug("Entering the passport Number5 as " + passport);

	}

	public void insuredMember6() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 5, 1);

		String firstName = elib.getExcelData("InsuredDetails", 26, 1);

		String lastName = elib.getExcelData("InsuredDetails", 33, 1);

		String day = elib.getExcelData("InsuredDetails", 40, 1);
		String month = elib.getExcelData("InsuredDetails", 40, 2);
		String year = elib.getExcelData("InsuredDetails", 40, 3);
		// String weight = elib.getExcelData("InsuredDetails", 47, 1);
		// String height = elib.getExcelData("InsuredDetails", 54, 1);
		// String inches = elib.getExcelData("InsuredDetails", 61, 1);
		String relation = elib.getExcelData("InsuredDetails", 19, 1);

		WebElement relation6 = BaseClass.driver.findElement(By.id("relationCd-6"));

		Select sel = new Select(relation6);

		sel.selectByValue(relation);
		log.debug("Selecting the relation6 as" + relation);
		BaseClass.driver.findElement(By.id("firstNamecd-6")).sendKeys(firstName);
		log.debug("Entering the firstName6 as " + firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-6")).sendKeys(lastName);
		log.debug("Entering the LastName6 as " + firstName);
		Thread.sleep(3000);
		BaseClass.driver.findElement(By.id("datepickerCD-6")).click();
		WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
		Select d3 = new Select(dp3);
		d3.selectByVisibleText(month);

		WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
		Select d4 = new Select(dp4);
		d4.selectByVisibleText(year);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the date of birth ");

		BaseClass.driver.findElement(By.id("passport-6")).sendKeys(passport);
		log.debug("Entering the passport Number6 as " + passport);
		// WebElement heights = BaseClass.driver.findElement(By.id("heightFeet-6"));
		// Select h = new Select(heights);
		// h.selectByVisibleText(height);
		// // log.debug("Entering the Height as" +height);
		// WebElement inche = BaseClass.driver.findElement(By.id("heightInches-6"));
		// Select i = new Select(inche);
		// i.selectByVisibleText(inches);
		// //log.debug("Entering the inches as" +inches);
		// BaseClass.driver.findElement(By.id("weight-6")).sendKeys(weight);
		// // log.debug("Entering the weight as" +weight);
		// BaseClass.driver.findElement(By.id("submit_insured_details")).click();}
		// catch(Exception e) {e.getMessage();}

	}

	public void next() throws IOException {

		BaseClass.driver.findElement(By.id("submit_insured_details")).click();
		log.debug("Clicking on the submit button ");
	}

	public void panCard() throws Throwable {
try {
		String pancard = elib.getExcelData("fillExploreDetails", 12, 1);
		panCard.sendKeys(pancard);
		log.debug("entering the pancard number as " + pancard);}catch(Exception e) {}
	}

	public void secureinsuredpage() throws Throwable {
		String ocp = elib.getExcelData("InsuredDetails", 63, 1);
		WebElement oc = BaseClass.driver.findElement(By.id("ValidOccupationAddClass-1"));
		Select op = new Select(oc);
		op.selectByVisibleText(ocp);
		log.debug("The occupation as " + ocp);
		if (ocp.equals("Other")) {
			BaseClass.driver.findElement(By.id("customerOccupation")).sendKeys("business");
		} else {

		}
		BaseClass.driver.findElement(By.id("insured")).click();
		log.debug("Clicking on the Submit button ");
	}

	public void studentExploreInsuredpage() throws Throwable {

		// String prm2 =
		// BaseClass.driver.findElement(By.id("enhancePremiumResultRroposalOne")).getAttribute("innerHTML");
		// log.debug("The Enhance Premium Resultone on Screen3 is " + prm2);
		// try {
		// BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();
		//
		// } catch (Exception e) {
		// System.out.println(e.getMessage());
		// }
		String clg = elib.getExcelData("InsuredDetails", 65, 1);
		String course = elib.getExcelData("InsuredDetails", 66, 1);
		String clgmail = elib.getExcelData("InsuredDetails", 67, 1);
		String Nation = elib.getExcelData("InsuredDetails", 68, 1);
		String spr = elib.getExcelData("InsuredDetails", 69, 1);
		String sponserYear = elib.getExcelData("InsuredDetails", 70, 1);
		String sponsermonth = elib.getExcelData("InsuredDetails", 71, 1);
		String Sponserday = elib.getExcelData("InsuredDetails", 72, 1);
		String relation = elib.getExcelData("InsuredDetails", 73, 1);

		BaseClass.driver.findElement(By.id("NameofEducationalInstitute")).sendKeys(clg);
		log.debug("Name of Educational Institute is " + clg);
		BaseClass.driver.findElement(By.id("EducationalCourseDetails")).sendKeys(course);
		log.debug("Educational Course is " + course);
		BaseClass.driver.findElement(By.id("EducationalInstituteAddress")).sendKeys(clgmail);
		log.debug("Educational Institute Mail ID is " + clgmail);
		BaseClass.driver.findElement(By.id("Country")).sendKeys(Nation);
		log.debug("Country is " + Nation);

		BaseClass.driver.findElement(By.id("SponsorsName")).sendKeys(spr);
		log.debug("Sponsors Name is " + spr);

		WebElement selectDate = BaseClass.driver.findElement(By.id("SponsorDob"));
		selectDate.click();
		WebElement selyear = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
		Select sel7 = new Select(selyear);
		sel7.selectByVisibleText(sponserYear);
		WebElement selmnth = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
		Select sel8 = new Select(selmnth);
		sel8.selectByVisibleText(sponsermonth);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(Sponserday) + "']")).click();
		log.debug("Sponsor date of birth ");

		WebElement sprln = BaseClass.driver.findElement(By.id("sponserrelation"));
		Select sel3 = new Select(sprln);
		sel3.selectByVisibleText(relation);
		log.debug("Selecting the Sponsor Relation is " + relation);
		Thread.sleep(1000);
		BaseClass.driver.findElement(By.id("studentAdditional")).click();
		log.debug("Clicking on the next button ");

	}

	public void studentInsuredNext() throws IOException {
		// clib.Passscreenshot("StudentExploreInsured", "StudentExploreInsuredPage");
		studentInsuredNext.click();
		log.debug("Clicking on the submit button");
	}

	public void isInputPresentInProposalDetailsPage() throws Throwable {

		Boolean maleIconIsPresent = maleIcon.isDisplayed();
		elib.setExcelDataBoolean("Assure", 61, 3, maleIconIsPresent);
		log.debug("In Proposerdetails Maleicon is present " + maleIconIsPresent);
		Boolean femaleIconIsPresent = femaleIcon.isDisplayed();
		elib.setExcelDataBoolean("Assure", 62, 3, femaleIconIsPresent);
		log.debug("In Proposerdetails FeMaleicon is present " + femaleIconIsPresent);
		System.out.println("In Proposerdetails Maleicon is present " + maleIconIsPresent);
		log.debug("In Proposerdetails FeMaleicon is present " + femaleIconIsPresent);
		System.out.println("In Proposerdetails FeMaleicon is present " + femaleIconIsPresent);

		Boolean fillFirstNameTextBoxIsPresentEditQuote = fillFirstNameTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 63, 3, fillFirstNameTextBoxIsPresentEditQuote);
		log.debug("In Proposerdetails FirstName Textbox is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean fillLastNameTextBoxIsPresent = fillLastNameTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 64, 3, fillLastNameTextBoxIsPresent);
		log.debug("In Proposerdetails LastName Textbox is present " + fillLastNameTextBoxIsPresent);

		Boolean validMobileNumberIsPresent = validMobileNumber.isDisplayed();
		elib.setExcelDataBoolean("Assure", 65, 3, validMobileNumberIsPresent);
		log.debug("In Proposerdetails MobileNumber Textbox is present " + validMobileNumberIsPresent);

		Boolean dobIsPresent = dob.isDisplayed();
		elib.setExcelDataBoolean("Assure", 66, 3, dobIsPresent);
		log.debug("In Proposerdetails DOB is present " + dobIsPresent);

		Boolean emailTextBoxIsPresentEditQuote = emailTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 67, 3, emailTextBoxIsPresentEditQuote);
		log.debug("In Proposerdetails email Textbox is present " + emailTextBoxIsPresentEditQuote);

		Boolean nomineeNameTextBoxIsPresent = nomineeNameTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 68, 3, nomineeNameTextBoxIsPresent);
		log.debug("In Proposerdetails nomineeNameTextbox is present " + nomineeNameTextBoxIsPresent);

		Boolean nomineeRelationDropDownIsPresent = nomineeRelationDropDown.isDisplayed();
		elib.setExcelDataBoolean("Assure", 69, 3, nomineeRelationDropDownIsPresent);
		log.debug("In Proposerdetails NomineeRelation Dropdown is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean validAddressOneTextBoxIsPresent = validAddressOneTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 70, 3, validAddressOneTextBoxIsPresent);
		log.debug("In Proposerdetails Addressone Textbox is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean validAdressTwoTextBoxIsPresent = validAdressTwoTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 71, 3, validAdressTwoTextBoxIsPresent);
		log.debug("In Proposerdetails Addresstwo Textbox is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean landmarkTextBoxIsPresent = landmarkTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 72, 3, landmarkTextBoxIsPresent);
		log.debug("In Proposerdetails Landmark TextBox Textbox is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean validpinCodeTextBoxIsPresent = validpinCodeTextBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 73, 3, validpinCodeTextBoxIsPresent);
		log.debug("In Proposerdetails Pincode TextBox is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean validCityNameDropDownIsPresent = validCityNameDropDown.isDisplayed();
		elib.setExcelDataBoolean("Assure", 74, 3, validCityNameDropDownIsPresent);
		log.debug("In Proposerdetails City dropdown is present " + fillFirstNameTextBoxIsPresentEditQuote);

		Boolean validStateNameTextboxIsPresent = validStateNameTextbox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 75, 3, validStateNameTextboxIsPresent);
		log.debug("In Proposerdetails State is present " + fillFirstNameTextBoxIsPresentEditQuote);
		System.out.println("In Proposerdetails FirstName Textbox is present " + fillFirstNameTextBoxIsPresentEditQuote);;
	}

	public void verifyRadioBtn() throws Throwable {

		String genderIcon = elib.getExcelData("fillExploreDetails", 0, 1);

		Boolean maleIconIsEnabled = maleIcon.isEnabled();
		elib.setExcelDataBoolean("Assure", 80, 3, maleIconIsEnabled);
		log.debug("In Proposaldetails Maleicon is enabled " + maleIconIsEnabled);
		// 61
		Boolean femaleIconIsEnabled = femaleIcon.isEnabled();
		elib.setExcelDataBoolean("Assure", 81, 3, femaleIconIsEnabled);
		log.debug("In Proposaldetails Femaleicon is enabled " + femaleIconIsEnabled);

	System.out.println("In Proposaldetails Maleicon is enabled " + maleIconIsEnabled);
		if (genderIcon.equals("male")) {
			jse.executeScript("arguments[0].click();", maleIcon);
			String genderTextMale = maleIcon.getAttribute("class");
			elib.setExcelData3("Assure", 82, 3, genderTextMale);
			log.debug("In Proposaldetails Maleicon  status is " + genderTextMale);
			System.out.println("In Proposaldetails Maleicon  status is " + genderTextMale);
		} else if (genderIcon.equalsIgnoreCase("female")) {
			femaleIcon.click();
			String genderTextFemale = femaleIcon.getAttribute("class");
			elib.setExcelData3("Assure", 82, 3, genderTextFemale);
			log.debug("In Proposaldetails Femaleicon status is " + genderTextFemale);
			System.out.println("In Proposaldetails Femaleicon status is " + genderTextFemale);
		}
	}

	public void verifyFirstName() throws Throwable {

		String firstName = elib.getExcelData("fillExploreDetails", 1, 1);
		fillFirstNameTextBox.sendKeys(firstName);
		clib.waitForPageToLoad();
		String value = fillFirstNameTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 85, 3, value);
		log.debug("In Proposerdetails Firstname is entered as " + value);
		System.out.println("In Proposerdetails Firstname is entered as " + value);
	}

	public void verifyLastName() throws Throwable {
		clib.waitForPageToLoad();
		String lastName = elib.getExcelData("fillExploreDetails", 2, 1);
		fillLastNameTextBox.sendKeys(lastName);
		String value = fillLastNameTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 86, 3, value);
		log.debug("In Proposerdetails Lastname is entered as " + value);
		System.out.println("In Proposerdetails Lastname is entered as " + value);

	}

	public void verifyMobileNumber() throws Throwable {
		String mobileNumber = validMobileNumber.getAttribute("value");
		elib.setExcelData3("Assure", 87, 3, mobileNumber);
		log.debug("In Proposerdetails Mobilenumber is entered as " + mobileNumber);
	}

	public void clearMobileNumberDataAndReset() throws Throwable {
		validMobileNumber.clear();
		String mobileNum = elib.getExcelData("testSpecificData", 0, 1);
		validMobileNumber.sendKeys(mobileNum);
		String value = validMobileNumber.getAttribute("value");
		elib.setExcelData3("Assure", 88, 3, value);
		log.debug("In Proposerdetails Mobile Number is entered as " + value);
		System.out.println("In Proposerdetails Mobile Number is entered as " + value);

	}

	public void verifyDOB() throws Throwable {
		dob.click();
		String dobVerify = dob.getAttribute("value");
		System.out.println(dobVerify);
		String dob1 = elib.getExcelData("InsuredDetails", 38, 4);
		dob.sendKeys(dob1);
		String value = dob.getAttribute("value");
		elib.setExcelData3("Assure", 91, 3, value);
		log.debug("In Proposerdetails DOB is entered as " + value);

		System.out.println("In Proposerdetails DOB is entered as " + value);
		// emailTextBox.click();
		dob.clear();
		emailTextBox.click();
	}

	public void verifyDOBSelect() throws Throwable {
		String month = elib.getExcelData("fillExploreDetails", 18, 1);
		String year = elib.getExcelData("fillExploreDetails", 19, 1);
		String day = elib.getExcelData("fillExploreDetails", 17, 1);

		dob.click();
		clib.select(monthPicker, month);

		clib.select(yearPicker, year);

		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		String value = dob.getAttribute("value");
		elib.setExcelData3("Assure", 92, 3, value);
		log.debug("In Proposerdetails Calenderdate  is selecting as " + value);

		System.out.println("In Proposerdetails Calenderdate  is selecting as " + value);
	}

	public void verifyEmailAddress() throws Throwable {
		String emailId = elib.getExcelData("fillExploreDetails", 4, 1);
		emailTextBox.sendKeys(emailId);
		String value = emailTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 97, 3, value);
		log.debug("In Proposerdetails Email Address is entered as " + value);

		System.out.println("In Proposerdetails Email Address is entered as " + value);
	}

	public void verifyNomineeTextbox() throws Throwable {
		String nomineeName = elib.getExcelData("fillExploreDetails", 5, 1);
		nomineeNameTextBox.sendKeys(nomineeName);
		String value = nomineeNameTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 95, 3, value);
		log.debug("In Proposerdetails NomineeName is selecting as " + value);
		System.out.println("In Proposerdetails NomineeName is selecting as " + value);
	}

	public void valueOfDropDownAndIsClickable() throws Throwable {

		nomineeRelationDropDown.click();

		Boolean relationDropDownIsClickable = nomineeRelationDropDown.isEnabled();
		elib.setExcelDataBoolean("Assure", 101, 3, relationDropDownIsClickable);
		log.debug("In Proposerdetails NomineeRelation is clickable " + relationDropDownIsClickable);
		System.out.println("In Proposerdetails NomineeRelation is clickable " + relationDropDownIsClickable);
		String valueInnomineeRelationDropDown = nomineeRelationDropDown.getText();
		elib.setExcelData3("Assure", 102, 3, valueInnomineeRelationDropDown);
		log.debug("In Proposerdetails Following NomineeRelations are " + valueInnomineeRelationDropDown);
		System.out.println("In Proposerdetails Following NomineeRelations are " + valueInnomineeRelationDropDown);
	}

	public void verifyRelationDropDown() throws Throwable {

		String nomineeRelation = elib.getExcelData("fillExploreDetails", 6, 1);

		Select sel = new Select(nomineeRelationDropDown);
		// clib.select(ageDropDown9, age);
		sel.selectByValue(nomineeRelation);

		WebElement getSelectedOptions = sel.getFirstSelectedOption();
		String selectedOptions = getSelectedOptions.getText();
		elib.setExcelData3("Assure", 104, 3, selectedOptions);
		log.debug("In Proposerdetails Relation is selecting as " + selectedOptions);
		System.out.println("In Proposerdetails Relation is selecting as " + selectedOptions);
	}

	public void verifFillAddressOne() throws Throwable {
		String addressOne = elib.getExcelData("fillExploreDetails", 7, 1);
		validAddressOneTextBox.sendKeys(addressOne);
		String value = validAddressOneTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 106, 3, value);
		log.debug("In Proposerdetails FirstAddress is entered as " + value);
		System.out.println("In Proposerdetails FirstAddress is entered as " + value);
	}

	public void verifFillAddressTwo() throws Throwable {
		String addressTwo = elib.getExcelData("fillExploreDetails", 8, 1);
		validAdressTwoTextBox.sendKeys(addressTwo);
		String value = validAdressTwoTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 108, 3, value);
		log.debug("In Proposerdetails SecondAddress is entered as " + value);
		System.out.println("In Proposerdetails SecondAddress is entered as " + value);
	}

	public void verifyLandmark() throws Throwable {
		String landmark = elib.getExcelData("fillExploreDetails", 9, 1);
		landmarkTextBox.sendKeys(landmark);
		String value = landmarkTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 110, 3, value);
		log.debug("In Proposerdetails Landmark is entered as " + value);

		System.out.println("In Proposerdetails Landmark is entered as " + value);
	}

	public void verifyPinCode() throws Throwable {
		String pincode = elib.getExcelData("fillExploreDetails", 10, 1);
		validpinCodeTextBox.sendKeys(pincode);
		String value = validpinCodeTextBox.getAttribute("value");
		elib.setExcelData3("Assure", 112, 3, value);
		log.debug("In Proposerdetails Pincode is entered as " + value);

		System.out.println("In Proposerdetails Pincode is entered as " + value);

	}

	public void verifyStateAutoPinCode() throws Throwable {
		clib.waitForPageToLoad();
		Thread.sleep(600);
		String stateNam = stateName.getAttribute("value");
		elib.setExcelData3("Assure", 114, 3, stateNam);
		log.debug("In Proposerdetails Statename is entered as " + stateNam);

		System.out.println("In Proposerdetails Statename is entered as " + stateNam);

	}

	public void verifyErrorMessage() throws Throwable {
		submitBtn.click();
                clib.Passscreenshot("Assureerrormessage","AssureerrormessagePage");
		String errorMsg = errorMsgKinFil.getText();
		log.debug("In Proposerdetails Capturing error message is  " + errorMsg);

		elib.setExcelData3("Assure", 116, 3, errorMsg);
		String errorMsgGender = genderError.getText();
		log.debug("In Proposerdetails Capturing error message for Gender is " + errorMsgGender);
		System.out.println("In Proposerdetails Capturing error message is  " + errorMsg);
		elib.setExcelData3("Assure", 117, 3, errorMsgGender);
		String errorMsgFirstName = firstNameError.getText();
		elib.setExcelData3("Assure", 118, 3, errorMsgFirstName);
		log.debug("In Proposerdetails Capturing error message for FirstName is " + errorMsgFirstName);
		String errorMsgLastName = lastNameError.getText();
		elib.setExcelData3("Assure", 119, 3, errorMsgLastName);
		log.debug("In Proposerdetails  Capturing error message for Lastname is " + errorMsgLastName);
		String errorMsgDOB = datePickerError.getText();
		elib.setExcelData3("Assure", 120, 3, errorMsgDOB);
		log.debug("In Proposerdetails  Capturing error message for DOB is " + errorMsgDOB);
		String errorMsgEmail = emailError.getText();
		elib.setExcelData3("Assure", 121, 3, errorMsgEmail);
		log.debug("In Proposerdetails  Capturing error message for Email is" + errorMsgEmail);
		String errorMsgNomineeName = nomineeNameError.getText();
		elib.setExcelData3("Assure", 122, 3, errorMsgNomineeName);
		log.debug("In Proposerdetails Capturing error message for NomineeName is " + errorMsgNomineeName);
		String errorMsgAddress = addressError.getText();
		elib.setExcelData3("Assure", 123, 3, errorMsgAddress);
		log.debug("In Proposerdetails Capturing error message for Address is " + errorMsgAddress);
		String errorMsgLandmark = landmarkError.getText();
		elib.setExcelData3("Assure", 124, 3, errorMsgLandmark);
		log.debug("In Proposerdetails Capturing error message for Landmark is " + errorMsgLandmark);
		String errorMsgPinCode = errorMsgKinFil.getText();
		elib.setExcelData3("Assure", 125, 3, errorMsgPinCode);
		log.debug("In Proposerdetails Capturing error message for Pincode is" + errorMsgPinCode);
	}


	public void submitBTN() throws Throwable {

		submitBtn.click();
		log.debug("Clicking the proposerdetails next button");
		String currentTitle = BaseClass.driver.getCurrentUrl();
		elib.setExcelData3("Assure", 127, 3, currentTitle);
		log.debug("In Insured detils Capturing Title as " + currentTitle);

		System.out.println("In Insured detils Capturing Title as " + currentTitle);

	}

	public void citySelect() throws Throwable {
		String city = elib.getExcelData("fillExploreDetails", 11, 1);
		List<WebElement> Ele = BaseClass.driver.findElements(By.xpath("//Select[@id='ValidCityName']/option"));
		for (WebElement t : Ele) {
			// System.out.println(t.getText());
			if (t.getText().equals(city)) {
				// System.out.println(t.getText());
				t.click();
				log.debug("Selecting the city " +city);
				System.out.println("Selecting the city " +city);
			}
		}
	}
	public void verifyinsureddetailpage() {
		String currentURl = BaseClass.driver.getCurrentUrl();
		if (currentURl.equals("https://rhicluat.religarehealthinsurance.com/travel-insurance-explore/explore-filldetails"))
		{
			System.out.println("Redirected to the Insured detail page of explore");
			log.debug("Redirected to the Insured detail page of explore");
		}else {
			System.out.println(" Failed to redirect the Insured detail page of explore");
			log.debug("Failed to redirect the Insured detail page of explore");
	
	}}

	public void verifySelfRelationDropDown() throws Throwable {

		String value = selfPrimary.getAttribute("value");
		elib.setExcelData3("Assure", 143, 3, value);
		log.debug("In Insured details selecting Relation as " + value);

		// Select sel = new Select(relationCDDropDown1);
		// List<WebElement> valueOfDropDown = sel.getOptions();
		// String valueOfDropDown = relationProposal.getText();
		// elib.setExcelData3("Assure", 139, 2, valueOfDropDown);
		//// select[@class='styled']/option
		// List<WebElement> ele =
		// BaseClass.driver.findElements(By.xpath("//select[@class ='styled']/option"));
		// for (WebElement w : ele) {
		// // String str = w.getAttribute("value");
		// String str = w.getText();
		// elib.setExcelData3("Assure", 139, 3, str);
		// // System.out.println(str);
		// }
	}

	public void getSelfName() throws Throwable {
		// String valueFirstName = firstName1.getAttribute("value");
		// JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;
		// jse.executeScript("window.myvar =
		// document.getElementById('firstNamecd-1').value;");
		// Object var =
		// jse.executeScript("document.getElementById('firstNamecd-1').value;");
		// System.out.println(var);
		// String str = var.toString();
		// System.out.println(str);
		// String value = (String) jse.executeScript("return window.myvar;");
		// System.out.println(value);
		String firstName = firstName1.getAttribute("value");
		elib.setExcelData3("Assure", 145, 3, firstName);
		log.debug("In Insured detils Firstname is entered as " + firstName);
		String valueTitle = title1.getAttribute("value");
		// String va = title1.getText();
		// System.out.println(valueTitle);
		// System.out.println(va);
		elib.setExcelData3("Assure", 146, 3, valueTitle);
		String valueDOB = datePicker1.getAttribute("value");
		elib.setExcelData3("Assure", 147, 3, valueDOB);
		log.debug("In Insured detils DOB is entered as " + valueDOB);
		String lastName = lastName1.getAttribute("value");
		elib.setExcelData3("Assure", 148, 3, lastName);
		log.debug("In Insured detils Lastname is entered as " + lastName);
		// System.out.println(lastName);
	}

	public void selectRelationInsured() throws Throwable {

		// relationSelect.click();
		String relation = elib.getExcelData("InsuredDetails", 15, 1);
		clib.selectByValue(relationSelectError, relation);
		Select sel = new Select(relationSelect);
		WebElement getSelectecd = sel.getFirstSelectedOption();
		String getSelected = getSelectecd.getText();
		elib.setExcelData3("Assure", 150, 3, getSelected);
		log.debug("In Insureddetails Relation is selected as " + getSelected);
		// System.out.println(getSelected);
		String valueTitle = title1.getAttribute("value");
		elib.setExcelData3("Assure", 151, 3, valueTitle);
		log.debug("In Insured details Title is captured as " + valueTitle);
		String lastName = lastName1.getAttribute("value");
		elib.setExcelData3("Assure", 152, 3, lastName);
		log.debug("In Insured details Lastname is captured as " + lastName);
	}

	public void verifyFirstNameInsured() throws Throwable {

		String firstName = elib.getExcelData("InsuredDetails", 22, 1);
		firstName1.sendKeys(firstName);
		String firstNameProposal = firstName1.getAttribute("value");
		elib.setExcelData3("Assure", 154, 3, firstNameProposal);
		log.debug("In Insureddetails Firstname is entered as  " + firstNameProposal);

	}

	public void verifyDOBInsuredDetails() throws Throwable {
		String dob = elib.getExcelData("InsuredDetails", 38, 4);
		String relation = elib.getExcelData("InsuredDetails", 15, 1);
		if(relation.equalsIgnoreCase("SELF")) {
			System.out.println("donot click on DOB");
			}else {
		datePicker1.sendKeys(dob);
			}
		String valueDOB = datePicker1.getAttribute("value");
		elib.setExcelData3("Assure", 157, 3, valueDOB);
		log.debug("In Insureddetails selecting DOB as " + valueDOB);
		medHistProp.click();
	}

	public void verifyHeightAndFeetInsuredDetails() throws Throwable {

		String weight = elib.getExcelData("InsuredDetails", 42, 1);
		String height = elib.getExcelData("InsuredDetails", 49, 1);
		String inches = elib.getExcelData("InsuredDetails", 56, 1);

		clib.select(heightFeet1, height);
		Select sel = new Select(heightFeet1);
		WebElement heightSelected = sel.getFirstSelectedOption();
		String valueOfFeet = heightSelected.getText();
		elib.setExcelData3("Assure", 164, 3, valueOfFeet);
		log.debug("In Insureddetails selecting height as " + valueOfFeet);
		String valueDropFeet = heightFeet1.getText();
		Thread.sleep(2000);
		elib.setExcelData3("Assure", 160, 3, valueDropFeet);

		clib.select(heightInches1, inches);
		Select sel1 = new Select(heightInches1);
		WebElement heightInches1Selected = sel1.getFirstSelectedOption();
		String valueOfInches = heightInches1Selected.getText();
		elib.setExcelData3("Assure", 165, 3, valueOfInches);
		log.debug("In Insureddetails selecting inch as " + valueOfInches);
		String valueDropInches = heightInches1.getText();
		Thread.sleep(2000);
		elib.setExcelData3("Assure", 161, 3, valueDropInches);

		weight1.sendKeys(weight);
		String valueOfWeight = weight1.getAttribute("value");
		elib.setExcelData3("Assure", 166, 3, valueOfWeight);
		log.debug("In Insureddetails weight is entered as " + valueOfWeight);
	}

	public void verifyNegativErrorMessageInsured() throws Throwable {

		clib.select(relationSelect, "Relation");
		submitInsuredDetails.click();
		Thread.sleep(3000);
		clib.screenshotalert("AssureInsuredetailserrormsg");
		Thread.sleep(1100);
		String firstNameError = firstNameErrorInsured.getText();
		elib.setExcelData3("Assure", 170, 3, firstNameError);
		log.debug("In Insureddetails Capturing error message for Firstname" + firstNameError);
		Thread.sleep(1000);
		String lastNameError = lastNamecdErrorInsured.getText();
		elib.setExcelData3("Assure", 171, 3, lastNameError);
		log.debug("In Insureddetails Capturing error message for Lastname" + lastNameError);
		Thread.sleep(1100);
		String dobError = datePickerErrorInsured.getText();
		elib.setExcelData3("Assure", 172, 3, dobError);
		log.debug("In Insureddetails Capturing error message for DOB" + dobError);
		Thread.sleep(1100);
		String kindlyFillError = kindlyFillErrorInsured.getText();
		elib.setExcelData3("Assure", 173, 3, kindlyFillError);
		log.debug("In Insureddetails Capturing all error message for allerrormessage " + kindlyFillError);

	}

	public void getURLOfMedHistPageAssure() throws Throwable {

		submitInsuredDetails.click();
		log.debug("Selecting the insured next button");
		String medHistURL = BaseClass.driver.getCurrentUrl();
		elib.setExcelData3("Assure", 175, 3, medHistURL);
		log.debug("In Medicalhistory Capturing Title as " + medHistURL);
	}

	public void verifyDetailsFORInsuredDetails() throws Throwable {

		Boolean relation = relationSelect.isDisplayed();
		elib.setExcelDataBoolean("Assure", 135, 3, relation);
		log.debug("In Insureddetails Relation Dropdown is present " + relation);
		Boolean title = title1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 136, 3, title);
		log.debug("In Insureddetails Title is present " + title);
		Boolean firstName = firstName1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 137, 3, firstName);
		log.debug("In Insureddetails Firstname Textbox is present " + firstName);
		Boolean lastName = lastName1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 138, 3, lastName);
		log.debug("In Insureddetails Lastname Textbox is present " + lastName);
		Boolean datePicker = datePicker1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 139, 3, datePicker);
		log.debug("In Insureddetails Datapicker is present " + datePicker);
		Boolean height = heightFeet1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 140, 3, height);
		log.debug("In Insureddetails Height Dropdown is present " + height);
		Boolean feet = heightInches1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 141, 3, feet);
		log.debug("In Insureddetails Inch Dropdown is present " + feet);
		Boolean weight = weight1.isDisplayed();
		elib.setExcelDataBoolean("Assure", 142, 3, weight);
		log.debug("In Insureddetails Weight Textbox is present " + weight);
	}

	public void getProposalSummary() {

		List<WebElement> list = BaseClass.driver.findElements(By.xpath("//td[@valign = 'top']"));
		System.out.println(list.size());

		for (WebElement wb : list) {

			System.out.println(wb.getText());
		}
	}

	
	public void verifyinsuredmember1() {
		 Boolean relation = relation1.isDisplayed();
	        String relt =relation1.getText();
	        log.debug("Verifying the realtion 1 dropdown list is presentt " +relt);
	        log.debug("Verifying the realtion 1 dropdown list " +relation);
	        System.out.println("Verifying the realtion 1 is present " +relation);
	        Boolean title1 = tite1.isDisplayed();
	        String titt=tite1.getText();
	        log.debug("Verifying the title 1 dropdown list is " +titt);
	        log.debug("Verifying the title 1 dropdown is present " +title1);
	        System.out.println("Verifying the title 1 is present " +title1);
	        
	        Boolean fn1 =firstname1.isDisplayed();
	        log.debug("Verifying the firstname1 text field is present " +fn1);
	        System.out.println("Verifying the firstname1 field is present " +fn1);
	        
	        Boolean ln1 =lastname1.isDisplayed();
	        log.debug("Verifying the lastname1 field is present " +ln1);
	        System.out.println("Verifying the lastname1 field  is present " +ln1);
	        
	        Boolean pp1 =passport1.isDisplayed();
	        log.debug("Verifying the passport1 textfield is present " +pp1);
	        System.out.println("Verifying the passport1 textfield is present " +pp1);
	        
	        Boolean ct =citizenship1.isDisplayed();
	        String ctt =citizenship1.getText();
	        log.debug("Verifying the citizenship1 dropdown list is  " +ctt);
	        log.debug("Verifying the citizenship 1 dropdown is present " +ct);
	        System.out.println("Verifying the citizenship 1 is present " +ct);
	        
	        Boolean dob1 =datecal1.isDisplayed();
	        log.debug("Verifying the DOB1 is present " +dob1);
	        System.out.println("Verifying the DOB1 is present " +dob1);
	}
	public void verifyinsuredmember2() {
		 Boolean relation = relation2.isDisplayed();
	      //  String relt =relation2.getText();
	        //log.debug("Verifying the realtion 2 dropdown list is t " +relt);
	        log.debug("Verifying the realtion 2 dropdown is present " +relation);
	        System.out.println("Verifying the realtion 2 is present " +relation);
	        Boolean title1 = title2.isDisplayed();
	        //String titt=title2.getText();
	        //log.debug("Verifying the title 2 dropdown list is " +titt);
	        log.debug("Verifying the title 2 dropdown is present " +title1);
	        System.out.println("Verifying the title 2 is present " +title1);
	        
	        Boolean fn1 =firstname2.isDisplayed();
	        log.debug("Verifying the firstname2 text field is present " +fn1);
	        System.out.println("Verifying the firstname2 field is present " +fn1);
	        
	        Boolean ln1 =lastname2.isDisplayed();
	        log.debug("Verifying the lastname2 field is present " +ln1);
	        System.out.println("Verifying the lastname2 field  is present " +ln1);
	        
	        Boolean pp1 =passport2.isDisplayed();
	        log.debug("Verifying the passport2 textfield is present " +pp1);
	        System.out.println("Verifying the passport2 textfield is present " +pp1);
	        
	        Boolean ct =citizenship2.isDisplayed();
	        //String ctt =citizenship2.getText();
	        //log.debug("Verifying the citizenship2 dropdown list is  " +ctt);
	        log.debug("Verifying the citizenship 2 dropdown is present " +ct);
	        System.out.println("Verifying the citizenship 2 is present " +ct);
	        
	        Boolean dob1 =datecal2.isDisplayed();
	        log.debug("Verifying the DOB2 is present " +dob1);
	        System.out.println("Verifying the DOB2 is present " +dob1);
	}
	
	public void verifyinsuredmember3() {
		 Boolean relation = relation3.isDisplayed();
	       // String relt =relation3.getText();
	       // log.debug("Verifying the realtion 3 dropdown list is t " +relt);
	        log.debug("Verifying the realtion 3 dropdown is present " +relation);
	        System.out.println("Verifying the realtion 3 is present " +relation);
	        Boolean title1 = title3.isDisplayed();
	       // String titt=title3.getText();
	       // log.debug("Verifying the title 3 dropdown list is " +titt);
	        log.debug("Verifying the title 3 dropdown is present " +title1);
	        System.out.println("Verifying the title 3 is present " +title1);
	        
	        Boolean fn1 =firstname3.isDisplayed();
	        log.debug("Verifying the firstname3 text field is present " +fn1);
	        System.out.println("Verifying the firstname3 field is present " +fn1);
	        
	        Boolean ln1 =lastname3.isDisplayed();
	        log.debug("Verifying the lastname3 field is present " +ln1);
	        System.out.println("Verifying the lastname3 field  is present " +ln1);
	        
	        Boolean pp1 =passport3.isDisplayed();
	        log.debug("Verifying the passport3 textfield is present " +pp1);
	        System.out.println("Verifying the passport3 textfield is present " +pp1);
	        
	        Boolean ct =citizenship3.isDisplayed();
	        //String ctt =citizenship3.getText();
	        //log.debug("Verifying the citizenship3 dropdown list is  " +ctt);
	        log.debug("Verifying the citizenship 3 dropdown is present " +ct);
	        System.out.println("Verifying the citizenship 3 is present " +ct);
	        
	        Boolean dob1 =datecal3.isDisplayed();
	        log.debug("Verifying the DOB3 is present " +dob1);
	        System.out.println("Verifying the DOB3 is present " +dob1);
	}
	
	public void verifyinsuredmember4() {
		 Boolean relation = relation4.isDisplayed();
	        //String relt =relation4.getText();
	        //log.debug("Verifying the realtion 4 dropdown list is t " +relt);
	        log.debug("Verifying the realtion 4 dropdown is present " +relation);
	        System.out.println("Verifying the realtion 4 is present " +relation);
	        Boolean title1 = title4.isDisplayed();
	        //String titt=title2.getText();
	       // log.debug("Verifying the title 4 dropdown list is " +titt);
	        log.debug("Verifying the title 4 dropdown is present " +title1);
	        System.out.println("Verifying the title 4 is present " +title1);
	        
	        Boolean fn1 =firstname4.isDisplayed();
	        log.debug("Verifying the firstname4 text field is present " +fn1);
	        System.out.println("Verifying the firstname4 field is present " +fn1);
	        
	        Boolean ln1 =lastname4.isDisplayed();
	        log.debug("Verifying the lastname4 field is present " +ln1);
	        System.out.println("Verifying the lastname4 field  is present " +ln1);
	        
	        Boolean pp1 =passport4.isDisplayed();
	        log.debug("Verifying the passport4 textfield is present " +pp1);
	        System.out.println("Verifying the passport4 textfield is present " +pp1);
	        
	        Boolean ct =citizenship4.isDisplayed();
	        //String ctt =citizenship4.getText();
	        //log.debug("Verifying the citizenship4 dropdown list is  " +ctt);
	        log.debug("Verifying the citizenship 4 dropdown is present " +ct);
	        System.out.println("Verifying the citizenship 4 is present " +ct);
	        
	        Boolean dob1 =datecal4.isDisplayed();
	        log.debug("Verifying the DOB4 is present " +dob1);
	        System.out.println("Verifying the DOB4 is present " +dob1);
	}
	
	public void verifyinsuredmember5() {
		 Boolean relation = relation5.isDisplayed();
	        //String relt =relation5.getText();
	        //log.debug("Verifying the realtion 5 dropdown list is t " +relt);
	        log.debug("Verifying the realtion 5 dropdown is present " +relation);
	        System.out.println("Verifying the realtion 5 is present " +relation);
	        Boolean title1 = title5.isDisplayed();
	        //String titt=title5.getText();
	        //log.debug("Verifying the title 5 dropdown list is " +titt);
	        log.debug("Verifying the title 5 dropdown is present " +title1);
	        System.out.println("Verifying the title 5 is present " +title1);
	        
	        Boolean fn1 =firstname5.isDisplayed();
	        log.debug("Verifying the firstname5 text field is present " +fn1);
	        System.out.println("Verifying the firstname5 field is present " +fn1);
	        
	        Boolean ln1 =lastname5.isDisplayed();
	        log.debug("Verifying the lastname5 field is present " +ln1);
	        System.out.println("Verifying the lastname5 field  is present " +ln1);
	        
	        Boolean pp1 =passport5.isDisplayed();
	        log.debug("Verifying the passport5 textfield is present " +pp1);
	        System.out.println("Verifying the passport5 textfield is present " +pp1);
	        
	       Boolean ct =citizenship5.isDisplayed();
	        //String ctt =citizenship5.getText();
	       // log.debug("Verifying the citizenship5 dropdown list is  " +ctt);
	        log.debug("Verifying the citizenship 5 dropdown is present " +ct);
	        System.out.println("Verifying the citizenship 5 is present " +ct);
	        
	        Boolean dob1 =datecal5.isDisplayed();
	        log.debug("Verifying the DOB5 is present " +dob1);
	        System.out.println("Verifying the DOB5 is present " +dob1);
	}
	public void verifyinsuredmember6() {
		 Boolean relation = relation6.isDisplayed();
	        //String relt =relation6.getText();
	        //log.debug("Verifying the realtion 6 dropdown list is t " +relt);
	        log.debug("Verifying the realtion 6 dropdown is present " +relation);
	        System.out.println("Verifying the realtion 6 is present " +relation);
	        Boolean title1 = title6.isDisplayed();
	        //String titt=title6.getText();
	        //log.debug("Verifying the title 6 dropdown list is " +titt);
	        log.debug("Verifying the title 6 dropdown is present " +title1);
	        System.out.println("Verifying the title 6 is present " +title1);
	        
	        Boolean fn1 =firstname6.isDisplayed();
	        log.debug("Verifying the firstname6 text field is present " +fn1);
	        System.out.println("Verifying the firstname6 field is present " +fn1);
	        
	        Boolean ln1 =lastname6.isDisplayed();
	        log.debug("Verifying the lastname6 field is present " +ln1);
	        System.out.println("Verifying the lastname6 field  is present " +ln1);
	        
	        Boolean pp1 =passport6.isDisplayed();
	        log.debug("Verifying the passport6 textfield is present " +pp1);
	        System.out.println("Verifying the passport6 textfield is present " +pp1);
	        
	        Boolean ct =citizenship6.isDisplayed();
	        //String ctt =citizenship6.getText();
	        //log.debug("Verifying the citizenship6 dropdown list is  " +ctt);
	        log.debug("Verifying the citizenship 6 dropdown is present " +ct);
	        System.out.println("Verifying the citizenship 6 is present " +ct);
	        
	        Boolean dob1 =datecal6.isDisplayed();
	        log.debug("Verifying the DOB6 is present " +dob1);
	        System.out.println("Verifying the DOB6 is present " +dob1);
	}
	@FindBy(id ="firstNamecd-2Error")
	WebElement firstnamerror;
	@FindBy(id="lastNamecd-2Error")
	WebElement lastnamerror;
	@FindBy(id="datepickerCD-2Error")
	WebElement DOBerror;
	@FindBy(id="passport-2Error")
	WebElement passporterror;
	@FindBy(id="errordisplay_insured")
	WebElement errormsg;
	
	public void verifyerrormessageinsureddetailpage() throws InterruptedException {
		Thread.sleep(2000);
		String fstname  =firstnamerror.getText();
		log.debug("verifying the error message of empty first name text field is " +fstname);
		System.out.println("verifying the error message of empty first name text field is " +fstname);
		
		String lstname  =lastnamerror.getText();
		log.debug("verifying the error message of empty last name text field is " +lstname);
		System.out.println("verifying the error message of empty last name text field is " +lstname);
		

		String DOB  =DOBerror.getText();
		log.debug("verifying the error message of empty DOB calendar is " +DOB);
		System.out.println("verifying the error message of empty DOB calendar is " +DOB);
		
		String pss  =passporterror.getText();
		log.debug("verifying the error message of empty Passport number text field is " +pss);
		System.out.println("verifying the error message of empty Passport number text field is " +pss);
		
		String err  =errormsg.getText();
		log.debug("verifying the error message if any of the fields are empty as " +err);
		System.out.println("verifying the error message if any of the fields are empty as " +err);
		
	}
	
	
	public void verifyerrorforinvalidnamedobpassport() throws IOException, InterruptedException {
		Thread.sleep(2000);
		firstname2.sendKeys("238$$khalid");
		lastname2.sendKeys("447$#$kj");
		datecal2.sendKeys("6732282");
		passport2.sendKeys("4368328");
		
		Thread.sleep(3000);
		next();
	
		String fstname  =firstnamerror.getText();
		log.debug("verifying the error message if we enter invalid first name  " +fstname);
		System.out.println("verifying the error message if we enter invalid first name " +fstname);
		String lstname  =lastnamerror.getText();
		log.debug("verifying the error message if we enter invalid last name " +lstname);
		System.out.println("verifying the error message if we enter invalid last name " +lstname);
		String pss  =passporterror.getText();
		log.debug("verifying the error message if we enter invalid Passport number text field is " +pss);
		System.out.println("verifying the error message if we enter invalid Passport number text field is " +pss);
		String DOB  =DOBerror.getText();
		log.debug("verifying the error message if enter invlaid DOB  " +DOB);
		System.out.println("verifying the error message if enter invlaid DOB  " +DOB);
			
	}
	
	
	public void verifyredirectingtomedicalpage() {
		String currentURl = BaseClass.driver.getCurrentUrl();
		if (currentURl.equals("https://rhicluat.religarehealthinsurance.com/travel-insurance-explore/explore-filldetails"))
		{
			System.out.println("Redirected to the Medical History page of explore");
			log.debug("Redirected to the Medical History page of explore");
		}else {
			System.out.println("failed to redirect to the Medical History page of explore");
			log.debug("failed to redirect to the Medical History page of explore");
		}}
		

public void verifysavedata() throws Throwable {
	
	
	//String actualfirstName=fillFirstNameTextBox.getText();
	//if(firstName.equals(actualfirstName)){
	//System.out.println("hii");
		
		
	}
	
	


	
}


