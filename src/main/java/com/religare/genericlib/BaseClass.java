package com.religare.genericlib;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import io.appium.java_client.android.AndroidDriver;

//Logger log = Logger.getLogger("devpinoyLogger");
public class BaseClass {

	ExcelLib elib = new ExcelLib();

	public static RemoteWebDriver driver;
/*
	PrintStream orgStream = null;
	PrintStream fileStream = null;
*/
	@BeforeClass
	public void launchBrowser() throws Throwable {

		String url = elib.getExcelData("commonData", 0, 1);

		String browser = elib.getExcelData("commonData", 1, 1);

		PrintStream orgStream = System.out;
		orgStream = new PrintStream(new FileOutputStream("D:\\test_output\\console_logs\\console_logs.txt", true));

		// Redirecting console output to file

		System.setOut(orgStream);

		System.out.println("==launch browser=");

		if (browser.equalsIgnoreCase("Firefox")) {

			driver = new FirefoxDriver();
			driver.manage().window().maximize();

		}

		else if (browser.equalsIgnoreCase("Chrome")) {
			// C:\Users\meankur\eclipse-workspace\ReligareHealthInsuranceFrameWork\resouRces\chromedriver.exe
			System.setProperty("webdriver.chrome.driver", ".//resouRces//chromedriver.exe");
			// driver.manage().window().maximize();
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}

		else if (browser.equalsIgnoreCase("Opera")) {

			String operaChromiumDriver = ".//resouRces//operadriver.exe";
			String operaBrowserLocation = "C:\\Program Files\\Opera\\52.0.2871.40\\opera.exe";
			System.setProperty("webdriver.opera.driver", operaChromiumDriver);

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			options.setBinary(operaBrowserLocation);

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			OperaDriver browser1 = new OperaDriver(capabilities);

			driver = browser1;
			driver.manage().window().maximize();

		}

		else if (browser.equalsIgnoreCase("Safari")) {
			System.setProperty("webdriver.safari.driver", "C:\\Program Files (x86)\\Safari.exe");
			WebDriver driver = new SafariDriver();
			driver.manage().window().maximize();
		}

		else {

			System.setProperty("webdriver.ie.driver", "./resouRces/IEDriverServer.exe");

			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get(url);
		Thread.sleep(2000);

	}

	/*
	 * @AfterClass public void closeBrowser()
	 * 
	 * { driver.quit();
	 * 
	 * }
	 */

}
