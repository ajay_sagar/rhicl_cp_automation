
package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.FillDetailsExplore;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.JoyClick;
import com.religare.objectrepository.Joypolicyplan;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;

@Listeners(com.religare.genericlib.SampleListener.class)

public class JoyTest extends BaseClass {
	Logger log = Logger.getLogger("devpinoyLogger");
	WebDriverCommonLib clib = new WebDriverCommonLib();

	@Test(priority = 1)

	public void moveToJoy() throws Throwable {

		JoyClick click = PageFactory.initElements(BaseClass.driver, JoyClick.class);
		// clib.Passscreenshot("Religarehome", "ReligareHomePage");
		click.clickOnJoy();
		log.debug("***************Automating the Joy Module*****************");
		log.debug("Clicking on the Joy Module");
		Thread.sleep(3000);

		Joypolicyplan plan = PageFactory.initElements(BaseClass.driver, Joypolicyplan.class);

		clib.waitForPageToLoad();

		Thread.sleep(5000);

		plan.joytype();

		plan.totalmember();

		plan.age();

		Thread.sleep(3000);

		// try {
		plan.mobile();

		plan.slider();

		plan.tensure();

		// plan.totalpremium();

	}

	@Test(priority = 2)
	public void joyProposersdetails() throws Throwable {

		clib.waitForPageToLoad();

		// clib.Passscreenshot("JoyFirst", "JoyFirstPage");

		try {

			WebElement wb = BaseClass.driver.findElement(By.id("carebuynowimage"));
			Thread.sleep(2100);
			// clib.waitForElementPresent(wb);

			wb.click();
			log.debug("Clicking on the Submit button");
		}

		catch (Exception e) {
		} // plan.buy();

		// @Test
		//
		// public void fillProposalAndInsuredDetailsExploreTest() throws Throwable {

		clib.waitForPageToLoad();

		com.religare.genericlib.FillDetailsExplore fdx = PageFactory.initElements(BaseClass.driver,
				com.religare.genericlib.FillDetailsExplore.class);

		fdx.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		fdx.fillDetailsAddress();

		Thread.sleep(3500);

		try {

			fdx.panCard();

		} catch (NoSuchElementException e) {

			e.getMessage();

		}
	}

	// clib.Passscreenshot("JoyProposer's", "JoyProposer'sPage");

	@Test(priority = 3)
	public void joyInsureddetails() throws Throwable {

		BaseClass.driver.findElement(By.id("joytomorrowproposal")).click();
		log.debug("Clicking on the Submit button");
		Thread.sleep(7000);

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.insuredMember1();

		idp.insuredMember2();
		try {
			idp.insuredMember3();

			idp.insuredMember4();

			idp.insuredMember5();

			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

	}

	@Test(priority = 4)
	public void joyMedicalhistoryTest() throws Throwable {
		FillDetailsExplore fd = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		// clib.Passscreenshot("JoyInsured", "JoyInsuredPage");

		fd.Nextjoytomorrowinsuredscreen();

		clib.waitForPageToLoad();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		Thread.sleep(4000);

		mp.joyMedicalHistory();

	}

	@Test(priority = 5)
	public void joyProposalSummaryTest() throws Throwable {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);
		mp.joyProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));

	}

	@Test(priority = 6)
	public void joyPaymentTest() throws Throwable {

		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);

		// payment.Addonpremium();
		//
		// payment.Applicationno();

		payment.proceedTomorrow();
		//
		clib.waitForPageToLoad();

		Thread.sleep(5000);

		// }
		// // @Te}st
		// //
		// // public void paymentGateWay() {
		// // ]
		// // }
		//
		// @Test
		//
		// public void paymentPageCheckServiceResponseJoyFreedomTest() throws Throwable
		// {

		// WebElement wb = BaseClass.driver.findElement(By.xpath("//p[contains(text(),
		// 'Service response')]"));
		//
		// System.out.println(wb.getText());

		// Thread.sleep(4500);

		clib.waitForPageToLoad();

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);

		clib.waitForPageToLoad();

		// payu.gateway("JoyPayment", "JoyPaymentPage", "JoyThankyou",
		// "JoyThankyouPage");
		payu.gateway();

		Thread.sleep(1500);

		// button[@class='btn btn-success mb-10']

		// BaseClass.driver.findElement(By.xpath("//button[@class='btn btn-success
		// mb-10']")).click();

		// }
	}

	@Test(priority = 7)
	public void joyThankyouTest() throws Throwable {
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.thankyou();

	}
}
