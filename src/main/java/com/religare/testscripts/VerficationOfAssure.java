package com.religare.testscripts;
import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.ScreenshotPAGE;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.AssureClick;
import com.religare.objectrepository.AssureFirstPage;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;

@Listeners(com.religare.genericlib.SampleListener.class)

public class VerficationOfAssure extends BaseClass{
	
	WebDriverCommonLib clib = new WebDriverCommonLib();
	ScreenshotPAGE scr =new ScreenshotPAGE();
	
	@Test

	public void moveToassureTest() throws Throwable {
		AssureClick click = PageFactory.initElements(BaseClass.driver, AssureClick.class);
		scr.captureScreenshot(driver, "Religare Home Page");

		click.clickOnAssure();

		Thread.sleep(6000);
		
		clib.waitForPageToLoad();

		Thread.sleep(2500);	
		assertEquals("Your Page Title" , driver.getTitle());
		
		
		
}
	
	}
