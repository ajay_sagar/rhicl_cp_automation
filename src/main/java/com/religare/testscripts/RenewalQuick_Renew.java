package com.religare.testscripts;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.RenewalMethods;
@Listeners(com.religare.genericlib.SampleListener.class)

public class RenewalQuick_Renew extends BaseClass {
	WebDriverCommonLib clib = new WebDriverCommonLib();

	Logger log = Logger.getLogger("devpinoyLogger");
	@Test(priority=1)
	public void Quick_RenewRenewalFirstPage() throws Throwable {
		BaseClass b = PageFactory.initElements(BaseClass.driver, BaseClass.class);
		//b.launchBrowser();
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		log.debug("**************Automating the script for 'Quick Renew' Module*********************");
		re.clickRenewals();
		re.renewalFirstpage();
		
	}
	@Test(priority=2)
	public void Quick_RenewRenewaldashboard() throws Throwable {
		BaseClass b = PageFactory.initElements(BaseClass.driver, BaseClass.class);
		//b.launchBrowser();
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.renewalFirstpagenext();
	
}
	@Test(priority=3)
	public void Quick_RenewalPayment() throws Throwable {
		BaseClass b = PageFactory.initElements(BaseClass.driver, BaseClass.class);
		//b.launchBrowser();
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.Medicalhistoryanswer();
		re.quickrenew();
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.gateway();
	
}
	@Test(priority = 4)
	public void  Quick_RenewalThankyouTest() {
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();

	}
}
		
