package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.CareClick;
import com.religare.objectrepository.CareFirstPage;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;

@Listeners(com.religare.genericlib.SampleListener.class)

public class CareTest extends BaseClass

{

	JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

	// Logger log = Logger.getLogger(getClass());
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();

	WebDriverCommonLib clib = new WebDriverCommonLib();

	@Test(priority = 1)

	public void clickOnCareTest() throws Throwable {

		CareClick care = PageFactory.initElements(BaseClass.driver, CareClick.class);

		// clib.Passscreenshot("Religarehome", "ReligareHomePage");

		care.clickOnCare();
		log.debug("***************Automating the Care Module*****************");
		log.debug("Clicking on the Care Module");

		// }

		// @Test(priority = 2)

		// public void careFirstpageTest() throws Throwable {

		clib.waitForPageToLoad();

		CareFirstPage plan = PageFactory.initElements(BaseClass.driver, CareFirstPage.class);

		plan.totalMember();

		// String totalMember="one";

		String totalMember = elib.getExcelData("CareSpecificData", 2, 1);

		if (totalMember.equalsIgnoreCase("one")) {

			plan.ageOfEldestMember1();

		} else {

			plan.ageOfEldestMember();
		}

		try {

			plan.enterMobileNumber();

			Alert al = BaseClass.driver.switchTo().alert();

			WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

			wait.until(ExpectedConditions.alertIsPresent());

			al.accept();
		}

		catch (Exception e) {

			e.getMessage();
		}

		Thread.sleep(1500);

		plan.slider();

		String tenure = elib.getExcelData("CareSpecificData", 3, 1);
		//
		// if (tenure.equalsIgnoreCase("two")) {
		// plan.tenure2();
		// }
		//
		// else if (tenure.equalsIgnoreCase("three")) {
		//
		// plan.tenure3();
		//
		// } else {
		//
		// System.out.println("two tenure");
		// }
		//
		// // plan.tenure2();

		Thread.sleep(2000);

		// js.executeScript("window.scrollBy(0,-1000)");

		// clib.Passscreenshotone("CaretestFirst", "caretestFirstpage");
		/*
		 * Date d = new Date(); Timestamp t = new Timestamp(d.getTime()); String
		 * timeStamp = t.toString(); timeStamp = timeStamp.replace(' ', '_'); timeStamp
		 * = timeStamp.replace(':', '_');
		 * 
		 * File scrFile = ((TakesScreenshot)
		 * BaseClass.driver).getScreenshotAs(OutputType.FILE);
		 * 
		 * FileUtils.copyFile(scrFile, new
		 * File("D:\\" + "test_output\\" + "screenshot_pass\\" + "
		 * CareFirst" + timeStamp + ".png"));
		 * 
		 * String str = BaseClass.driver.getCurrentUrl();
		 * 
		 * BufferedImage image = ImageIO .read(new
		 * File("D:\\" + "test_output\\" + "screenshot_pass\\" + "
		 * CareFirst" + timeStamp + ".png"));
		 * 
		 * Graphics g = image.getGraphics(); g.setFont(g.getFont().deriveFont(20f));
		 * g.setColor(Color.red);
		 * 
		 * g.drawString(str, 120, 70); g.dispose();
		 * 
		 * ImageIO.write(image, "png", new
		 * File("D:\\" + "test_output\\" + "screenshot_pass\\" + "
		 * CareFirstPage" + timeStamp + ".png"));
		 */

		/*
		 * WebElement Buynow = driver.findElement(By.id("carebuynowimage"));
		 * 
		 * ((JavascriptExecutor)
		 * driver).executeScript("arguments[0].scrollIntoView(true);", Buynow);
		 * 
		 * plan.next();
		 * 
		 * try { // js.executeScript("window.scrollBy(0,-1000)"); plan.next();
		 * 
		 * Alert al = BaseClass.driver.switchTo().alert();
		 * 
		 * al.accept();
		 * 
		 * // js.executeScript("window.scrollBy(0,-1000)"); plan.next();
		 * 
		 * } catch (Exception e) {
		 * 
		 * e.getMessage(); }
		 */

	}

	@Test(priority = 2)

	public void fillProposersdetailsTest() throws Throwable {

		CareFirstPage plan = PageFactory.initElements(BaseClass.driver, CareFirstPage.class);
		WebElement Buynow = driver.findElement(By.id("carebuynowimage"));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", Buynow);

		plan.next();

		try {
			// js.executeScript("window.scrollBy(0,-1000)");
			plan.next();

			Alert al = BaseClass.driver.switchTo().alert();

			al.accept();

			// js.executeScript("window.scrollBy(0,-1000)");
			plan.next();

		} catch (Exception e) {

			e.getMessage();
		}

		clib.waitForPageToLoad();

		FillDetailsExplore fdx = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		fdx.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		fdx.fillDetailsAddress();

		Thread.sleep(3500);

		try {

			fdx.panCard();

		} catch (NoSuchElementException e) {

			e.getMessage();

		}

	}

	@Test(priority = 3)
	public void carefillInsureddetailsTest() throws Throwable {

		FillDetailsExplore fdx = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		// clib.Passscreenshot("CareProposer's", "CareProposer'sPage");

		fdx.nextClick();

		Thread.sleep(7000);

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.insuredMember1();
		Thread.sleep(2000);

		try {
			idp.insuredMember2();

			idp.insuredMember3();

			Thread.sleep(1500);

			idp.insuredMember4();

			Thread.sleep(1500);

			idp.insuredMember5();

			Thread.sleep(1500);

			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

		// clib.Passscreenshot("CareInsured", "CareInsuredPage");
		/*
		 * idp.next();
		 * 
		 * clib.waitForPageToLoad();
		 * 
		 * Thread.sleep(2000);
		 * 
		 * try {
		 * 
		 * BaseClass.driver.findElement(By.id("premiumPopupOk")).click(); } catch
		 * (Exception e) { e.getMessage(); }
		 * 
		 * clib.waitForPageToLoad();
		 */

	}

	@Test(priority = 4)

	public void careMedicalhistoryTest() throws Throwable {

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.next();

		clib.waitForPageToLoad();

		Thread.sleep(2000);

		try {

			BaseClass.driver.findElement(By.id("premiumPopupOk")).click();
		} catch (Exception e) {
			e.getMessage();
		}

		clib.waitForPageToLoad();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		mp.careMedicalhistory();
	}

	@Test(priority = 5)

	public void careProposalsummary1Test() throws InterruptedException {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		mp.careProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//form[@id='submitPMT'])[2]/div[1]/table/tbody/tr[1]/th[2]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Plan Type";
		assertTrue(str.equals(Expectedresult));


	}

	@Test(priority = 6)
	public void careProposalsummary2Test() throws InterruptedException {
		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);

		Thread.sleep(2000);

		// payment.ApplicationNocarecarefreedom();

		payment.Addonpremium();
		try {
			payment.caresbumit();
		} catch (Exception e) {
		}
		Thread.sleep(2000);
		
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[3]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));


		// payment.ApplicationNocarecarefreedom();
		//
		// payment.totalcarepremium();
	}

	@Test(priority = 7)

	public void carePaymentTest() throws Throwable {

		//
		// // payment.Totalpremiumcarefreedom();
		//
		// // payment.proceedcare();();
		//
		// clib.waitForPageToLoad();
		//
		// Thread.sleep(3000);
		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);
		try {
			payment.proceedcare();
		} catch (Exception e) {
		}
		//
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.gateway();
		//
		// payu.gateway("CarePayment", "CarePaymentPage", "CareThankyou",
		// "CareThankyouPage");

	}

	@Test(priority = 8)
	public void careThankyouTest() {
	PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
	payu.thankyou();
	
	}

}
