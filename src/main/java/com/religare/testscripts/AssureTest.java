package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.AssureClick;
import com.religare.objectrepository.AssureFirstPage;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;

@Listeners(com.religare.genericlib.SampleListener.class)

public class AssureTest extends BaseClass {

	WebDriverCommonLib clib = new WebDriverCommonLib();

	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)

	public void moveToassureTest() throws Throwable {

		// clib.Passscreenshot("Religarehome", "ReligareHomePage");
		AssureClick click = PageFactory.initElements(BaseClass.driver, AssureClick.class);

		click.clickOnAssure();
		log.debug("***************Automating the Assure Module*****************");
		log.debug("Clicking on the Assure Module");

		Thread.sleep(6000);
		// }
		//
		// @Test
		//
		// public void assureTest() throws Throwable {

		clib.waitForPageToLoad();

		Thread.sleep(2500);

	}

	@Test(priority = 2)

	public void assurefirstpage() throws Throwable {

		AssureFirstPage page = PageFactory.initElements(BaseClass.driver, AssureFirstPage.class);

		page.assurePolicyPlan();

		page.slider();

		page.tenure();

		// clib.Passscreenshot("AssureFirst", "AssureFirstPage");
		// page.next();

	}

	@Test(priority = 3)

	public void assureproposerDetail() throws Throwable {

		AssureFirstPage page = PageFactory.initElements(BaseClass.driver, AssureFirstPage.class);
		page.next();

		clib.waitForPageToLoad();

		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		pdp.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		pdp.fillDetailsAddress();

		Thread.sleep(3500);

		try {

			pdp.panCard();

		} catch (NoSuchElementException e) {

			e.getMessage();

		}

	}
	@Test(priority = 4)
	public void assureInsuredetails() throws Throwable {

		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		pdp.nextClick();

		Thread.sleep(7000);

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.insuredMember1();

		// clib.Passscreenshot("AssureInsured", "AssureInsuredPage");
		/*
		 * idp.next();
		 * 
		 * Thread.sleep(2500);
		 * 
		 * try {
		 * 
		 * BaseClass.driver.findElement(By.id("premiumPopupOk")).click(); } catch
		 * (Exception e) { e.getMessage(); }
		 * 
		 * clib.waitForPageToLoad();
		 */
	}

	@Test(priority = 5)

	public void assureMedicalHistory() throws InterruptedException, IOException {

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.next();

		Thread.sleep(2500);

		try {

			BaseClass.driver.findElement(By.id("premiumPopupOk")).click();
		} catch (Exception e) {
			e.getMessage();
		}

		clib.waitForPageToLoad();

		Thread.sleep(5000);

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);
		//
		// clib.waitForPageToLoad();

		mp.assureMedicalhistory();

		Thread.sleep(2000);

		clib.waitForPageToLoad();

	}

	@Test(priority = 6)

	public void assureroposalsummaryTest() throws InterruptedException {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);
		mp.assuremedicalhistorysubmit();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));


	}

	@Test(priority = 7)

	public void assurePaymentTest() throws Throwable {

		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);

		// payment.Applicationno();

		// payment.Totalpremiumassure();

		payment.proceedassure();

		clib.waitForPageToLoad();

		Thread.sleep(3000);

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);

		// payu.gateway("AssurePayment","AssurePaymentPage","AssureThankyou","AssureThankyouPage");
		payu.gateway();

	}
	@Test(priority = 8)
		public void assureThankyouTest() {
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();
		
	}
	

}