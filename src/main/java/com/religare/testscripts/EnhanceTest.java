package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.EnhanceClick;
import com.religare.objectrepository.EnhanceFirstPage;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;

@Listeners(com.religare.genericlib.SampleListener.class)

public class EnhanceTest extends BaseClass {
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();

	WebDriverCommonLib clib = new WebDriverCommonLib();

	@Test(priority = 1)

	public void clickOnEnhance() throws Throwable {

		// clib.Passscreenshot("Religarehome", "ReligareHomePage");

		EnhanceClick enhance = PageFactory.initElements(BaseClass.driver, EnhanceClick.class);

		enhance.clickOnEnhance();
		log.debug("***************Automating the Enhance Module*****************");
		log.debug("Clicking on the Enhance Module");

	}

	@Test(priority = 2)

	public void enhanceFirstpageTest() throws Throwable {

		EnhanceFirstPage efd = PageFactory.initElements(BaseClass.driver, EnhanceFirstPage.class);

		String totalMember = elib.getExcelData("EnhanceFillDetails", 0, 1);

		clib.waitForPageToLoad();

		if (totalMember.equalsIgnoreCase("one")) {

			efd.clickOnTotalMemberMinus();
		}

		else if (totalMember.equalsIgnoreCase("three")) {

			efd.clickOnTotalMemberPlus();
		}

		else if (totalMember.equalsIgnoreCase("four")) {

			efd.clickOnTotalMemberPlus();

			efd.clickOnTotalMemberPlus();
		}

		else if (totalMember.equalsIgnoreCase("five")) {
			efd.clickOnTotalMemberPlus();

			efd.clickOnTotalMemberPlus();

			efd.clickOnTotalMemberPlus();
		}

		else if (totalMember.equalsIgnoreCase("six")) {

			efd.clickOnTotalMemberPlus();

			efd.clickOnTotalMemberPlus();

			efd.clickOnTotalMemberPlus();

			efd.clickOnTotalMemberPlus();
		}

		clib.waitForPageToLoad();

		// efd.scroll();

		efd.clickOnTotalMemberMinus();

		// ((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
		if (totalMember.equalsIgnoreCase("one")) {

			efd.ageDropDown8();

		} else {

			efd.ageDropDown();
		}

		try {

			efd.enterMobileNumber();

			Alert al = BaseClass.driver.switchTo().alert();

			al.accept();
		}

		catch (Exception e) {

			e.getMessage();
		}

		efd.slider();

		Thread.sleep(3000);

		efd.slider1();

		// clib.Passscreenshot("Enhancefirst", "EnhanceFirstPage");

	}

	@Test(priority = 3)

	public void enhanceProposerdetailsTest() throws Throwable {

		EnhanceFirstPage efd = PageFactory.initElements(BaseClass.driver, EnhanceFirstPage.class);

		efd.next();

		Thread.sleep(5000);

		efd.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		efd.fillDetailsAddress();

		Thread.sleep(5000);

		// clib.Passscreenshot("EnhanceProposer's", "EnhanceProposer'sPage");

	}

	@Test(priority = 4)

	public void enhanceInsureddetailsTest() throws Throwable {
		

		EnhanceFirstPage efd = PageFactory.initElements(BaseClass.driver, EnhanceFirstPage.class);
		clib.waitForPageToLoad();
		
		efd.nextClick();

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.insuredMember1();

		try {

			idp.insuredMember2();

			idp.insuredMember3();

			idp.insuredMember4();

			idp.insuredMember5();

			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

		// clib.Passscreenshot("EnhanceInsured", "EnhanceInsuredPage");
		/*BaseClass.driver.findElement(By.id("enhanceinsured")).click();

		clib.waitForPageToLoad();

		Thread.sleep(2500);

		try {

			BaseClass.driver.findElement(By.id("premiumPopupOk")).click();
		} catch (Exception e) {
			e.getMessage();
		}
		clib.waitForPageToLoad();*/
	}

	@Test(priority = 5)

	public void enhanceMedicalhistoryTest() throws Throwable {
		
		BaseClass.driver.findElement(By.id("enhanceinsured")).click();

		clib.waitForPageToLoad();
		Thread.sleep(2500);

		try {

			BaseClass.driver.findElement(By.id("premiumPopupOk")).click();
		} catch (Exception e) {
			e.getMessage();
		}
		clib.waitForPageToLoad();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		mp.enchanceMedicalHistory();

		clib.waitForPageToLoad();

	}

	@Test(priority = 6)
	public void enhanceProposalsummaryTest() throws InterruptedException {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		mp.enhanceProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));

	}

	@Test(priority = 7)
	public void enhancePaymentTest() throws Throwable {

		BaseClass.driver.findElement(By.id("proceed_to_pay_enhance")).click();
		log.debug("Clicking on the proceed to pay button");
		clib.waitForPageToLoad();

		Thread.sleep(2300);

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);

		// payu.gateway("EnhancePayment", "EnhancePaymentPage", "EnhanceThankyou",
		// "EnhanceThankyouPage");
		payu.gateway();

	}

	@Test(priority = 8)
	public void enhanceThankyouTest() {

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();

		// payu.gateway("EnhancePayment", "EnhancePaymentPage", "EnhanceThankyou",
		// "EnhanceThankyouPage");

	}
}