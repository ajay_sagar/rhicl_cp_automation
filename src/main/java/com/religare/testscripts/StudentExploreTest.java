package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;
import com.religare.objectrepository.StudentExploreClick;
import com.religare.objectrepository.StudentExplorePolicyplan;

@Listeners(com.religare.genericlib.SampleListener.class)

public class StudentExploreTest extends BaseClass {

	WebDriverCommonLib clib = new WebDriverCommonLib();
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();

	@Test(priority = 1)

	public void clickOnStudentExploreTest() throws IOException {
		// clib.Passscreenshot("Religarehome", "ReligareHomePage");
		StudentExploreClick spc = PageFactory.initElements(BaseClass.driver, StudentExploreClick.class);

		spc.clickOnStudentExplore();
		log.debug("***************Automating the StudentExplore Module*****************");
		log.debug("Clicking on the StudentExplore Module");
	}

	@Test(priority = 2)

	public void studentexplorefirstpageTest() throws Throwable {

		StudentExplorePolicyplan plan = PageFactory.initElements(BaseClass.driver, StudentExplorePolicyplan.class);

		plan.travellingTo();

		plan.policyTensure();

		plan.mobile();

		plan.PED();

		plan.slider();

		plan.totalpremium();

		// clib.Passscreenshot("StudentExploreFirst", "StudentExploreFirstPage");

		plan.submit();

		plan.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		plan.fillDetailsAddress();

	}

	// clib.Passscreenshot("StudentExploreProposer's",
	// "StudentExploreProposer'sPage");

	@Test(priority = 3)

	public void studentexploreproposerTest() throws Throwable {
		StudentExplorePolicyplan plan = PageFactory.initElements(BaseClass.driver, StudentExplorePolicyplan.class);
		plan.nextClick();

		FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		idp.insuredMember1();

	}

	@Test(priority = 4)

	public void studentexploreInsuredTest() throws Throwable {

		FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		idp.studentInsuredNext();

		clib.waitForPageToLoad();

		com.religare.objectrepository.FillDetailsExplore explore = PageFactory.initElements(BaseClass.driver,
				com.religare.objectrepository.FillDetailsExplore.class);

		clib.waitForPageToLoad();

	}

	// clib.Passscreenshot("StudentExploreAdditional",
	// "StudentExploreAdditionalPage");

	@Test(priority = 5)

	public void studentexploreMedicalhistoryTest() throws Throwable {

		com.religare.objectrepository.FillDetailsExplore explore = PageFactory.initElements(BaseClass.driver,
				com.religare.objectrepository.FillDetailsExplore.class);

		explore.studentExploreInsuredpage();

		clib.waitForPageToLoad();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		clib.waitForPageToLoad();

		Thread.sleep(4000);

		mp.studenteEploreMedicalHistory();

	}

	@Test(priority = 6)

	public void studentexploreProposalSummaryTest() throws Throwable {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);
		mp.studentexploreProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));

	}

	@Test(priority = 7)

	public void studentexplorePaymentTest() throws Throwable {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		clib.waitForPageToLoad();

		Thread.sleep(4000);

		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);

		payment.studentExploreProceedToBuy();
		
	}
	

	@Test(priority = 8)
	public void joyThankyouTest() throws Throwable {
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.thankyou();

	}
	}

