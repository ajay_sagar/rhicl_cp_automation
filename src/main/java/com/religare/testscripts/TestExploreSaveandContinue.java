
package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;

import com.religare.objectrepository.ExploreClick;
import com.religare.objectrepository.ExploreFirstPage;
import com.religare.objectrepository.GmailConfig;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedicalHistoryPage;
import com.religare.objectrepository.PayU;

@Listeners(com.religare.genericlib.SampleListener.class)
public class TestExploreSaveandContinue extends BaseClass {
	WebDriverCommonLib clib = new WebDriverCommonLib();
	ExcelLib elib = new ExcelLib();
	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)
	public void ExploreFirstpage() throws Throwable {

		ExploreClick click = PageFactory.initElements(BaseClass.driver, ExploreClick.class);
		click.moveToExplore();
		log.debug("Clicking on the Explore Module");

		ExploreFirstPage afp = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		afp.getCurrentURLExplore();
		afp.VerifyTripType();
		afp.verifyStartdate();
		afp.verifyenddate();
		afp.verifyAgeoftravel();
		afp.verifyTotalmember();
		afp.verifymobile();
		afp.VerifyRegion();
		afp.verifyslider();
		// ((JavascriptExecutor) driver).executeScript("scroll(0,500);");
		Thread.sleep(3500);
		afp.verifyRadiobutton();
		afp.verifyTermandconditioncheckbox();
		afp.verifyGetquoteandbuy();
		afp.verifyingquotepagewithouttermsandcondition();
		afp.verifyingquotepagewithoutmobilenumber();
		afp.verifyingquotepagewithoutStrtdate();

		// String tripType = afp.getTripType();
		// String travelingTo = afp.getTravelingTo();

		/*
		 * if (tripType.equals("Single") && (travelingTo.equals("Asia") ||
		 * travelingTo.equals("Africa"))) { afp.verifyingquotepagewithoutenddate();
		 * }else {}
		 */

	}

	@Test(priority = 2)
	public void Exploreeditquotepage() throws Throwable {
		ExploreFirstPage afp = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		afp.VerifyingBuyNowbuttonisclickable();
		afp.verifyfilldetailpage();
		afp.verifyeditquotepage();

	}

	@Test(priority = 3)
	public void FillDetailspropserdetailsExplore() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		// pdp.verifyErrorMessage();
		pdp.isInputPresentInProposalDetailsPage();
		pdp.verifyRadioBtn();
		pdp.verifyFirstName();
		pdp.verifyLastName();
		pdp.verifyMobileNumber();
		pdp.clearMobileNumberDataAndReset();
		pdp.verifyDOB();
		pdp.verifyDOBSelect();
		pdp.verifyEmailAddress();
		pdp.verifyNomineeTextbox();
		pdp.valueOfDropDownAndIsClickable();
		pdp.verifyRelationDropDown();
		pdp.verifFillAddressOne();
		pdp.verifFillAddressTwo();
		pdp.verifyLandmark();
		pdp.verifyPinCode();
		clib.waitForPageToLoad();
		Thread.sleep(3000);
		pdp.verifyStateAutoPinCode();
		clib.waitForPageToLoad();
		Thread.sleep(5000);
		pdp.panCard();
		pdp.citySelect();
	}

	@Test(priority = 4)
	public void verifyingeachfieldininsureddetailpage() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		pdp.submitBTN();

		clib.waitForPageToLoad();
		// clib.waitForPageToLoad();
		pdp.verifyinsureddetailpage();
		ExploreFirstPage edit = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		// edit.verifyeditquotepage();

		FillDetailsExplore ins = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		ins.verifyinsuredmember1();
		try {
			ins.verifyinsuredmember2();
			ins.verifyinsuredmember3();
			ins.verifyinsuredmember4();
			ins.verifyinsuredmember5();
			ins.verifyinsuredmember6();
		} catch (Exception e) {
		}

	}

	@Test(priority = 5)
	public void errormessageInsureddetailpage() throws IOException, InterruptedException {
		FillDetailsExplore fill = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		fill.next();
		fill.verifyerrormessageinsureddetailpage();
		fill.verifyerrorforinvalidnamedobpassport();

	}

	@Test(priority = 6)
	public void fillinsuredetailspage() throws Throwable {
		FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		idp.insuredMember1();

		try {

			idp.insuredMember2();
			idp.insuredMember3();
			idp.insuredMember4();
			idp.insuredMember5();
			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

	}

	@Test(priority = 7)
	public void verifymedicalhistorypage() throws Throwable {
		FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		clib.waitForPageToLoad();

		idp.next();
		clib.waitForPageToLoad();
		// clib.waitForPageToLoad();
		idp.verifyredirectingtomedicalpage();
		ExploreFirstPage edit = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		edit.verifyeditquotepage();

		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		Thread.sleep(3000);
		emh.verifyHealthQuestionnaire();
		emh.verifydisclaimerpoints();
		emh.verifybuttonsinmedicalpage();
		emh.medHist();
		
	}
	@Test(priority = 8)
	public void verifymedicalhistoryemailpopup() throws Throwable {
		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		emh.savemedical();
		emh.verifyemailpopup();
		emh.verifyemailidtextfield();
	}
	
	@Test(priority = 9)
	public void gmaillogin() throws Throwable {
		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		emh.verifysndemail();
		emh.confirmmsg();
		GmailConfig gmail = PageFactory.initElements(BaseClass.driver, GmailConfig.class);
		gmail.gmaillogin();
		clib.waitForPageToLoad();

	}

	@Test(priority = 10)

	public void gmailconfig() throws InterruptedException {
		GmailConfig gmail = PageFactory.initElements(BaseClass.driver, GmailConfig.class);
		gmail.verifymail();
		Thread.sleep(4000);

	}

	@Test(priority = 11)
	public void verifyproposerdetailsdatapage() throws Throwable {

		FillDetailsExplore fill = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		// fill.verifysavedata();

		// if(fill.fillFirstNameTextBox.getText().equals(firstName) &&
		// fill.fillLastNameTextBox.getText().equals(lastName)
		// );

		String str1 = BaseClass.driver.getWindowHandle();
		Set<String> str = BaseClass.driver.getWindowHandles();
		System.out.println(str);
		// BaseClass.driver.switchTo().window(str);

		Thread.sleep(5000);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_TAB);
		for (String g : str) {

			driver.switchTo().window(g);
			if (!g.equals(str1)) {
				System.out.println(fill.fillFirstNameTextBox.getAttribute("value"));
				String firstname=(fill.fillFirstNameTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for firstname " + firstname);
				System.out.println(fill.fillLastNameTextBox.getAttribute("value"));
				String lastname=(fill.fillLastNameTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for lastname " + lastname);
				System.out.println(fill.validMobileNumber.getAttribute("value"));
				String validphonenumber=(fill.validMobileNumber.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for validmobilenumber " + validphonenumber);
				System.out.println(fill.emailTextBox.getAttribute("value"));
				String emailtextbox=(fill.emailTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for emailtextbox " + emailtextbox);
				System.out.println(fill.nomineeNameTextBox.getAttribute("value"));
				String nomineeNameTextBox=(fill.nomineeNameTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for nomineeNameTextBox " + nomineeNameTextBox );
				System.out.println(fill.nomineeRelationDropDown.getAttribute("value"));
				String nomineeRelationDropDown=(fill.nomineeRelationDropDown.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for nomineeRelationDropDown " + nomineeRelationDropDown);
				System.out.println(fill.validAddressOneTextBox.getAttribute("value"));
				String validAddress=(fill.validAddressOneTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for validAddressOneTextBox " + validAddress);
				System.out.println(fill.validAdressTwoTextBox.getAttribute("value"));
				String validAddresstwo=(fill.validAdressTwoTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for validAddressTwoTextBox " + validAddresstwo );
				System.out.println(fill.landmarkTextBox.getAttribute("value"));
				String landmark=(fill.landmarkTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for landmarkTextBox " + landmark);
				System.out.println(fill.validpinCodeTextBox.getAttribute("value"));
				String pincode=(fill.validpinCodeTextBox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for validpincodeTextbox " + pincode);
				System.out.println(fill.validCityNameDropDown.getAttribute("value"));
				String city=(fill.validCityNameDropDown.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for validCityNameDropDown " + city);
				System.out.println(fill.validStateNameTextbox.getAttribute("value"));
				String statename=(fill.validStateNameTextbox.getAttribute("value"));
				log.debug("verifying the data in proposerdetails for validStateNameTextbox " + statename);

			}

		}

	}

	@Test(priority = 12)
	public void insureddetails() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		pdp.nextClick();
		//FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		
		/*idp.insuredMember1();

		try {

			idp.insuredMember2();
			idp.insuredMember3();
			idp.insuredMember4();
			idp.insuredMember5();
			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

		clib.waitForPageToLoad();

		// clib.Passscreenshot("ExploreInsured", "ExploreInsuredPage");
		*/
		System.out.println("List started ");
		 List<WebElement> ele=BaseClass.driver.findElements(By.xpath("//div[@class='middleContainerIn']/div[starts-with(@id,'cont')]"));
		int count=ele.size();
		for(int i=1;i<=2;i++) {
			
			for(int j=1;j<8;j++) {
		try {		
String str =BaseClass.driver.findElement(By.xpath("(//div[@class='middleContainerIn']/div[starts-with(@id,'cont')]/div["+j+"])["+i+"]/div[1]/input")).getAttribute("value");
				
				System.out.println(str);
				log.debug("verify insured details data " + str);
		}
		catch(Exception e){
			
			
		}
				
			}
			
			
			
			
			
		}System.out.println("List ended ");

		
		
		
		
		
		
		
		

	}

	@Test(priority = 12)
	public void verifymedicalhistorydatapage() throws IOException, InterruptedException {
		FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		idp.next();

		Thread.sleep(2500);

		try {

			BaseClass.driver.findElement(By.id("premiumPopupOk")).click();
		}

		catch (Exception e) {

			e.getMessage();
		}

		clib.waitForPageToLoad();
		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		System.out.println(emh.noCheck1.isSelected());
		boolean answerno1=(emh.noCheck1.isSelected());
		log.debug("verifying the data in Medicalhistory for checkboxNo1 " + answerno1);
		System.out.println(emh.noCheck2.isSelected());
		boolean answerno2=(emh.noCheck2.isSelected());
		log.debug("verifying the data in Medicalhistory for checkboxNo2 " + answerno2);
		System.out.println(emh.noCheck3.isSelected());
		boolean answerno3=(emh.noCheck3.isSelected());
		log.debug("verifying the data in Medicalhistory for checkboxNo2 " + answerno3);

	}

	@Test(priority = 13)
	public void exploreProposalsummaryTest() throws Throwable {

		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		emh.exploreProposalsummary();
		WebElement ele = driver
				.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));
	}

	@Test(priority = 14)
	public void explorePaymentgatewayTest() throws Throwable {
		Thread.sleep(5000);

		clib.waitForPageToLoad();
		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);

		emh.exploreProceedtoPay();
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.gateway();

	}

	@Test(priority = 15)
	public void exploreThankyouTest() throws Throwable {
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.thankyou();

	}

}
