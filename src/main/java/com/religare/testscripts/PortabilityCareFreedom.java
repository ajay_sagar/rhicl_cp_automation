package com.religare.testscripts;

import static org.testng.Assert.assertTrue;
import java.awt.AWTException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.PortabilityMethod;
import com.religare.objectrepository.Portabilityform;

@Listeners(com.religare.genericlib.SampleListener.class)

public class PortabilityCareFreedom extends BaseClass {

	WebDriverCommonLib clib = new WebDriverCommonLib();
	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)

	public void verifyportabilityHomepage() throws Throwable {

		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
		log.debug("***************Automating the portability Carefreedom module*****************");

		portableobject.Portabilitypage();

		portableobject.verifyportableHome();

	}

	@Test(priority = 2)

	public void verifyFAQ() throws InterruptedException, IOException, AWTException {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.verifyFaQClickable();

		portableobject.verifyFAQslinkonfooterofPortability();

		portableobject.verifyquestionnairesonFAQs();

	}

	@Test(priority = 3)
	public void verifyheaderelement() throws AWTException, InterruptedException {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.verifyheaderelement1();
		
		
	}
	
	@Test(priority=4)
	public void Portabilitydashboard() throws InterruptedException {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

	}

	/*
	 * @Test(priority = 5 ,enabled=false) public void Portabilitydashboard() throws
	 * InterruptedException { PortabilityMethod portableobject =
	 * PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
	 * 
	 * 
	 * }
	 */

	@Test(priority = 4)
	public void carefreedomportabilityfillquotebox1() throws Throwable {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
		portableobject.carefreedompolicy();
		Thread.sleep(3000);

		portableobject.totalMember();
		// portableobject.verifyheaders();
		Thread.sleep(3000);

		portableobject.defaultfeatures();

		portableobject.verifyCovertypecarefreedom();
		
	}
  @Test(priority=5)
  public void Carefreedomportabilityfillquotebox() throws Throwable {
	  PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
		portableobject.verifyheaders();

		portableobject.defaultfeatures();

		portableobject.verifyCovertypecarefreedom();
		portableobject.totalMember();
		portableobject.verifyMemberdetails();
		portableobject.verifyMobilenumber();
		portableobject.verifyRelation();
		try {
			portableobject.Member1();
			Thread.sleep(2000);

			portableobject.Member2();
			Thread.sleep(1500);

			portableobject.Member3();

			Thread.sleep(1500);

			portableobject.Member4();

			Thread.sleep(1500);

			portableobject.Member5();

			Thread.sleep(1500);

			portableobject.Member6();

		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test(priority = 5)
	public void carefreedomportabilityfillquotebox2() throws Throwable {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.findreligarepremium();
		portableobject.verifySIforcarefreedom();

		portableobject.verifytenureaddonspremiumtotalpremiumforcarefreedom();

		portableobject.premium();

	}

	@Test(priority = 6)
	public void carefreedomfillproposerdetails() throws Throwable {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.portnow();

		Thread.sleep(5000);

		portableobject.verifyproceedtobuttonredirection();
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.fillproposersdetails();

	}

	@Test(priority = 7)
	public void carefreedomfillexistinginsurancepolicydetails() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.proposersdetailssave();
		Thread.sleep(2000);

		try {

			portableform.existinginsurancePolicyCarefreedomsinglemember();
			Thread.sleep(1000);
			portableform.existinginsurancePolicyCarefreedomtwomember();
			Thread.sleep(1000);
			// portableform.existinginsurancePolicySave();

		} catch (Exception e) {
			e.getMessage();

		}

	}


	@Test(priority = 8)
	public void carefreedompreviousinsurancepolicy() throws Throwable {

		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.existinginsurancePolicySave();

		portableform.previousinsurancepolicy();

	}

	@Test(priority = 9)
	public void carefreedompreviouspolicy() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.previousinsurancepolicysave();

		portableform.Previousyearpolicy();
	}

	@Test(priority = 10)
	public void carefreedomrenewalnotice() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.previouspolicysave();

		portableform.Renewalnotice();

	}

	@Test(priority = 11)
	public void carefreedomclaim() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.renewalnoticesave();

		portableform.Claims();

	}

	@Test(priority = 12)
	public void carefreedomMedicalhistory() throws InterruptedException, IOException, AWTException {

		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.claimsave();

		portableform.Next();

		clib.waitForPageToLoad();

		portableform.Healthquestions();

	}

	@Test(priority = 13)
	public void carefreedomProposalsummary1() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.SubmitHealthquestions();

		portableform.SubmitPolicy();
		WebElement ele = driver.findElement(By.xpath("//div[@class='col-md-3 proposer_fields'][1]/label"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Total Members";
		assertTrue(str.equals(Expectedresult));
		portableform.Proposalsummary();

	}

	@Test(priority = 14)
	public void carefreedomProposalsummary2() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.confirmPolicy();
		WebElement ele = driver.findElement(By.xpath("//div[@class='col-md-3 proposer_fields'][1]/label"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Total Members";
		assertTrue(str.equals(Expectedresult));

		clib.waitForPageToLoad();
		/*
		 * portableobject.findreligarepremium();
		 * portableobject.verifySIforcarefreedom();
		 * 
		 * portableobject.verifytenureaddonspremiumtotalpremiumforcarefreedom();
		 * portableobject.carefreedomportnow();
		 * 
		 * Thread.sleep(5000);
		 * 
		 * portableobject.verifyproceedtobuttonredirection();
		 */

		// }
		//
		// @Test
		// public void Portabilityform() throws Throwable {
		/*
		 * Portabilityform portableform = PageFactory.initElements(BaseClass.driver,
		 * Portabilityform.class);
		 */
		// portableform.verifyheaders();

		/*
		 * portableform.fillproposersdetails();
		 * 
		 * portableform.existinginsurancePolicyCarefreedomsinglemember();
		 * 
		 * portableform.previousinsurancepolicy();
		 * 
		 * portableform.Previousyearpolicy();
		 * 
		 * portableform.Renewalnotice();
		 * 
		 * portableform.Claims();
		 * 
		 * portableform.Next();
		 * 
		 * clib.waitForPageToLoad();
		 * 
		 * portableform.Healthquestions();
		 * 
		 * portableform.SubmitHealthquestions();
		 * 
		 * portableform.SubmitPolicy();
		 * 
		 * clib.waitForPageToLoad();
		 */

	}

	@Test(priority = 15)
	public void carefreedomPayment() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.Proceedtopay();
	}

}

// }
