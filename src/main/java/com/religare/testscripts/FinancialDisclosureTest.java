package com.religare.testscripts;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.religare.genericlib.ExcelLib;

public class FinancialDisclosureTest {

	static int index = 0;

	static ExcelLib elib = new ExcelLib();

	public static WebDriver driver;

	@BeforeMethod

	public void launchDisclosurePage() throws Throwable {

		// Launching Chrome Driver

		System.setProperty("webdriver.chrome.driver", ".//resouRces//chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Launching disclosure Page
		String url = elib.getExcelData("Disclosure", 0, 1);
		driver.get(url);

	}

	public static void disclosurePage(String xpathOfFinancialYear, String xpathOfQTR, String xpathOfPDF,
			String excelName, String pathOfExcel, String pathOfScreenshot) throws Throwable {

		driver.switchTo().frame(index);
		driver.findElement(By.xpath(xpathOfFinancialYear)).click();
		// Getting the size of frame

		Workbook workbook = new XSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();
		Sheet sheet = workbook.createSheet(excelName);
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// Create a Row
		Row headerRow = sheet.createRow(0);

		String[] columns = { "PDF Name", "URL of PDF", "Failed Title" };
		for (int j = 0; j < columns.length; j++) {
			Cell cell = headerRow.createCell(j);
			cell.setCellValue(columns[j]);
			cell.setCellStyle(headerCellStyle);
		}
		// Create Cell Style for formatting Date

		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

		// Create Other rows and cells with employees data

		int rowNum = 1;
		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println("Total Frames --" + size);

		//// div[@contentindex= '1c']//li[1]/a[1]/i
		//// a[contains(text(), 'Qtr 1 17-18')]

		// "//a[contains(text(), 'Qtr 4 18-19')]"

		WebElement container = driver.findElement(By.xpath(xpathOfQTR));
		// wait.until(ExpectedConditions.visibilityOf(container));\
		container.click();
		String containerName = container.getText();
		System.out.println(containerName);
		// Getting the size of PDF's
		// int size1 = driver.findElements(By.partialLinkText("NL")).size();
		// System.out.println(size1);
		int i;
		// "//a[contains(text(), 'Qtr 4 18-19')]/../ul/li/a"
		// Storing PDF's elements in list
		List<WebElement> web = driver.findElements(By.xpath(xpathOfPDF));
		// Iterating the elements in loop
		for (i = 0; i < web.size(); i++) {
			// rowNum++;
			web.get(i).click();
			String pdfName = web.get(i).getText();
			String urlOFPdfs = web.get(i).getAttribute("href");
			System.out.println(urlOFPdfs);
			// Getting PDF's name
			System.out.println(pdfName);
			// Using Set for Window Handling
			Set<String> set = driver.getWindowHandles();
			// Iterating the windows
			Iterator<String> it = set.iterator();
			// Getting parentWindow ID
			String parentWin = it.next();
			// Getting childWindowID
			String childWin = it.next();
			// Switching to childWindow
			driver.switchTo().window(childWin);

			String pngName = "";
			pngName = pdfName;
			TakesScreenshot t = (TakesScreenshot) driver;
			File srcFile = t.getScreenshotAs(OutputType.FILE);
			File destFile = new File(pathOfScreenshot + pngName + ".png");
			// File destFile = new File("D:\\Screenshots\\ financialD1 " + datef + ".png");
			org.apache.commons.io.FileUtils.copyFile(srcFile, destFile);
			// Getting the current URL

			System.out.println(driver.getCurrentUrl());
			// String getTitle = driver.getTitle();

			// Checking if file not found

			// WebElement noFile = driver.findElement(By.tagName("p"));
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(pdfName);
			row.createCell(1).setCellValue(urlOFPdfs);
			try {

				WebElement titleOfPage = driver.findElement(By.xpath("//title[contains(text(), 'Error Page')]"));
				String failedTitle = titleOfPage.getText();
				System.out.println(failedTitle);
				row.createCell(2).setCellValue(failedTitle);
				// row.createCell(2).setCellValue(failedTitle);

			} catch (NoSuchElementException e) {
				e.getMessage();
			}

			for (int j = 0; j < columns.length; j++) {
				sheet.autoSizeColumn(j);
			}

			// Closing the current browser
			driver.close();
			// Switching to parentWindow
			driver.switchTo().window(parentWin);
			// Switching to frame for Qtr1_17_18
			driver.switchTo().frame(index);

		}

		// LocalDateTime now = LocalDateTime.now();

		// ".\\Qtr1-7-18Screenshot\\"

		FileOutputStream fileOut = new FileOutputStream(pathOfExcel + System.currentTimeMillis() + ".xlsx");
		workbook.write(fileOut);
		workbook.close();
		// fileOut.close();
		driver.quit();
	}

	public static void disclosurePage(String xpathOfQTR, String xpathOfPDF, String excelName, String pathOfExcel,
			String pathOfScreenshot) throws Throwable {

		// driver.findElement(By.xpath(xpathOfFinancialYear)).click();
		// Getting the size of frame

		Workbook workbook = new XSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();
		Sheet sheet = workbook.createSheet(excelName);
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// Create a Row
		Row headerRow = sheet.createRow(0);

		String[] columns = { "PDF Name", "URL of PDF", "Failed Title" };
		for (int j = 0; j < columns.length; j++) {
			Cell cell = headerRow.createCell(j);
			cell.setCellValue(columns[j]);
			cell.setCellStyle(headerCellStyle);
		}
		// Create Cell Style for formatting Date

		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

		// Create Other rows and cells with employees data

		int rowNum = 1;
		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println("Total Frames --" + size);

		//// div[@contentindex= '1c']//li[1]/a[1]/i
		//// a[contains(text(), 'Qtr 1 17-18')]

		// "//a[contains(text(), 'Qtr 4 18-19')]"
		try {
			driver.switchTo().frame(index);
		} catch (Exception e) {
		}
		WebElement container = driver.findElement(By.xpath(xpathOfQTR));
		// wait.until(ExpectedConditions.visibilityOf(container));\
		container.click();
		String containerName = container.getText();
		System.out.println(containerName);
		// Getting the size of PDF's
		int size1 = driver.findElements(By.partialLinkText("NL")).size();
		System.out.println(size1);
		int i;
		// "//a[contains(text(), 'Qtr 4 18-19')]/../ul/li/a"
		// Storing PDF's elements in list
		List<WebElement> web = driver.findElements(By.xpath(xpathOfPDF));
		// Iterating the elements in loop
		for (i = 0; i < web.size(); i++) {
			// rowNum++;
			web.get(i).click();
			String pdfName = web.get(i).getText();
			String urlOFPdfs = web.get(i).getAttribute("href");
			System.out.println(urlOFPdfs);
			// Getting PDF's name
			System.out.println(pdfName);
			// Using Set for Window Handling
			Set<String> set = driver.getWindowHandles();
			// Iterating the windows
			Iterator<String> it = set.iterator();
			// Getting parentWindow ID
			String parentWin = it.next();
			// Getting childWindowID
			String childWin = it.next();
			// Switching to childWindow
			driver.switchTo().window(childWin);
			String pngName = "";
			pngName = pdfName;
			TakesScreenshot t = (TakesScreenshot) driver;
			File srcFile = t.getScreenshotAs(OutputType.FILE);
			File destFile = new File(pathOfScreenshot + pngName + ".png");
			// File destFile = new File("D:\\Screenshots\\ financialD1 " + datef + ".png");
			org.apache.commons.io.FileUtils.copyFile(srcFile, destFile);
			// Getting the current URL

			System.out.println(driver.getCurrentUrl());
			// String getTitle = driver.getTitle();

			// Checking if file not found

			// WebElement noFile = driver.findElement(By.tagName("p"));
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(pdfName);
			row.createCell(1).setCellValue(urlOFPdfs);

			try {
				WebElement titleOfPage = driver.findElement(By.xpath("//title[contains(text(), 'Error Page')]"));
				String failedTitle = titleOfPage.getText();
				System.out.println(failedTitle);
				row.createCell(2).setCellValue(failedTitle);

				// row.createCell(2).setCellValue(failedTitle);

			} catch (NoSuchElementException e) {
				e.getMessage();
			}

			for (int j = 0; j < columns.length; j++) {
				sheet.autoSizeColumn(j);
			}

			// Closing the current browser
			driver.close();
			// Switching to parentWindow
			driver.switchTo().window(parentWin);
			// Switching to frame for Qtr1_17_18
			driver.switchTo().frame(index);

		}

		// LocalDateTime now = LocalDateTime.now();

		// ".\\Qtr1-7-18Screenshot\\"

		FileOutputStream fileOut = new FileOutputStream(pathOfExcel + System.currentTimeMillis() + ".xlsx");
		workbook.write(fileOut);
		workbook.close();
		// fileOut.close();
		driver.quit();
	}

	// FOR 18 - 19

	@Test

	public void FY1819QTR1() throws Throwable {
		String checkForQtrPresent = elib.getExcelData("Disclosure", 5, 1);
		if (checkForQtrPresent.equalsIgnoreCase("yes")) {
			FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 1 18-19')]",
					"//a[contains(text(), 'Qtr 1 18-19')]/../ul/li/a", "FY1819QTR1",
					"D:\\test_output\\FYExcel\\FY1819QTR-1-", "D:\\test_output\\ScreenshotFY\\FY1819QTR-1-");
		} else {
			System.out.println("FY 18 - 19 QTR - 1 is not present in disclosure page");
			driver.close();
		}

	}

	@Test

	public void FY1819QTR2() throws Throwable {

		String checkForQtrPresent = elib.getExcelData("Disclosure", 6, 1);
		if (checkForQtrPresent.equalsIgnoreCase("yes")) {
			FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 2 18-19')]",
					"//a[contains(text(), 'Qtr 2 18-19')]/../ul/li/a", "FY1819QTR2",
					"D:\\test_output\\FYExcel\\FY1819QTR-2-", "D:\\test_output\\ScreenshotFY\\FY1819-QTR-2");
		} else {
			System.out.println("FY 18 - 19 QTR - 2 is not present in disclosure page");
			driver.close();
		}

	}
//
	@Test

	public void FY1819QTR3() throws Throwable {
		String checkForQtrPresent = elib.getExcelData("Disclosure", 7, 1);
		if (checkForQtrPresent.equalsIgnoreCase("yes")) {
			FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 3 18-19')]",
					"//a[contains(text(), 'Qtr 3 18-19')]/../ul/li/a", "FY1819QTR3",
					"D:\\test_output\\FYExcel\\FY1819QTR-3-", "D:\\test_output\\ScreenshotFY\\FY1819QTR-3-");
		} else {
			System.out.println("FY 18 - 19 QTR - 3 is not present in disclosure page");
			driver.close();
		}

	}

	@Test

	public void FY1819QTR4() throws Throwable {
		String checkForQtrPresent = elib.getExcelData("Disclosure", 8, 1);
		if (checkForQtrPresent.equalsIgnoreCase("yes")) {
			FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 4 18-19')]",
					"//a[contains(text(), 'Qtr 4 18-19')]/../ul/li/a", "FY1819QTR4",
					"D:\\test_output\\FYExcel\\FY1819QTR-4-", "D:\\test_output\\ScreenshotFY\\FY1819QTR-4-");
		} else {
			System.out.println("FY 18 - 19 QTR - 4 is not present in disclosure page");
			driver.close();
		}

	}

	// FOR FY 17 - 18

	@Test

	public void FY1718QTR1() throws Throwable {
		// code[contains(text(),'FY 17-18')]",
		FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 1 17-18')]",
				"//a[contains(text(), 'Qtr 1 17-18')]/../ul/li/a", "FY1718QTR1",
				"D:\\test_output\\FYExcel\\FY1718QTR-1-", "D:\\test_output\\ScreenshotFY\\FY1718QTR-1-");

	}

	@Test

	public void FY1718QTR2() throws Throwable {
		// "//code[contains(text(),'FY 17-18')]",
		FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 2 17-18')]",
				"//a[contains(text(), 'Qtr 2 17-18')]/../ul/li/a", "FY1718QTR2",
				"D:\\test_output\\FYExcel\\FY1718QTR-2-", "D:\\test_output\\ScreenshotFY\\FY1718QTR-2-");

	}

	@Test

	public void FY1718QTR3() throws Throwable {
		// "//code[contains(text(),'FY 17-18')]",
		FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 3 17-18')]",
				"//a[contains(text(), 'Qtr 3 17-18')]/../ul/li/a", "FY1718QTR3",
				"D:\\test_output\\FYExcel\\FY1718QTR-3-", "D:\\test_output\\ScreenshotFY\\FY1718QTR-3-");

	}

	@Test

	public void FY1718QTR4() throws Throwable {
		// "//code[contains(text(),'FY 17-18')]",
		String checkForQtrPresent = elib.getExcelData("Disclosure", 10, 1);
		if (checkForQtrPresent.equalsIgnoreCase("yes")) {
			FinancialDisclosureTest.disclosurePage("//a[contains(text(), 'Qtr 4 17-18')]",
					"//a[contains(text(), 'Qtr 4 17-18')]/../ul/li/a", "FY1718QTR4",
					"D:\\test_output\\FYExcel\\FY1718QTR-4-", "D:\\test_output\\ScreenshotFY\\FY1718QTR-4-");
		} else {
			System.out.println("FY 17 - 18 QTR - 4 is not present in disclosure page");
			driver.close();
		}
	}
//
//	// FOR FY 16 - 17
//
	@Test

	public void FY1617QTR1() throws Throwable {

		FinancialDisclosureTest.disclosurePage("//code[contains(text(),'FY 16-17')]",
				"//a[contains(text(), 'Qtr 1 16-17')]", "//a[contains(text(), 'Qtr 1 16-17')]/../ul/li/a",
				"FY1617QTR-1", "D:\\test_output\\FYExcel\\FY1617QTR-1-", "D:\\test_output\\ScreenshotFY\\FY1617QTR-1-");

	}

	@Test

	public void FY1617QTR2() throws Throwable {

		FinancialDisclosureTest.disclosurePage("//code[contains(text(),'FY 16-17')]",
				"//a[contains(text(), 'Qtr 2 16-17')]", "//a[contains(text(), 'Qtr 2 16-17')]/../ul/li/a", "FY1617QTR2",
				"D:\\test_output\\FYExcel\\FY1617QTR-2-", "D:\\test_output\\ScreenshotFY\\FY1617QTR-2-");

	}
//
	@Test

	public void FY1617QTR3() throws Throwable {

		FinancialDisclosureTest.disclosurePage("//code[contains(text(),'FY 16-17')]",
				"//a[contains(text(), 'Qtr 3 16-17')]", "//a[contains(text(), 'Qtr 3 16-17')]/../ul/li/a",
				"FY1617QTR-3", "D:\\test_output\\FYExcel\\FY1617QTR-3-", "D:\\test_output\\ScreenshotFY\\FY1617QTR-3-");

	}

	@Test

	public void FY1617QTR4() throws Throwable {

		FinancialDisclosureTest.disclosurePage("//code[contains(text(),'FY 16-17')]",
				"//a[contains(text(), 'Qtr 4 16-17')]", "//a[contains(text(), 'Qtr 4 16-17')]/../ul/li/a", "FY1617QTR4",
				"D:\\test_output\\FYExcel\\FY1617QTR-4-", "D:\\test_output\\ScreenshotFY\\FY1617QTR-4-");

	}

}
