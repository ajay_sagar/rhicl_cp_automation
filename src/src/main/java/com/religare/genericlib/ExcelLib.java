package com.religare.genericlib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelLib {

	String path = ".\\testData\\testData.xlsx";

	// String path ="C://Users//meankur//Downloads//_$testData.xlsx";
	// _$testData.xlsx
	public String getExcelData(String sheetName, int rowNum, int colNum) throws Throwable {

		FileInputStream file = new FileInputStream(path);
		Workbook wb = WorkbookFactory.create(file);
		Sheet sh = wb.getSheet(sheetName);
		// Row row = sh.getRow(rowNum);
		DataFormatter formatter = new DataFormatter();
		Cell cell = sh.getRow(rowNum).getCell(colNum);
		String data = formatter.formatCellValue(cell);
		// String data = row.getCell(colNum).getStringCellValue();

		wb.close();

		return data;
		// DataFormatter formatter = new DataFormatter(); //creating formatter using the
		// default locale
		// Cell cell = sheet.getRow(i).getCell(0);
		// String j_username = formatter.formatCellValue(cell);
		//
	}

	public void setExcelDataNewSheet(String Result, int RowNum, int ColNum, String excelSheetName) {
		// Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();
		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet(excelSheetName);
		XSSFSheet ExcelWSheet = sheet;
		XSSFCell Cell;
		XSSFRow Row;

		try {
			Row = ExcelWSheet.createRow(RowNum);
			Cell = Row.getCell(ColNum);
			if (Cell == null) {
				Cell = Row.createCell(ColNum);
				Cell.setCellValue(Result);
			} else {
				Cell.setCellValue(Result);
			}
			sheet.autoSizeColumn(ColNum);
			FileOutputStream out = new FileOutputStream(
					new File(".\\TestCasesSetExcelData\\" + System.currentTimeMillis() + ".xlsx"));
			workbook.write(out);
			out.close();
			System.out.println("Written successfully on disk.");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setExcelData3(String sName, int rowNum, int colNum, String data) throws Throwable {
		String path1 = ".\\TestCasesSetExcelData\\Book1.xlsx";
		File src = new File(path1);
		FileInputStream fis = new FileInputStream(src);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sName);
		// sheet.getRow(rowNum).createCell(colNum1).setCellValue(value);
		sheet.autoSizeColumn(colNum);
		sheet.getRow(rowNum).createCell(colNum).setCellValue(data);
		sheet.autoSizeColumn(colNum);
		FileOutputStream fos = new FileOutputStream(src);
		workbook.write(fos);
		workbook.close();
	}

	public void setExcelData3(int rowNum, String sName, int colNum) throws Throwable {
		String path1 = ".\\TestCasesSetExcelData\\Book1.xlsx";
		File src = new File(path1);
		FileInputStream fis = new FileInputStream(src);
		ZipSecureFile.setMinInflateRatio(-1.0d); 
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sName);
		// sheet.getRow(rowNum).createCell(colNum1).setCellValue(value);
		sheet.autoSizeColumn(colNum);
		sheet.getRow(rowNum).createCell(colNum).setCellValue("");
		sheet.autoSizeColumn(colNum);
		FileOutputStream fos = new FileOutputStream(src);
		workbook.write(fos);
		workbook.close();
	}

	public void setExcelDataBoolean(String sName, int rowNum, int colNum, Boolean value) throws Throwable {
		String path1 = ".\\TestCasesSetExcelData\\Book1.xlsx";
		File src = new File(path1);
		FileInputStream fis = new FileInputStream(src);
		ZipSecureFile.setMinInflateRatio(-1.0d); 
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sName);
		// sheet.getRow(rowNum).createCell(colNum1).setCellValue(value);
		sheet.autoSizeColumn(colNum);
		sheet.getRow(rowNum).createCell(colNum).setCellValue(value);
		sheet.autoSizeColumn(colNum);
		FileOutputStream fos = new FileOutputStream(src);
		workbook.write(fos);
		workbook.close();
	}

}
