package com.religare.genericlib;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class WebDriverCommonLib {

	ExcelLib elib = new ExcelLib();

	public void select(WebElement selWb, String text) {

		Select sel = new Select(selWb);

		sel.selectByVisibleText(text);
	}

	
	public void selectByIndex(WebElement selWb, int index) {

		Select sel = new Select(selWb);

		sel.selectByIndex(index);
	}
	
	
	
	
	public void selectByValue(WebElement selWb, String text) {

		Select sel = new Select(selWb);

		sel.selectByValue(text);
	}

	public void moveMouseToElement(WebElement element) {

		Actions act = new Actions(BaseClass.driver);

		act.moveToElement(element).perform();
	}

	public void dragAndDrop(WebElement source, WebElement dest) {

		Actions act = new Actions(BaseClass.driver);

		act.dragAndDrop(source, dest).perform();

	}

	public void waitForPageToLoad() {

		BaseClass.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void waitForElementPresent(WebElement element) {

		WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

		wait.until(ExpectedConditions.visibilityOf(element));

		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void javaScriptScroll() {

		JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

		js.executeScript("window.scrollBy(0,1000)");
	}

	public void Passscreenshot(String srcname, String destname) throws IOException {

		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');
		Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
				.takeScreenshot(BaseClass.driver);
		ImageIO.write(fpScreenshot.getImage(), "PNG",
				new File("D:\\" + "test_output\\" + "screenshot_pass\\" + srcname + timeStamp + ".png"));

		// File scrFile =
		// ((TakesScreenshot)BaseClass.driver).getScreenshotAs(OutputType.FILE);
		// FileUtils.copyFile(scrFile, new File( "E:\\" + "selenium\\" + srcname+".png"
		// ));

		String str = BaseClass.driver.getCurrentUrl();

		BufferedImage image = ImageIO
				.read(new File("D:\\" + "test_output\\" + "screenshot_pass\\" + srcname + timeStamp + ".png"));

		Graphics g = image.getGraphics();
		g.setFont(g.getFont().deriveFont(20f));
		g.setColor(Color.red);

		g.drawString(str, 120, 70);
		g.dispose();

		ImageIO.write(image, "png",
				new File("D:\\" + "test_output\\" + "screenshot_pass\\" + destname + timeStamp + ".png"));

	}

	public void Failscreenshot(String srcname, String destname) throws IOException {

		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');

		Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
				.takeScreenshot(BaseClass.driver);
		ImageIO.write(fpScreenshot.getImage(), "PNG",
				new File("D:\\" + "test_output\\" + "screenshot_fail\\" + srcname + timeStamp + ".png"));

		// File scrFile =
		// ((TakesScreenshot)BaseClass.driver).getScreenshotAs(OutputType.FILE);
		// FileUtils.copyFile(scrFile, new File( "E:\\" + "selenium\\" + srcname+".png"
		// ));

		String str = BaseClass.driver.getCurrentUrl();

		BufferedImage image = ImageIO
				.read(new File("D:\\" + "test_output\\" + "screenshot_fail\\" + srcname + timeStamp + ".png"));

		Graphics g = image.getGraphics();
		g.setFont(g.getFont().deriveFont(20f));
		g.setColor(Color.red);

		g.drawString(str, 120, 70);
		g.dispose();

		ImageIO.write(image, "png",
				new File("D:\\" + "test_output\\" + "screenshot_fail\\" + destname + timeStamp + ".png"));

	}

	public void Passscreenshotone(String srcname, String destname) throws IOException {

		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');

		// Screenshot fpScreenshot = new
		// AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(BaseClass.driver);
		// ImageIO.write(fpScreenshot.getImage(),"PNG",new File("D:\\" + "test_output\\"
		// + "screenshot_pass\\" + srcname+".png"));

		File scrFile = ((TakesScreenshot) BaseClass.driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("D:\\" + "test_output\\" + "screenshot_pass\\" + srcname + timeStamp +".png"));

		String str = BaseClass.driver.getCurrentUrl();

		BufferedImage image = ImageIO.read(new File("D:\\" + "test_output\\" + "screenshot_pass\\" + "xyz" + timeStamp + ".png"));

		Graphics g = image.getGraphics();
		g.setFont(g.getFont().deriveFont(20f));
		g.setColor(Color.red);

		g.drawString(str, 120, 70);
		g.dispose();

		ImageIO.write(image, "png", new File("D:\\" + "test_output\\" + "screenshot_pass\\" + destname + timeStamp + ".png"));

	}

	public void screenshotalert(String srcname) throws IOException, AWTException {

		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');

		BufferedImage image = new Robot()
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		ImageIO.write(image, "png",
				new File("D:\\" + "test_output\\" + "screenshot_pass\\" + srcname + timeStamp + ".png"));

	}

	public void getCurrentURL(int row, int col) throws Throwable {
		// submitInsuredDetails.click();
		String medHistURL = BaseClass.driver.getCurrentUrl();
		elib.setExcelData3("Assure", row, col, medHistURL);
	}
	
	public void errormessagescreenshot(String srcname) throws AWTException, IOException {
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');
		Robot rb = new Robot();

		BufferedImage image = ImageIO
				.read(new File("D:\\" + "test_output\\" + "screenshot_pass\\" + srcname + timeStamp + ".png"));
		rb.keyPress(KeyEvent.VK_WINDOWS);
		rb.keyPress(KeyEvent.VK_PRINTSCREEN);
		rb.keyRelease(KeyEvent.VK_PRINTSCREEN);
		rb.keyRelease(KeyEvent.VK_WINDOWS);
		ImageIO.write(image, "png",
				new File("D:\\" + "test_output\\" + "screenshot_pass\\" + srcname + timeStamp + ".png"));
		
	}
	
	

}
