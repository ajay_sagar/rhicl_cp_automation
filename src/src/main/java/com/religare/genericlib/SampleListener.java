package com.religare.genericlib;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class SampleListener implements ITestListener {

	WebDriverCommonLib clib = new WebDriverCommonLib();

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult result) {

			
			String className = this.getClass().getSimpleName(); 
			String PassedTestName = result.getMethod().getMethodName();
	if( PassedTestName.equals("clickOnCareTest")||PassedTestName.equals("carefreedomportabilityfillquotebox1")||PassedTestName.equals("carefreedomportabilityfillquotebox2")||PassedTestName.equals(" careportabilityfillquotebox1")||PassedTestName.equals("careportabilityfillquotebox2") ){
			try {
				clib.Passscreenshotone(PassedTestName ,  PassedTestName+"withURL");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}

	else{


	try {
				clib.Passscreenshot(PassedTestName ,  PassedTestName+"withURL");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	}

	public void onTestFailure(ITestResult result) {

		// TODO Auto-generated method stub

		 String failedTestName = result.getMethod().getMethodName();
		/*EventFiringWebDriver ed = new EventFiringWebDriver(BaseClass.driver);
		try {
			clib.Failscreenshot(failedTestName, failedTestName + "urlrewrite");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		try {
			clib.Failscreenshot(failedTestName, failedTestName+"url");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

}