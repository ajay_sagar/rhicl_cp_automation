package com.religare.objectrepository;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class MedicalHistoryPage {

	ExcelLib elib = new ExcelLib();
    Logger log = Logger.getLogger("devpinoyLogger");
	WebDriverCommonLib clib = new WebDriverCommonLib();

	// Does any person(s) to be insured has any Pre-existing diseases if user tap
	// yes

	@FindBy(id = "clck1")

	WebElement clickHealthQuestionnaire1;
	
	@FindBy(id = "clck2")

	WebElement clickHealthQuestionnair12;
	@FindBy(id = "clck1")

	WebElement clickHealthQuestionnair3;
	// Liver Disease? * checkbox 1

	@FindBy(xpath = "//label[@class='label_checkbox' and @for='insuredCdQuestionOne-128-1']")

	WebElement liverDiseaseCheckBox1;

	// Liver Disease? * checkbox 2

	@FindBy(xpath = "//label[@class='label_checkbox' and @for='insuredCdQuestionOne-128-2']")

	WebElement liveDiseaseCheckBox2;

	@FindBy(xpath = "//label[@class='label_checkbox' and @for='insuredCdQuestionOne-128-3']")

	WebElement liveDiseaseCheckBox3;

	@FindBy(xpath = "//label[@class='label_checkbox' and @for='insuredCdQuestionOne-128-4']")

	WebElement liveDiseaseCheckBox4;

	@FindBy(xpath = "//label[@class='label_checkbox' and @for='insuredCdQuestionOne-128-5']")

	WebElement liveDiseaseCheckBox5;

	@FindBy(xpath = "//label[@class='label_checkbox' and @for='insuredCdQuestionOne-128-6']")

	WebElement liveDiseaseCheckBox6;

	// for cancer tumor

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-114-1']")

	WebElement cancerOrTumorCheckBox1;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-114-2']")

	WebElement cancerOrTumorCheckBox2;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-114-3']")

	WebElement cancerOrTumorCheckBox3;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-114-4']")

	WebElement cancerOrTumorCheckBox4;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-114-5']")

	WebElement cancerOrTumorCheckBox5;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-114-6']")

	WebElement cancerOrTumorCheckBox6;

	// for heart disease

	@FindBy(xpath = "//label[@for ='insuredCdQuestionOne-143-1']")

	WebElement heartDiseaseCheckBox1;

	@FindBy(xpath = "//label[@for ='insuredCdQuestionOne-143-2']")

	WebElement heartDiseaseCheckBox2;

	@FindBy(xpath = "//label[@for ='insuredCdQuestionOne-143-3']")

	WebElement heartDiseaseCheckBox3;

	@FindBy(xpath = "//label[@for ='insuredCdQuestionOne-143-4']")

	WebElement heartDiseaseCheckBox4;

	@FindBy(xpath = "//label[@for ='insuredCdQuestionOne-143-5']")

	WebElement heartDiseaseCheckBox5;

	@FindBy(xpath = "//label[@for ='insuredCdQuestionOne-143-6']")

	WebElement heartDiseaseCheckBox6;

	// kidney disease

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-129-1']")

	WebElement kidneyDiseaseCheckobx1;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-129-2']")

	WebElement kidneyDiseaseCheckobx2;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-129-3']")

	WebElement kidneyDiseaseCheckobx3;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-129-4']")

	WebElement kidneyDiseaseCheckobx4;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-129-5']")

	WebElement kidneyDiseaseCheckobx5;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-129-6']")

	WebElement kidneyDiseaseCheckobx6;

	// for paralysis or stroke

	@FindBy(xpath = "//label[@for='insuredCdQuestionOne-164-1']")

	WebElement paralysisOrStrokeCheckbox1;

	@FindBy(xpath = "//label[@for='insuredCdQuestionOne-164-2']")

	WebElement paralysisOrStrokeCheckbox2;

	@FindBy(xpath = "//label[@for='insuredCdQuestionOne-164-3']")

	WebElement paralysisOrStrokeCheckbox3;

	@FindBy(xpath = "//label[@for='insuredCdQuestionOne-164-4']")

	WebElement paralysisOrStrokeCheckbox4;

	@FindBy(xpath = "//label[@for='insuredCdQuestionOne-164-5']")

	WebElement paralysisOrStrokeCheckbox5;

	@FindBy(xpath = "//label[@for='insuredCdQuestionOne-164-6']")

	WebElement paralysisOrStrokeCheckbox6;

	// other disease checkboxs

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-210-1']")

	WebElement otherDiseaseCheckBox1;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-210-2']")

	WebElement otherDiseaseCheckBox2;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-210-3']")

	WebElement otherDiseaseCheckBox3;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-210-4']")

	WebElement otherDiseaseCheckBox4;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-210-5']")

	WebElement otherDiseaseCheckBox5;

	@FindBy(xpath = "//label[@for = 'insuredCdQuestionOne-210-6']")

	WebElement otherDiseaseCheckBox6;

	// other disease descriptionTextbox

	@FindBy(id = "otherdisease-210-1")

	WebElement otherDiseaseTextBox1;

	@FindBy(id = "otherdisease-210-2")

	WebElement otherDiseaseTextBox2;

	@FindBy(id = "otherdisease-210-3")

	WebElement otherDiseaseTextBox3;

	@FindBy(id = "otherdisease-210-4")

	WebElement otherDiseaseTextBox4;

	@FindBy(id = "otherdisease-210-5")

	WebElement otherDiseaseTextBox5;

	@FindBy(id = "otherdisease-210-6")

	WebElement otherDiseaseTextBox6;

	// Has anyone been diagnosed / hospitalized / or under any treatment for any
	// illness / injury during the last 48 months?

	@FindBy(id = "clck2")

	WebElement clickHealthQuestionnaire2;

	@FindBy(id = "clck3")

	// Have you ever claimed under any travel policy?

	WebElement clickHealthQuestionnaire3;

	@FindBy(id = "label-id1-0")

	WebElement noCheck1;

	@FindBy(id = "label-id2-0")

	WebElement noCheck2;

	@FindBy(id = "label-id3-0")

	WebElement noCheck3;

	@FindBy(id = "validTermCondition-1")

	WebElement agreeChekc;

	@FindBy(id = "TripStartIndia")

	WebElement tripChkBox;

	@FindBy(id = "submit_medical_history")

	WebElement nextBtn;

	@FindBy(id = "proceed_to_pay_explore")

	WebElement proceedToPayBtn;
@FindBy (id="recivedSms")
WebElement smsemail;

@FindBy (xpath = "//td[text()='Your Proposal will be saved and sent as it is to your id']")

WebElement verifypopup;

public void verifydisclaimerpoints() {
	boolean a = agreeChekc.isDisplayed();
	 log.debug("Checking the I hereby agree to the Terms & Conditions of the purchase of this policy option is present " +a);
	    System.out.println("Checking the I hereby agree to the Terms & Conditions of the purchase of this policy option is present " +a);
Boolean t =	tripChkBox.isDisplayed();
	 log.debug("Checking the Trip start from India only option is present " +t );
	    System.out.println("Checking the Trip start from India only option is present " +t );
	    Boolean sms =smsemail.isDisplayed();
	    log.debug("Checking the Receive Service SMS and E-mail alerts option is present " +sms );
	    System.out.println("Checking the Receive Service SMS and E-mail alerts option is present " +sms );
	
}

@FindBy (id="medicalBack")
WebElement backbutton;
@FindBy (id="save_for_later")
WebElement savebutton;


public void verifybuttonsinmedicalpage() {
	Boolean nxt = nextBtn.isDisplayed();
	log.debug("Checking the Next button is present "+nxt);
    System.out.println("Checking the Next button is present "+nxt);
    Boolean bck = backbutton.isDisplayed();
	log.debug("Checking the Back button is present "+bck);
    System.out.println("Checking the Back button is present "+bck);
    
    Boolean sve = savebutton.isDisplayed();
   	log.debug("Checking the save button is present "+sve);
       System.out.println("Checking the save button is present "+sve);
}
	public void medHist() throws Throwable {
		
		noCheck1.click();
		noCheck2.click();
		noCheck3.click();
    log.debug("Choosing 'No' for all the questions available on medical history page");
    System.out.println("Choosing 'No' for all the questions available on medical history page");
		agreeChekc.click();
		 log.debug("Enabling the I hereby agree to the Terms & Conditions of the purchase of this policy option");
		    System.out.println("Enabling the I hereby agree to the Terms & Conditions of the purchase of this policy option");
		tripChkBox.click();
		 log.debug("Enabling the Trip start from India only option");
		    System.out.println("Enabling the Trip start from India only option");
	}
	
	public void savemedical() {
		savebutton.click();
		log.debug("Clicking on the Save button");
	    System.out.println("Clicking on the Save button");
	}
	
	
	
	
	public void verifyemailpopup() {
		Boolean a = verifypopup.isDisplayed();
		if(a==true) {
			System.out.println("User is redirected to a pop-up for entering email id(on which proposal has to be sent)");
			log.debug("User is redirected to a pop-up for entering email id(on which proposal has to be sent)");
		}else {
			System.out.println("User failed to  redirect to a pop-up for entering email id(on which proposal has to be sent)");
			log.debug("User failed to  redirect to a pop-up for entering email id(on which proposal has to be sent)");
		}
	}
	@FindBy(id="confirmMail")
	WebElement cnfrmmail;
	@FindBy(xpath="//a[text()='Send Email']")
	WebElement sndeml;
	@FindBy(xpath="//p[text()='The proposal has been sent. We thank you for showing interest in Religare Health Insurance. ']")
	WebElement cnfmmsg;
	
	public void confirmmsg()
	{
		Boolean a = cnfmmsg.isDisplayed();
		if (a==true) {
			System.out.println("User is Redirected to a pop-up containg message 'The proposal has been sent. We thank you for showing your interest in Religare Health Insurance'");
			log.debug("User is Redirected to a pop-up containg message 'The proposal has been sent. We thank you for showing your interest in Religare Health Insurance'");
		}else {
			System.out.println("User Failed to  Redirected to a pop-up containg message 'The proposal has been sent. We thank you for showing your interest in Religare Health Insurance'");
			log.debug("User  Failed to Redirected to a pop-up containg message 'The proposal has been sent. We thank you for showing your interest in Religare Health Insurance'");	
			
		}
	}
	public void verifyemailidtextfield() throws Throwable {
		
		String cfml = elib.getExcelData("Medicalhsty", 0, 1);
		cnfrmmail.clear();
		cnfrmmail.sendKeys(cfml);
		log.debug("Verify the Email Id text field is editable and entering the new email id as " +cfml);
		System.out.println("Verify the Email Id text field is editable and entering the new email id as " +cfml);
	}
	public void verifysndemail() throws InterruptedException {
		Thread.sleep(3000);
		Boolean a = sndeml.isDisplayed();
		log.debug("Verifying that Send Email button is present "+a);
		System.out.println("Verifying that Send Email button is present "+a);
		sndeml.click();
		log.debug("Clicking on the Send Email button");
		System.out.println("Clicking on the Send Email button");
	}
	
	
	public void nextmedical() throws InterruptedException {
		nextBtn.click();
		 log.debug("Clicking on the Submit button");
		    System.out.println("Clicking on the Submit button");
		Thread.sleep(5000);

	}
	public void proceedToPayBtn() throws InterruptedException {
		proceedToPayBtn.click();

		Thread.sleep(5000);
	}

	public void preExistingDiseaseYesMember() throws Throwable {

		// String liverDisease = elib.getExcelData("MedicalHistory", 1, 1);
		// String cancerOrTumor = elib.getExcelData("MedicalHistory", 2, 1);
		// String coronaryArteryHeartDisease = elib.getExcelData("MedicalHistory", 3,
		// 1);
		// String kidneyDisease = elib.getExcelData("MedicalHistory", 4, 1);
		// String paralysisOrStroke = elib.getExcelData("MedicalHistory", 5, 1);
		// String anyOtherDiseasesOrailmentsnotmentionedabove =
		// elib.getExcelData("MedicalHistory", 6, 1);
		//
		String otherDiseaseDescription = elib.getExcelData("MedicalHistory", 7, 1);
		//
		// BaseClass.driver.findElement(By.id("clck1")).click();

		clib.waitForPageToLoad();

		clickHealthQuestionnaire1.click();

		// if(liverDisease.equalsIgnoreCase("yes")) {
		liverDiseaseCheckBox1.click();
		// }
		// else if(cancerOrTumor.equalsIgnoreCase("yes")) {
		cancerOrTumorCheckBox1.click();
		// }
		// else if(kidneyDisease.equalsIgnoreCase("yes")) {
		kidneyDiseaseCheckobx1.click();

		heartDiseaseCheckBox1.click();
		// }
		// else if(coronaryArteryHeartDisease.equalsIgnoreCase("yes")) {
		kidneyDiseaseCheckobx1.click();
		// }
		// else if(paralysisOrStroke.equalsIgnoreCase("yes")) {
		paralysisOrStrokeCheckbox1.click();
		// }
		// else if(anyOtherDiseasesOrailmentsnotmentionedabove.equalsIgnoreCase("yes"))
		// {
		otherDiseaseCheckBox1.click();
		otherDiseaseTextBox1.sendKeys(otherDiseaseDescription);
		// }

		// clickHealthQuestionnaire1.click();

		// if(liverDisease.equalsIgnoreCase("yes")) {
		liveDiseaseCheckBox2.click();
		// }
		// else if(cancerOrTumor.equalsIgnoreCase("yes")) {
		cancerOrTumorCheckBox2.click();

		heartDiseaseCheckBox2.click();
		// }
		// else if(kidneyDisease.equalsIgnoreCase("yes")) {
		kidneyDiseaseCheckobx2.click();
		// }
		// else if(coronaryArteryHeartDisease.equalsIgnoreCase("yes")) {
		kidneyDiseaseCheckobx1.click();
		// }
		// else if(paralysisOrStroke.equalsIgnoreCase("yes")) {
		paralysisOrStrokeCheckbox2.click();
		// }
		// else if(anyOtherDiseasesOrailmentsnotmentionedabove.equalsIgnoreCase("yes"))
		// {
		otherDiseaseCheckBox2.click();
		otherDiseaseTextBox2.sendKeys(otherDiseaseDescription);

		// }

	}

	@FindBy(id = "selectbox_HEDTravelHospitalized_1")

	WebElement reasonForTreatmentdropDown1;

	@FindBy(id = "selectbox_HEDTravelHospitalized_2")

	WebElement reasonForTreatmentdropDown2;

	@FindBy(id = "desc-HEDTravelHospitalized-1")

	WebElement reasonTextBox1;

	@FindBy(id = "desc-HEDTravelHospitalized-2")

	WebElement reasonTextBox2;

	public void diagnosedAndhospitalizedMember1() throws Throwable {
		clickHealthQuestionnaire1.click();

		String dropDownValue1 = elib.getExcelData("MedicalHistory", 10, 1);
		String reason = elib.getExcelData("MedicalHistory", 11, 1);
		if (dropDownValue1.equalsIgnoreCase("yes")) {

			Select sel = new Select(reasonForTreatmentdropDown1);
			sel.selectByValue("YES");

			reasonTextBox1.sendKeys(reason);
		} else {
			Select sel = new Select(reasonForTreatmentdropDown1);
			sel.selectByValue("NO");

		}
	}

	public void diagnosedAndhospitalizedMember2() throws Throwable {

		String dropDownValue2 = elib.getExcelData("MedicalHistory", 12, 1);

		String reason = elib.getExcelData("MedicalHistory", 13, 1);

		if (dropDownValue2.equalsIgnoreCase("yes")) {

			Select sel = new Select(reasonForTreatmentdropDown2);

			sel.selectByValue("YES");

			reasonTextBox2.sendKeys(reason);
		} else {
			Select sel = new Select(reasonForTreatmentdropDown2);

			sel.selectByValue("NO");

		}
	}

	@FindBy(id = "selectbox_HEDTravelClaimPolicy_1")

	WebElement travelPolicyDropDown1;

	@FindBy(id = "selectbox_HEDTravelClaimPolicy_2")

	WebElement travelPolicyDropDown2;

	@FindBy(id = "desc-HEDTravelClaimPolicy-1")

	WebElement travelPolicyTextBox1;

	@FindBy(id = "desc-HEDTravelClaimPolicy-2")

	WebElement travelPolicyTextBox2;

	public void travelPloicyMember1() throws Throwable {
		// clickHealthQuestionnaire2.click();

		clickHealthQuestionnaire3.click();

		String dropDownValue1 = elib.getExcelData("MedicalHistory", 21, 1);
		String reason = elib.getExcelData("MedicalHistory", 21, 1);
		if (dropDownValue1.equalsIgnoreCase("yes")) {

			Select sel = new Select(travelPolicyDropDown1);
			sel.selectByValue("YES");

			travelPolicyTextBox1.sendKeys(reason);
		} else {
			Select sel = new Select(travelPolicyDropDown1);
			sel.selectByValue("NO");

		}
	}

	public void travelPloicyMember2() throws Throwable {

		String dropDownValue2 = elib.getExcelData("MedicalHistory", 23, 1);

		String reason = elib.getExcelData("MedicalHistory", 24, 1);

		if (dropDownValue2.equalsIgnoreCase("yes")) {

			Select sel = new Select(travelPolicyDropDown2);

			sel.selectByValue("YES");

			travelPolicyTextBox2.sendKeys(reason);
		} else {
			Select sel = new Select(travelPolicyDropDown2);

			sel.selectByValue("NO");

		}
	}

	public void next() throws Throwable {
		agreeChekc.click();

		tripChkBox.click();
		// clib.Passscreenshot("ExploreMedical", "ExploreMedicalPage");
	/*	nextBtn.click();
		Thread.sleep(5000);*/
		proceedToPayBtn.click();
	}

	public void exploreProposalsummary() throws InterruptedException {

		nextBtn.click();
		Thread.sleep(5000);
	}
	
	public void exploreProceedtoPay() {
		
		proceedToPayBtn.click();
      log.debug("Clicking on the  proceed to buy button");
      System.out.println("Clicking on the  proceed to buy button");
}
	public void verifyHealthQuestionnaire() {
		Boolean a =clickHealthQuestionnaire1.isDisplayed();
		log.debug("Verfiying the 1st Health Questionnaire is present "+a);
		System.out.println("Verfiying the 1st Health Questionnaire is present "+a);
		
		Boolean b =clickHealthQuestionnair12.isDisplayed();
		log.debug("Verfiying the 2nd Health Questionnaire is present "+b);
		System.out.println("Verfiying the 2nd Health Questionnaire is present "+b);
		
		Boolean c =clickHealthQuestionnair3.isDisplayed();
		log.debug("Verfiying the 3rd Health Questionnaire is present "+c);
		System.out.println("Verfiying the 3rd Health Questionnaire is present "+c);
		
		
	}
	
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[1]/table/tbody/tr[2]/td[1]")

	WebElement personscovered;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[1]/table/tbody/tr[2]/td[2]")

	WebElement dateofb;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[1]/table/tbody/tr[2]/td[3]")

	WebElement passportnu;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[1]/table/tbody/tr[2]/td[4]")

	WebElement verifyaddress;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[2]/table/tbody/tr[2]/td[1]")

	WebElement Appnum;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[2]/table/tbody/tr[2]/td[2]")

	WebElement plntyp;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[2]/table/tbody/tr[2]/td[3]")

	WebElement sumsi;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[2]/table/tbody/tr[2]/td[4]")

	WebElement polyday;
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[2]/table/tbody/tr[2]/td[5]")

	WebElement prme;
	
	@FindBy(xpath = "//*[@id=\"submitPMT\"]/div[3]/div/div[1]/strong")

	WebElement Tprm;
	@FindBy(id = "proposalBoxBack")

	WebElement backbtn;
	@FindBy(id = "proceed_to_pay_explore")

	WebElement prd;
	public void verifyredirectiontoproposoalsummary() {
		String currentURl = BaseClass.driver.getCurrentUrl();
		if (currentURl.contains("https://rhicluat.religarehealthinsurance.com/travel-insurance-explore/explore-filldetails"))
		{
			System.out.println("Redirected to the proposal summary page of explore");
			log.debug("Redirected to the proposal summary page of explore");
		}else {
			System.out.println(" Failed to redirect the  proposal summary page of explore");
			log.debug(" Failed to redirect the  proposal summary page of explore");
		}}
	
	
	public void verifyproposalsummarydetails() {
		Boolean a  = prd.isDisplayed();
		log.debug("Checking the Proceed button is present "+a);
		System.out.println("Checking the Proceed button is present "+a);
		
		Boolean b = backbtn.isDisplayed();
		log.debug("Checking the back button is present "+b);
		System.out.println("Checking the back button is present "+b);
		
		String si = sumsi.getText();
		log.debug("The Sum Insured is "+si);
		System.out.println("The Sum Insured is "+si);
		
		String top = Tprm.getText();
		log.debug("The Total Premium is "+top);
		System.out.println("The Total Premium is "+top);
		
		String pr = prme.getText();
		log.debug("The Premium is "+pr);
		System.out.println("The Premium is "+pr);
		
		String policyprd= polyday.getText();
		log.debug("The Policy Period is "+policyprd);
		System.out.println("The Policy Period is "+policyprd);
		
		String plan = plntyp.getText();
		log.debug("The plan type is " +plan);
		System.out.println("The plan type is " +plan);
		
		String ap = Appnum.getText();
		log.debug("The Application Number is " +ap);
		System.out.println("The Application Number is " +ap);
		
		String pc = personscovered.getText();
		log.debug("The Person covered is " +pc);
		System.out.println("The Person covered is " +pc);
		
		String db = dateofb.getText();
		log.debug("The Date of birth is " +db);
		System.out.println("The Date of birth is " +db);
		
		String psno= passportnu.getText();
		log.debug("The passport number is " +db);
		System.out.println("The passport number is " +db);
		
		String add= verifyaddress.getText();
		log.debug("The Address is " +add);
		System.out.println("The Address is " +add);
		
	}
	
}
	
}
