package com.religare.objectrepository;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class ExploreFirstPage {
	
	WebDriverWait wait = new WebDriverWait(BaseClass.driver, 10);
	Logger log = Logger.getLogger("devpinoyLogger");
	JavascriptExecutor executor = (JavascriptExecutor) BaseClass.driver;

	WebDriverCommonLib clib = new WebDriverCommonLib();

	ExcelLib elib = new ExcelLib();

	JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;

	// For Trip Type

	@FindBy(id = "tripType")

	WebElement tripTypeDropDown;

	// For traveling

	@FindBy(id = "travellingTo")

	WebElement travellingToDropDown;

	// For maximumTriPduration

	@FindBy(id = "maximumtripduration")

	WebElement maximumTripDuration;

	// Date from

	@FindBy(id = "from")

	WebElement fromDate;

	@FindBy(xpath = "//select[@class='ui-datepicker-month']")

	WebElement monthPicker;

	@FindBy(xpath = "//select[@class='ui-datepicker-yeat']")

	WebElement yearPicker;

	// String day1 =elib.getExcelData("testSpecificData", 4, 1);
	// For 31 days in a month
	@FindBy(xpath = "//a[contains(text(), '1') and @class ='ui-state-default']")

	WebElement day1;

	@FindBy(xpath = "//a[contains(text(), '2') and @class ='ui-state-default']")

	WebElement day2;

	@FindBy(xpath = "//a[contains(text(), '3') and @class ='ui-state-default']")

	WebElement day3;

	@FindBy(xpath = "//a[contains(text(), '4') and @class ='ui-state-default']")

	WebElement day4;

	@FindBy(xpath = "//a[contains(text(), '5') and @class ='ui-state-default']")

	WebElement day5;

	@FindBy(xpath = "//a[contains(text(), '6') and @class ='ui-state-default']")

	WebElement day6;

	@FindBy(xpath = "//a[contains(text(), '7') and @class ='ui-state-default']")

	WebElement day7;

	@FindBy(xpath = "//a[contains(text(), '8') and @class ='ui-state-default']")

	WebElement day8;

	@FindBy(xpath = "//a[contains(text(), '9') and @class ='ui-state-default']")

	WebElement day9;

	@FindBy(xpath = "//a[contains(text(), '10') and @class ='ui-state-default']")

	WebElement day10;

	@FindBy(xpath = "//a[contains(text(), '11') and @class ='ui-state-default']")

	WebElement day11;

	// @FindBy(xpath ="//a[contains(text(), '12') and @class ='ui-state-default']")
	@FindBy(xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr[3]/td[2]/a[@class='ui-state-default' and text()=12]")

	WebElement day12;

	@FindBy(xpath = "//a[contains(text(), '13') and @class ='ui-state-default']")

	WebElement day13;

	@FindBy(xpath = "//a[contains(text(), '14') and @class ='ui-state-default']")

	WebElement day14;

	@FindBy(xpath = "//a[contains(text(), '15') and @class ='ui-state-default']")

	WebElement day15;

	@FindBy(xpath = "//a[contains(text(), '16') and @class ='ui-state-default']")

	WebElement day16;

	@FindBy(xpath = "//a[contains(text(), '17') and @class ='ui-state-default']")

	WebElement day17;

	@FindBy(xpath = "//a[contains(text(), '18') and @class ='ui-state-default']")

	WebElement day18;

	@FindBy(xpath = "//a[contains(text(), '19') and @class ='ui-state-default']")

	WebElement day19;

	@FindBy(xpath = "//a[contains(text(), '20') and @class ='ui-state-default']")

	WebElement day20;

	@FindBy(xpath = "//a[contains(text(), '21') and @class ='ui-state-default']")

	WebElement day21;

	@FindBy(xpath = "//a[contains(text(), '22') and @class ='ui-state-default']")

	WebElement day22;

	@FindBy(xpath = "//a[contains(text(), '23') and @class ='ui-state-default']")

	WebElement day23;

	@FindBy(xpath = "//a[contains(text(), '24') and @class ='ui-state-default']")

	WebElement day24;

	@FindBy(xpath = "//a[contains(text(), '25') and @class ='ui-state-default']")

	WebElement day25;

	@FindBy(xpath = "//a[contains(text(), '26') and @class ='ui-state-default']")

	WebElement day26;

	@FindBy(xpath = "//a[contains(text(), '27') and @class ='ui-state-default']")

	WebElement day27;

	@FindBy(xpath = "//a[contains(text(), '28') and @class ='ui-state-default']")

	WebElement day28;

	@FindBy(xpath = "//a[contains(text(), '29') and @class ='ui-state-default']")

	WebElement day29;

	@FindBy(xpath = "//a[contains(text(), '30') and @class ='ui-state-default']")

	WebElement day30;

	@FindBy(xpath = "//a[contains(text(), '31') and @class ='ui-state-default']")

	WebElement day31;
	@FindBy(id ="travelPremium")
	WebElement premium1;
	@FindBy(id ="travelPremiumgold")
	WebElement premiumgold1;
	@FindBy(id ="travelPremiumpletinum")
	WebElement premiumplat1;
	@FindBy(id = "to")

	WebElement toDate;

	@FindBy(id = "age1")

	WebElement fromAgeDropDownMember1;

	@FindBy(id = "age2")

	WebElement fromAgeDropDownMember2;

	@FindBy(id = "age3")

	WebElement fromAgeDropDownMember3;

	@FindBy(id = "age4")

	WebElement fromAgeDropDownMember4;

	@FindBy(id = "age5")

	WebElement fromAgeDropDownMember5;

	@FindBy(id = "age6")

	WebElement fromAgeDropDownMember6;

	@FindBy(id = "mobile")

	WebElement enterMobileNumberTextbox;

	@FindBy(id = "txtIconPlus_id") // txtIconPlus_id

	WebElement totalNumberIncrease;

	@FindBy(id = "txtIconMinus_id")

	WebElement totalNumberMinus;

	@FindBy(id = "pedQuestion-1")
	

	WebElement pedRadioBtnYes;

	@FindBy(id ="noOfTravellers1")
	WebElement totalmem1;
	
	@FindBy(id = "pedQuestion-2")

	WebElement pedRadioBtnNo;

	@FindBy(id = "goldplan-1")

	WebElement premiumBtn1;

	@FindBy(id = "goldplan-2")

	WebElement premiumBtn2;

	@FindBy(id = "checkn")

	WebElement ReadAgreeCheckbox;

	@FindBy(id = "carebuynowimage")

	WebElement careBuyNowBtn;

	@FindBy(xpath = "//div[contains(@class, 'ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min')]")

	WebElement slider;

	@FindBy(xpath = "//div[@class='slider-range newslider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']/div[1]/span[2]")

	WebElement moveLeftSlider;
	@FindBy(id ="noOfTravellers1")
	WebElement totalmem;
	
	@FindBy(id="noday")
	WebElement numberofdays;

	@FindBy(id="noOfTravellers")
	WebElement numberoftravelers;
	@FindBy(id="pedCd")
	WebElement verifypedinedit;

	@FindBy(id="sumInsuredTravel")
	WebElement verifysieditsingle;
	
	@FindBy(id="sumInsuredTravel1")
	WebElement verifysieditmultiple;
	@FindBy(xpath="//a[@class='editQuote']")
	WebElement edit;
	
	@FindBy(id="checkCurrentData")
	WebElement currentdate;
	// >Asia</option>
	// <option value="2">Africa</option>
	// <option value="3">Europe</option>
	// <option value="5">Worldwide</option>
	// <option value="4">Worldwide Excl. US</option>
	// <option value="6">WW-Excl. US / Canada
	private String tripType ;
	private String travelingTo; 
	private String month;
	private String day;
	private String totalMember;
	private String pedRadioButton;
	private String premiumRadioButton;
	private String age1;
	private String age2;
	private String age3;
	private String age4;
	private String age5;
	private String age6;
	
	private String currentURl;
	
	
	
	public void multiTripType() throws Throwable {
		tripType = elib.getExcelData("testSpecificData", 1, 1); // tripType
	
		travelingTo=  elib.getExcelData("testSpecificData", 2, 1); // travellingTo

		String maximumTripDuration1 = elib.getExcelData("testSpecificData", 3, 1); // maximumTripDuration
		try {
			clib.select(tripTypeDropDown, tripType);
			log.debug("Selecting the Trip Type as " +tripType);

			//clib.waitForElementPresent(travellingToDropDown);
	//		WebDriverWait wait2 = new WebDriverWait(BaseClass.driver, 10);
	//		wait2.until(ExpectedConditions.visibilityOf(travellingToDropDown));
			
			Thread.sleep(2100);

			if (travelingTo.equalsIgnoreCase("WW-Excl. US / Canada")) { //
				clib.select(travellingToDropDown, "WW-Excl. US / Canada");
			} else if (travelingTo.equalsIgnoreCase("Africa")) { //
				clib.select(travellingToDropDown, "Africa");
			} else if (travelingTo.equalsIgnoreCase("Europe")) { //
				clib.select(travellingToDropDown, "Europe");
			} else if (travelingTo.equalsIgnoreCase("Worldwide")) { //
				clib.select(travellingToDropDown, "Worldwide");
			}
			log.debug("Selecting the TravellingTo as " +travelingTo);
		} catch (Exception e) {
			e.getMessage();
		}
		if (maximumTripDuration1.equalsIgnoreCase("sixty")) {
			jse.executeScript("arguments[0].value='60'", maximumTripDuration);
			log.debug("Selecting the maximumTripDuration as " +maximumTripDuration1);
		}
	}
public void endmonth() throws Throwable {
	month = elib.getExcelData("testSpecificData", 5, 3); // forMonths
	
//	WebDriverWait wait3 = new WebDriverWait(BaseClass.driver, 10);
//	wait3.until(ExpectedConditions.visibilityOf(fromDate));

//	clib.waitForElementPresent(fromDate);
	
	Thread.sleep(2100);

	toDate.click();
	
//	WebDriverWait wait4 = new WebDriverWait(BaseClass.driver, 10);
//	wait4.until(ExpectedConditions.visibilityOf(monthPicker));
	
	Thread.sleep(2100);

	//clib.waitForElementPresent(monthPicker);

	monthPicker.click();

	if (month.equalsIgnoreCase("Jan")) {
		clib.select(monthPicker, "Jan");
	} else if (month.equalsIgnoreCase("Feb")) {
		clib.select(monthPicker, "Feb");
	} else if (month.equalsIgnoreCase("Mar")) {
		clib.select(monthPicker, "Mar");
	} else if (month.equalsIgnoreCase("Apr")) {
		clib.select(monthPicker, "Apr");
	} else if (month.equalsIgnoreCase("May")) {
		clib.select(monthPicker, "May");
	} else if (month.equalsIgnoreCase("Jun")) {
		clib.select(monthPicker, "Jun");
	} else if (month.equalsIgnoreCase("Jul")) {
		clib.select(monthPicker, "Jul");
	} else if (month.equalsIgnoreCase("Aug")) {
		clib.select(monthPicker, "Aug");
	} else if (month.equalsIgnoreCase("Sep")) {
		clib.select(monthPicker, "Sep");
	} else if (month.equalsIgnoreCase("Oct")) {
		clib.select(monthPicker, "Oct");
	} else if (month.equalsIgnoreCase("Nov")) {
		clib.select(monthPicker, "Nov");
	} else if (month.equalsIgnoreCase("Dec")) {
		clib.select(monthPicker, "Dec");
	}
	log.debug("Selecting the month as  "+month);
	
}
public void endday() throws Throwable {
	day = elib.getExcelData("testSpecificData", 4, 3); // forDaysbetween 1 - 31

	if (day.equalsIgnoreCase("one")) {
		day1.click();
	} else if (day.equalsIgnoreCase("two"))
		day2.click();
	else if (day.equalsIgnoreCase("three"))
		day3.click();
	else if (day.equalsIgnoreCase("four"))
		day4.click();
	else if (day.equalsIgnoreCase("five"))
		day5.click();
	else if (day.equalsIgnoreCase("six"))
		day6.click();
	else if (day.equalsIgnoreCase("seven"))
		day7.click();
	else if (day.equalsIgnoreCase("eight"))
		day8.click();
	else if (day.equalsIgnoreCase("nine"))
		day9.click();
	else if (day.equalsIgnoreCase("ten"))
		day10.click();
	else if (day.equalsIgnoreCase("eleven"))
		day11.click();
	else if (day.equalsIgnoreCase("twelve"))
		day12.click();
	else if (day.equalsIgnoreCase("thirteen"))
		day13.click();
	else if (day.equalsIgnoreCase("fourteen"))
		day14.click();
	else if (day.equalsIgnoreCase("fifteen"))
		day15.click();
	else if (day.equalsIgnoreCase("sixteen"))
		day16.click();
	else if (day.equalsIgnoreCase("seventeen"))
		day17.click();
	else if (day.equalsIgnoreCase("eighteen"))
		day18.click();
	else if (day.equalsIgnoreCase("nineteen"))
		day19.click();
	else if (day.equalsIgnoreCase("twenty"))
		day20.click();
	else if (day.equalsIgnoreCase("twenty one"))
		day21.click();
	else if (day.equalsIgnoreCase("twenty two"))
		day22.click();
	else if (day.equalsIgnoreCase("twenty three"))
		day23.click();
	else if (day.equalsIgnoreCase("twenty four"))
		day24.click();
	else if (day.equalsIgnoreCase("twenty five"))
		day25.click();
	else if (day.equalsIgnoreCase("twenty six"))
		day26.click();
	else if (day.equalsIgnoreCase("twent seven"))
		day27.click();
	else if (day.equalsIgnoreCase("twenty eight"))
		day28.click();
	else if (day.equalsIgnoreCase("twenty nine"))
		day29.click();
	else if (day.equalsIgnoreCase("thirty"))
		day30.click();
	else if (day.equalsIgnoreCase("thirty one"))
		day31.click();
	log.debug("Selecting the day as  "+day);
	
}
	public void month() throws Throwable {

		month = elib.getExcelData("testSpecificData", 5, 1); // forMonths
		
//		WebDriverWait wait3 = new WebDriverWait(BaseClass.driver, 10);
//		wait3.until(ExpectedConditions.visibilityOf(fromDate));

	//	clib.waitForElementPresent(fromDate);
		
		Thread.sleep(2100);

		fromDate.click();
		
//		WebDriverWait wait4 = new WebDriverWait(BaseClass.driver, 10);
//		wait4.until(ExpectedConditions.visibilityOf(monthPicker));
		
		Thread.sleep(2100);

		//clib.waitForElementPresent(monthPicker);

		monthPicker.click();

		if (month.equalsIgnoreCase("Jan")) {
			clib.select(monthPicker, "Jan");
		} else if (month.equalsIgnoreCase("Feb")) {
			clib.select(monthPicker, "Feb");
		} else if (month.equalsIgnoreCase("Mar")) {
			clib.select(monthPicker, "Mar");
		} else if (month.equalsIgnoreCase("Apr")) {
			clib.select(monthPicker, "Apr");
		} else if (month.equalsIgnoreCase("May")) {
			clib.select(monthPicker, "May");
		} else if (month.equalsIgnoreCase("Jun")) {
			clib.select(monthPicker, "Jun");
		} else if (month.equalsIgnoreCase("Jul")) {
			clib.select(monthPicker, "Jul");
		} else if (month.equalsIgnoreCase("Aug")) {
			clib.select(monthPicker, "Aug");
		} else if (month.equalsIgnoreCase("Sep")) {
			clib.select(monthPicker, "Sep");
		} else if (month.equalsIgnoreCase("Oct")) {
			clib.select(monthPicker, "Oct");
		} else if (month.equalsIgnoreCase("Nov")) {
			clib.select(monthPicker, "Nov");
		} else if (month.equalsIgnoreCase("Dec")) {
			clib.select(monthPicker, "Dec");
		}
		log.debug("Selecting the month as  "+month);
	}

	public void day() throws Throwable {

		day = elib.getExcelData("testSpecificData", 4, 1); // forDaysbetween 1 - 31

		if (day.equalsIgnoreCase("one")) {
			day1.click();
		} else if (day.equalsIgnoreCase("two"))
			day2.click();
		else if (day.equalsIgnoreCase("three"))
			day3.click();
		else if (day.equalsIgnoreCase("four"))
			day4.click();
		else if (day.equalsIgnoreCase("five"))
			day5.click();
		else if (day.equalsIgnoreCase("six"))
			day6.click();
		else if (day.equalsIgnoreCase("seven"))
			day7.click();
		else if (day.equalsIgnoreCase("eight"))
			day8.click();
		else if (day.equalsIgnoreCase("nine"))
			day9.click();
		else if (day.equalsIgnoreCase("ten"))
			day10.click();
		else if (day.equalsIgnoreCase("eleven"))
			day11.click();
		else if (day.equalsIgnoreCase("twelve"))
			day12.click();
		else if (day.equalsIgnoreCase("thirteen"))
			day13.click();
		else if (day.equalsIgnoreCase("fourteen"))
			day14.click();
		else if (day.equalsIgnoreCase("fifteen"))
			day15.click();
		else if (day.equalsIgnoreCase("sixteen"))
			day16.click();
		else if (day.equalsIgnoreCase("seventeen"))
			day17.click();
		else if (day.equalsIgnoreCase("eighteen"))
			day18.click();
		else if (day.equalsIgnoreCase("nineteen"))
			day19.click();
		else if (day.equalsIgnoreCase("twenty"))
			day20.click();
		else if (day.equalsIgnoreCase("twenty one"))
			day21.click();
		else if (day.equalsIgnoreCase("twenty two"))
			day22.click();
		else if (day.equalsIgnoreCase("twenty three"))
			day23.click();
		else if (day.equalsIgnoreCase("twenty four"))
			day24.click();
		else if (day.equalsIgnoreCase("twenty five"))
			day25.click();
		else if (day.equalsIgnoreCase("twenty six"))
			day26.click();
		else if (day.equalsIgnoreCase("twent seven"))
			day27.click();
		else if (day.equalsIgnoreCase("twenty eight"))
			day28.click();
		else if (day.equalsIgnoreCase("twenty nine"))
			day29.click();
		else if (day.equalsIgnoreCase("thirty"))
			day30.click();
		else if (day.equalsIgnoreCase("thirty one"))
			day31.click();
		log.debug("Selecting the day as  "+day);
	}

	public void totalMember() throws Throwable {

		 totalMember = elib.getExcelData("testSpecificData", 8, 1);

		BaseClass.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		if (totalMember.equalsIgnoreCase("one")) {
			wait.until(ExpectedConditions.visibilityOf(totalNumberMinus));
		//	clib.waitForElementPresent(totalNumberMinus);
			totalNumberMinus.click();
		} else if (totalMember.equalsIgnoreCase("two")) {
			System.out.println("Total members are two");
		} else if (totalMember.equalsIgnoreCase("three")) {
			wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			totalNumberIncrease.click();
		} else if (totalMember.equalsIgnoreCase("four")) {
			Thread.sleep(2100);
			
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
			//clib.waitForElementPresent(totalNumberIncrease);
			totalNumberIncrease.click();
			Thread.sleep(2100);
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			totalNumberIncrease.click();
		} else if (totalMember.equalsIgnoreCase("five")) {
			//wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
			//clib.waitForElementPresent(totalNumberIncrease);
			Thread.sleep(2100);
			totalNumberIncrease.click();
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			Thread.sleep(2100);
			totalNumberIncrease.click();
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
			//clib.waitForElementPresent(totalNumberIncrease);
			Thread.sleep(2100);
			totalNumberIncrease.click();
		} else if (totalMember.equalsIgnoreCase("six")) {
			Thread.sleep(4000);
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			totalNumberIncrease.click();
			Thread.sleep(2100);
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			totalNumberIncrease.click();
			Thread.sleep(2100);
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			totalNumberIncrease.click();
		//	wait.until(ExpectedConditions.visibilityOf(totalNumberIncrease));
		//	clib.waitForElementPresent(totalNumberIncrease);
			Thread.sleep(2100);
			totalNumberIncrease.click();
			log.debug("Selecting the Total Members as  "+totalMember);
		}

	}

	public void phoneNumber(String number) {

		clib.waitForPageToLoad();

		enterMobileNumberTextbox.sendKeys(number);
		log.debug("Entering the Mobile Number as  "+number);
	}

	public void pedRadioBtn() throws Throwable {

		 pedRadioButton = elib.getExcelData("testSpecificData", 6, 1);

		if (pedRadioButton.equalsIgnoreCase("Yes")) {
			pedRadioBtnYes.click();
		}

		else if (pedRadioButton.equalsIgnoreCase("No")) {
			pedRadioBtnNo.click();
		}
		log.debug("Selecting the PED Radio Button as  "+pedRadioButton);
	}

	public void premiumBtn() throws Throwable {

		 premiumRadioButton = elib.getExcelData("testSpecificData", 7, 1);
		try {
			if (premiumRadioButton.equalsIgnoreCase("GoldPlan")) {
				premiumBtn1.click();
			} else if (premiumRadioButton.equalsIgnoreCase("Platinum Plan")) {
				premiumBtn2.click();
			}
		} catch (Exception e) {
			e.getMessage();
		}
		//log.debug("Selecting the Premium Radio Button as  "+premiumRadioButton);
	}

	public void slider() {

		org.openqa.selenium.Dimension dim = slider.getSize();
		int x = dim.getWidth();
		Actions actions = new Actions(BaseClass.driver);
		actions.clickAndHold(slider).moveByOffset(x - 350, 0).release().build().perform();
		log.debug("Moving the slider ");
		// Actions act = new Actions(BaseClass.driver);
		//
		// act.dragAndDrop(moveRightSlider, moveLeftSlider).perform();
		//
		// act.dragAndDrop(moveLeftSlider, moveRSlider).perform(ight);

	}

	public void agreeCheckBox() {

		ReadAgreeCheckbox.click();
		log.debug("Agree the check Box");
	}

	public void careBuyBtn() {

		careBuyNowBtn.click();
		
	}
	
	public void ageOfMember1() throws Throwable {

		// String totalMember = elib.getExcelData("testSpecificData", 8, 1);
		// //forTotalMember = 1

		 age1 = elib.getExcelData("testSpecificData", 9, 1); // forAgeofMember1DropDown.
		
	//	wait.until(ExpectedConditions.visibilityOf(fromAgeDropDownMember1));

	//	clib.waitForElementPresent(fromAgeDropDownMember1);
		
		Thread.sleep(2100);

		if (age1.equalsIgnoreCase("one")) {
			jse.executeScript("arguments[0].value='41-60'", fromAgeDropDownMember1);
		} else if (age1.equalsIgnoreCase("two")) {
			jse.executeScript("arguments[0].value='61-70'", fromAgeDropDownMember1);
		}

		else if (age1.equalsIgnoreCase("three")) {
			jse.executeScript("arguments[0].value='71-80'", fromAgeDropDownMember1);
		}

		else if (age1.equalsIgnoreCase("four")) {
			jse.executeScript("arguments[0].value='81-99'", fromAgeDropDownMember1);
		}
		log.debug("Selecting the Age1 as " +age1);
	}

	public void ageOfMember2() throws Throwable {

		 age2 = elib.getExcelData("testSpecificData", 10, 1); // forAgeofMember2DropDown.

		if (age2.equalsIgnoreCase("one")) {
			jse.executeScript("arguments[0].value='41-60'", fromAgeDropDownMember2);
		} else if (age2.equalsIgnoreCase("two")) {
			jse.executeScript("arguments[0].value='61-70'", fromAgeDropDownMember2);
		}

		else if (age2.equalsIgnoreCase("three")) {
			jse.executeScript("arguments[0].value='71-80'", fromAgeDropDownMember2);
		}

		else if (age2.equalsIgnoreCase("four")) {
			jse.executeScript("arguments[0].value='81-99'", fromAgeDropDownMember2);
		}
		log.debug("Selecting the Age2 as " +age2);
	}

	public void ageOfMember3() throws Throwable {

		 age3 = elib.getExcelData("testSpecificData", 11, 1); //// forAgeofMember3DropDown.

		if (age3.equalsIgnoreCase("one")) {
			jse.executeScript("arguments[0].value='41-60'", fromAgeDropDownMember3);
		} else if (age3.equalsIgnoreCase("two")) {
			jse.executeScript("arguments[0].value='61-70'", fromAgeDropDownMember3);
		}

		else if (age3.equalsIgnoreCase("three")) {
			jse.executeScript("arguments[0].value='71-80'", fromAgeDropDownMember3);
		}

		else if (age3.equalsIgnoreCase("four")) {
			jse.executeScript("arguments[0].value='81-99'", fromAgeDropDownMember3);
		}
		log.debug("Selecting the Age3 as " +age3);
	}

	public void ageOfMember4() throws Throwable {

		 age4 = elib.getExcelData("testSpecificData", 12, 1); //// forAgeofMember3DropDown.

		if (age4.equalsIgnoreCase("one")) {
			jse.executeScript("arguments[0].value='41-60'", fromAgeDropDownMember4);
		} else if (age4.equalsIgnoreCase("two")) {
			jse.executeScript("arguments[0].value='61-70'", fromAgeDropDownMember4);
		}

		else if (age4.equalsIgnoreCase("three")) {
			jse.executeScript("arguments[0].value='71-80'", fromAgeDropDownMember4);
		}

		else if (age4.equalsIgnoreCase("four")) {
			jse.executeScript("arguments[0].value='81-99'", fromAgeDropDownMember4);
		}
		log.debug("Selecting the Age4 as " +age4);
	}

	public void ageOfMember5() throws Throwable {

		 age5 = elib.getExcelData("testSpecificData", 13, 1); //// forAgeofMember3DropDown.

		if (age5.equalsIgnoreCase("one")) {
			jse.executeScript("arguments[0].value='41-60'", fromAgeDropDownMember5);
		} else if (age5.equalsIgnoreCase("two")) {
			jse.executeScript("arguments[0].value='61-70'", fromAgeDropDownMember5);
		}

		else if (age5.equalsIgnoreCase("three")) {
			jse.executeScript("arguments[0].value='71-80'", fromAgeDropDownMember5);
		}

		else if (age5.equalsIgnoreCase("four")) {
			jse.executeScript("arguments[0].value='81-99'", fromAgeDropDownMember5);
		}
		log.debug("Selecting the Age5 as " +age5);
	}

	public void ageOfMember6() throws Throwable {

		 age6 = elib.getExcelData("testSpecificData", 14, 1); //// forAgeofMember3DropDown.

		if (age6.equalsIgnoreCase("one")) {
			jse.executeScript("arguments[0].value='41-60'", fromAgeDropDownMember6);
		} else if (age6.equalsIgnoreCase("two")) {
			jse.executeScript("arguments[0].value='61-70'", fromAgeDropDownMember6);
		}

		else if (age6.equalsIgnoreCase("three")) {
			jse.executeScript("arguments[0].value='71-80'", fromAgeDropDownMember6);
		}

		else if (age6.equalsIgnoreCase("four")) {
			jse.executeScript("arguments[0].value='81-99'", fromAgeDropDownMember6);
		}
		log.debug("Selecting the Age6 as " +age6);
	}
	
	public void getCurrentURLAssure() throws Throwable {

		currentURl = BaseClass.driver.getCurrentUrl();
		if (currentURl.equals("https://rhicluat.religarehealthinsurance.com/travel-insurance-explore.html"))
		{
			System.out.println("Redirected to the Quotation page when Explore Option is clicked");
			log.debug("Redirected to the Quotation page when Explore Option is clicked");
		}else {
			System.out.println("Failed to Redirect to the Quotation page when Explore Option is clicked");
			log.debug("Failed to Redirect to the Quotation page when Explore Option is clicked");
		}}
		
	public void VerifyTripType() throws InterruptedException {
		Thread.sleep(2000);
		Boolean Triptype = tripTypeDropDown.isDisplayed();
		String text = tripTypeDropDown.getText();
		log.debug("Verifying the trip Type dropDown is present  " +Triptype);
		System.out.println("Verifying the trip Type dropDown is present  " +Triptype);
		log.debug("By default in the trip type dropdown"+ text + "is displayed " );
		System.out.println("By default in the trip type dropdown"+ text + "is displayed " );
	}
	
	public void VerifyRegion() throws InterruptedException {
		Thread.sleep(1500);
		Boolean region = travellingToDropDown.isDisplayed();
		String text = travellingToDropDown.getText();
		log.debug("Verifying the trip Type dropDown is present " +region);
		System.out.println("Verifying the trip Type dropDown is present " +region);
		log.debug("By default in the Region dropdown"+ text + "is displayed  ");
		System.out.println("By default in the Region dropdown"+ text + "is displayed  ");
	}
	
	public void verifyStartdate() {
		Boolean sd = fromDate.isDisplayed();
		log.debug("Verifying the Start date calendar is present " +sd);
		System.out.println("Verifying the Start date calendar is present " +sd);
		
		
		
		
	}
	public void verifyenddate() throws InterruptedException {
		Thread.sleep(3000);
		Boolean ed = toDate.isDisplayed();
		log.debug("Verifying the End date calendar is present " +ed);
		System.out.println("Verifying the End date calendar is present " +ed);
	}
	
	
public void verifyTotalmember() {
	Boolean tm =totalNumberIncrease.isDisplayed();
	Boolean tmn = totalNumberMinus.isDisplayed();
	String text = totalmem1.getText();
	log.debug("Verifying the Total Member '+' icon is present " +tm);
	System.out.println("Verifying the Total Member '+' icon is present " +tm);
	log.debug("By default in the total members "+ text + "is selected  ");
	System.out.println("By default in the total members "+ text + "is selected  ");
	log.debug("Verifying the Total Member icon '-' is present " +tmn);
	System.out.println("Verifying the Total Member '-' icon is present " +tmn);
}

public void verifymobile() {
	Boolean mm =enterMobileNumberTextbox.isDisplayed();
	log.debug("Verifying the Mobile Number text field is present " +mm);
	System.out.println("Verifying the Mobile Number text field is present " +mm);
}
	public void verifyAgeoftravel() throws InterruptedException {
		Thread.sleep(4000);
		Boolean atd =fromAgeDropDownMember1.isDisplayed();
		log.debug("Verifying the Age of Travel1 Drop down is present " +atd);
		try {
		Boolean atd2=fromAgeDropDownMember2.isDisplayed();
		log.debug("Verifying the Age of Travel2 Drop down is present " +atd2);
		
		}catch(Exception e) {}
		
		System.out.println("Verifying the Age of Travel Drop down is present " +atd);
		
	}
public void verifyRadiobutton() throws InterruptedException {
	Thread.sleep(4000);
	Boolean a =pedRadioBtnYes.isEnabled();
	log.debug("Verifying the PED of radio button 'yes' is present " +a);
	System.out.println("Verifying the PED of radio button 'yes' is present" +a);
	Boolean n =pedRadioBtnNo.isEnabled();
	log.debug("Verifying the PED of radio button 'No' is present " +n);
	System.out.println("Verifying the PED of radio button 'No' is present " +n);
}

public void verifyslider() {
	Boolean si =moveLeftSlider.isDisplayed();
	log.debug("Verifying the Slider  is present " +si);
	System.out.println("Verifying the Slider  is present " +si);
}
public void verifyGetquoteandbuy(){
	Boolean by =careBuyNowBtn.isDisplayed();
	log.debug("Verifying the Get Quote and Buy link  is present " +by);
	System.out.println("Verifying the Get Quote and Buy link  is present " +by);
	
}
public void verifyTermandconditioncheckbox(){
	Boolean tc =ReadAgreeCheckbox.isDisplayed();
	Boolean tcv = ReadAgreeCheckbox.isSelected();
	log.debug("Verifying the Term and condition checkbox  is selected " +tcv);
	System.out.println("Verifying the Term and condition checkbox  is selected " +tcv);
	log.debug("Verifying the Term and condition checkbox  is present " +tc);
	System.out.println("Verifying the Term and condition checkbox  is present " +tc);
}	


public void verifypremium() throws InterruptedException {
	
	Thread.sleep(1000);
	if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa"))) {
		
		String prm = premium1.getAttribute("innerHTML");
		log.debug("Your premium is " + prm);
		System.out.println("Your premium is " + prm);
		}else  {
			Thread.sleep(5000);
			String gld1 = premiumgold1.getAttribute("innerHTML");
			log.debug("Your Gold premium is " + gld1);
			System.out.println("Your Gold premium is " + gld1);
			Thread.sleep(2000);
			String ptml = premiumplat1.getAttribute("innerHTML");
			log.debug("Your platinum premium is " + ptml);
			System.out.println("Your platinum premium is " + ptml);		
		}
	
}



public void verifyingquotepagewithouttermsandcondition() throws Throwable {
	Thread.sleep(2000);
	multiTripType();
	Thread.sleep(2500);
	try {
		BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
	month();
	day();
	if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa"))) {
	endmonth();
	endday();
	}else {
		Boolean dd= maximumTripDuration.isDisplayed();
		String dt= maximumTripDuration.getText();
		log.debug("Checking the Duration drop down is present "+dd);
		System.out.println("Checking the Duration drop down is present "+dd);
		log.debug("By default the Duration drop down conatins  "+dt);
		System.out.println("By default the Duration drop down conatins  "+dt);
	}
	Thread.sleep(5000);
	totalMember();
	phoneNumber("8880950593");
	ageOfMember1();
	try {
		ageOfMember2();
		ageOfMember3();
		ageOfMember4();
		ageOfMember5();
		ageOfMember6();
	}catch(Exception e) {}
	pedRadioBtn();
	try {
		premiumBtn();
	}catch(Exception e) {}
	slider();
	Thread.sleep(3000);
	//verifypremium();
	agreeCheckBox();
	careBuyBtn();
		Thread.sleep(1500);
		Alert al = BaseClass.driver.switchTo().alert();
		  String errorMessagePopUp = al.getText();
		
		  System.out.println("Verify the error message if we deselect the terms and condition checkbox as " + errorMessagePopUp);
		  log.debug("Verify the error message if we deselect the terms and condition checkbox as " + errorMessagePopUp);
		  al.accept();

}
 
public void verifyingquotepagewithoutmobilenumber() throws Throwable {
	BaseClass.driver.navigate().refresh();
	  Thread.sleep(2500);
	 // Thread.sleep(2000);
	  BaseClass.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	multiTripType();
	Thread.sleep(1500);
	((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
	try {
		BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	month();
	day();
	
	if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa"))) {
		endmonth();
		endday();
		}else {}
	Thread.sleep(5000);
	totalMember();
	//phoneNumber("8880950593");
	ageOfMember1();
	try {
		ageOfMember2();
		ageOfMember3();
		ageOfMember4();
		ageOfMember5();
		ageOfMember6();
	}catch(Exception e) {}
	pedRadioBtn();
	try {
		premiumBtn();
	}catch(Exception e) {}
	slider();
	Thread.sleep(2500);
	//verifypremium();
	//agreeCheckBox();
	careBuyBtn();
		Thread.sleep(1500);
		Alert al = BaseClass.driver.switchTo().alert();
		  String errorMessagePopUp = al.getText();
		  System.out.println("Verifying the error if we don't enter the mobile number as " + errorMessagePopUp);
		  log.debug("Verifying the error if we don't enter the mobile number as " + errorMessagePopUp);
		  al.accept();
}

public void verifyingquotepagewithoutStrtdate() throws Throwable {
	BaseClass.driver.navigate().refresh();
	  Thread.sleep(2500);
	 // Thread.sleep(2000);
	  BaseClass.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	multiTripType();
	//month();
	//day();
	Thread.sleep(1500);
	((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
	try {
		BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa"))) {
		endmonth();
		endday();
		}else {}
	Thread.sleep(5000);
	totalMember();
	phoneNumber("8880950593");
	ageOfMember1();
	try {
		ageOfMember2();
		ageOfMember3();
		ageOfMember4();
		ageOfMember5();
		ageOfMember6();
	}catch(Exception e) {}
	pedRadioBtn();
	try {
		premiumBtn();
	}catch(Exception e) {}
	slider();
	Thread.sleep(2500);
	//verifypremium();
	agreeCheckBox();
	careBuyBtn();
		Thread.sleep(1500);
		Alert al = BaseClass.driver.switchTo().alert();
		  String errorMessagePopUp = al.getText();
		  System.out.println("Verifying the error if we don't enter the start date as" + errorMessagePopUp);
		  log.debug("Verifying the error if we don't enter the start date as" + errorMessagePopUp);
		  al.accept();
}
public void verifyingquotepagewithoutenddate() throws Throwable {
	BaseClass.driver.navigate().refresh();
	  Thread.sleep(2500);
	 // Thread.sleep(2000);
	  BaseClass.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	multiTripType();
	Thread.sleep(1500);
	((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
	try {
		BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	month();
	day();
	//endmonth();
	//endday();
	Thread.sleep(5000);
	totalMember();
	phoneNumber("8880950593");
	ageOfMember1();
	try {
		ageOfMember2();
		ageOfMember3();
		ageOfMember4();
		ageOfMember5();
		ageOfMember6();
	}catch(Exception e) {}
	pedRadioBtn();
	try {
		premiumBtn();
	}catch(Exception e) {}
	slider();
	Thread.sleep(2500);
	//verifypremium();
	//agreeCheckBox();
	careBuyBtn();
		Thread.sleep(1500);
		Alert al = BaseClass.driver.switchTo().alert();
		  String errorMessagePopUp = al.getText();
		  System.out.println("Verifying the error if we don't enter the End date as" + errorMessagePopUp);
		  log.debug("Verifying the error if we don't enter the End date as" + errorMessagePopUp);
		  al.accept();
}

public void VerifyingBuyNowbuttonisclickable() throws Throwable {
	BaseClass.driver.navigate().refresh();
	  Thread.sleep(2500);
	 // Thread.sleep(2000);
	  BaseClass.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	multiTripType();
	Thread.sleep(1500);
	((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");
	try {
		BaseClass.driver.findElement(By.id("lhc_need_help_close")).click();

	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	month();
	day();
	
	if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa"))) {
		endmonth();
		endday();
		}else {}
	Thread.sleep(5000);
	totalMember();
	phoneNumber("8880950593");
	ageOfMember1();
	try {
		ageOfMember2();
		ageOfMember3();
		ageOfMember4();
		ageOfMember5();
		ageOfMember6();
	}catch(Exception e) {}
	pedRadioBtn();
	try {
		premiumBtn();
	}catch(Exception e) {}
	slider();
	Thread.sleep(2500);
	verifypremium();
	//agreeCheckBox();
	careBuyBtn();
	  System.out.println("Verifying that Buy Now button is clickable");
	  log.debug("Verifying that Buy Now button is clickable");
	  try {
		  Alert al = BaseClass.driver.switchTo().alert();
		  al.accept();
		  careBuyBtn();
	  }catch(Exception e) {}
	  Thread.sleep(2000);
	  BaseClass.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
}
@FindBy(xpath="//a[@class='editQuote']/span")
WebElement tooltipedit;
public void verifyNoofdaysmembers() throws InterruptedException {
	
	
	Boolean tr =numberoftravelers.isDisplayed();
	log.debug("Verifying the Number of Travellers field present " +tr);
	  System.out.println("Verifying the Number of Travellers field present " +tr);
	Boolean ped=verifypedinedit.isDisplayed();
	log.debug("Verifying the PED field is present " +ped);
	  System.out.println("Verifying the PED field is present " +ped);
	  if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa")))
	  {
	Boolean sisin=verifysieditsingle.isDisplayed();
	log.debug("Verifying the Si field is present " +sisin);
	  System.out.println("Verifying the Si field is present " +sisin);
	  Boolean nody=numberofdays.isDisplayed();
		log.debug("Verifying the Number of days field present " +nody);
		  System.out.println("Verifying the Number of days field present " +nody);
			  
}else if (tripType.equals("Multi") && (travelingTo.equals("Worldwide") || travelingTo.equals("WW-Excl. US / Canada"))){
	Boolean sisinm=verifysieditmultiple.isDisplayed();
	log.debug("Verifying the Si field is present " +sisinm);
	  System.out.println("Verifying the Si field is present " +sisinm);
}
}
public void verifyfilldetailpage() {
	String currentURl = BaseClass.driver.getCurrentUrl();
	if (currentURl.equals("https://rhicluat.religarehealthinsurance.com/travel-insurance-explore/explore-filldetails"))
	{
		System.out.println("Redirected to the fill detail page of explore");
		log.debug("Redirected to the fill detail page of explore");
	}else {
		System.out.println(" Failed to redirect the fill detail page of explore");
		log.debug(" Failed to redirect the fill detail page of explore");
	}}
	

public void verifyeditquotepage() throws InterruptedException {
	Thread.sleep(3000);
	try {
		  Boolean tp =tooltipedit.isDisplayed();
		  log.debug("Verifying the ToolTip of edit is present " +tp);
		  System.out.println("Verifying the ToolTip of edit is present " +tp);
		  }catch(Exception e){}
	//edit.click();
	Thread.sleep(3000);
	clib.waitForPageToLoad();
	VerifyTripType(); VerifyRegion(); verifyStartdate();verifyenddate();verifyAgeoftravel();
	verifyNoofdaysmembers();
	Thread.sleep(3000);
	clib.waitForPageToLoad();
}
}


