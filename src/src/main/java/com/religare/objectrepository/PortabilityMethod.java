package com.religare.objectrepository;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class PortabilityMethod {

	ExcelLib elib = new ExcelLib();

	WebDriverCommonLib clib = new WebDriverCommonLib();
	Logger log = Logger.getLogger("devpinoyLogger");
	JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;

	@FindBy(xpath = "(//div[@class='header_icon_cont'])[1]")

	WebElement portability;

	@FindBy(xpath = "//ul[@class='profile_presentation']/li[1]/a")
	WebElement features;

	@FindBy(xpath = "//ul[@class='profile_presentation']/li[2]/a")
	WebElement policyOption;

	@FindBy(xpath = "//ul[@class='profile_presentation']/li[3]/a")
	WebElement claimprocess;

	@FindBy(xpath = "/html/body/section[2]/div[1]/div/ul/li[4]/a")
	WebElement exclusions;

	@FindBy(xpath = "//div[@class='quote_right_container']/div[1]/p")
	WebElement getquotesinstantly;

	@FindBy(xpath = "//div[@class='backgroundimagetext']/a")

	WebElement getstarted;

	@FindBy(xpath = "(//div[@class='panel-body'])[2]")

	WebElement above45yearsage;

	@FindBy(xpath = "(//div[@class='panel-body'])[1]/div")

	WebElement below45yearsage;

	@FindBy(xpath = "(//div[@class='col-sm-6 col-xs-6 radio_btn_cont'])[1]/label[1]")

	WebElement individualcovertype;

	@FindBy(xpath = "(//div[@class='col-sm-6 col-xs-6 radio_btn_cont'])[2]/label[1]")

	WebElement floatercovertype;

	@FindBy(xpath = "//div[@class='col-sm-6 col-xs-6 radio_btn_cont'][1]/label[1]")

	WebElement individualradiobutton;

	@FindBy(id = "_p_cover_floater")

	WebElement floaterradiobutton;

	@FindBy(id = "_p_totalmembers")

	WebElement totalmembers;

	@FindBy(id = "_p_children")

	WebElement totalchildren;

	@FindBy(xpath = "(//div[@class='resizebtn members_details_btn']/span[2])[1]")

	WebElement decrementmember;

	@FindBy(xpath = "((//div[@class='resizebtn members_details_btn'])[1]/span[1]")

	WebElement incrementmember;

	@FindBy(xpath = "(//div[@class='resizebtn members_details_btn'])[2]/span[1]")

	WebElement childincrementmember;

	@FindBy(id = "_p_mobile")

	WebElement mobilenumber;

	@FindBy(xpath = "//div[@class='select_cont']/select")

	WebElement relation;

	@FindBy(xpath = "(//td[@class='shown_td'])[1]/input")

	WebElement SI;

	@FindBy(xpath = "(//td[@class='shown_td show_td_cb'])[1]/input")

	WebElement CB;

	@FindBy(xpath = "//td[@class= 'shown_td datepicker_cont']/input")

	WebElement DOB;

	@FindBy(xpath = "//div[@class='ui-datepicker-title']/select[1]")

	WebElement month;

	@FindBy(xpath = "//div[@class='ui-datepicker-title']/select[2]")

	WebElement year;
	String str = "";
	@FindBy(xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr/td/a[text()='3']")

	WebElement day;

	@FindBy(xpath = "(//div[@class='row feed_form_group redio_feed_form_group'])[2]/div[1]")
	WebElement tenureoneyear;

	@FindBy(xpath = "//div[@class='col-sm-4 col-xs-4 radio_btn_cont'][1]")

	WebElement tenuretwoyear;

	@FindBy(xpath = "//div[@class='col-sm-4 col-xs-4 radio_btn_cont'][2]")

	WebElement tenurethreeyear;

	@FindBy(xpath = "//div[@class='col-sm-4 col-xs-4 radio_btn_cont first_radio_btn_cont']/label[1]")

	WebElement tenureoneyearclick;

	@FindBy(xpath = "//div[@class='col-sm-4 col-xs-4 radio_btn_cont'][1]/label[1]")

	WebElement tenuretwoyearclick;

	@FindBy(xpath = "//div[@class='slider2 ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content ui-slider-pips']/span[1]")

	WebElement slider1;

	@FindBy(xpath = "//div[@class='slider2 ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content ui-slider-pips']/span[4]")

	WebElement slider2;

	@FindBy(xpath = "//div[@class='slider2 ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content ui-slider-pips']/span[8]")

	WebElement careslider1;

	@FindBy(xpath = "//div[@class='slider2 ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content ui-slider-pips']/span[9]")

	WebElement careslider2;

	@FindBy(xpath = ".//*[@id='getQuoteInMinutesCareFreedomForm']/div/div[7]/div[3]/div/div/div[1]/label[1]")

	WebElement careFreedompremium;

	@FindBy(xpath = ".//*[@id='getQuoteInMinutesCareFreedomForm']/div/div[7]/div[3]/div/div/div[2]/label[1]")

	WebElement careFreedomwithhomecarePremium;

	@FindBy(xpath = ".//*[@id='getQuoteInMinutesCarePlanForm']/div/div[7]/div[3]/div/div/div[1]/label[1]")

	WebElement carepremium;

	@FindBy(xpath = ".//*[@id='getQuoteInMinutesCarePlanForm']/div/div[7]/div[3]/div/div/div[2]/label[1]")

	WebElement carewithunlimitedrecharge;

	@FindBy(name = "get_religare_quote")

	WebElement findReligarePremium;

	@FindBy(xpath = "//p[@class='get_quot_total_premium']/span[2]")

	WebElement totalPremium;

	@FindBy(xpath = "//div[@class='share_qution_btn']/button")

	WebElement portNow;

	@FindBy(xpath = "//div[@class='quote_container']/div[1]/h3")

	WebElement FAQ;

	@FindBy(xpath = "//div[@class='faqs_btn_holder']/span")

	WebElement FAQCrossbutton;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[1]/div[1]/h4/div")

	WebElement question1;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[2]/div[1]/h4/div")

	WebElement question2;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[3]/div[1]/h4/div")

	WebElement question3;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[4]/div[1]/h4/div")

	WebElement question4;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[5]/div[1]/h4/div")

	WebElement question5;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[6]/div[1]/h4/div")

	WebElement question6;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[7]/div[1]/h4/div")

	WebElement question7;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[8]/div[1]/h4/div")

	WebElement question8;

	@FindBy(xpath = "(//div[@class='panel panel-default'])[9]/div[1]/h4/div")

	WebElement question9;

	@FindBy(xpath = "//ul[@id='navigation']/li[1]/a")

	WebElement travelInsurance;

	@FindBy(xpath = "//ul[@id='navigation']/li[2]/a")

	WebElement healthInsurance;

	@FindBy(xpath = "//ul[@id='navigation']/li[3]/a")

	WebElement fixedBenifitInsurance;

	@FindBy(xpath = "(//div[@id='category']/p)[1]")

	WebElement theworldwithout;

	@FindBy(xpath = "(//div[@id='category'])[4]/p")

	WebElement withReligare;

	@FindBy(xpath = "(//div[@id='category'])[9]/p")

	WebElement lifes;

	@FindBy(xpath = ".//*[@id='lhc_need_help_close']")

	WebElement chatclose;

	public void Portabilitypage() throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		WebDriverCommonLib clib = new WebDriverCommonLib();
		portability.click();

		Set<String> str = BaseClass.driver.getWindowHandles();
		for (String f : str) {
			System.out.println(f);
			BaseClass.driver.switchTo().window(f);
			Thread.sleep(5000);

			if (BaseClass.driver.getCurrentUrl()
					.equals("https://rhicluat.religarehealthinsurance.com/health-insurance-portability.html")) {

				log.debug("Portability Home page");

				//clib.Passscreenshot("PortabilityHome", "PortabilityHomePage");

				JavascriptExecutor js = (JavascriptExecutor) BaseClass.driver;
				js.executeScript("arguments[0].scrollIntoView();", getstarted);
				if (getstarted.isDisplayed())
					System.out.println("Get Started button is present");
				getstarted.click();
				log.debug("Getstarted button is clicked");

			}

		}
		if (above45yearsage.isDisplayed() && below45yearsage.isDisplayed()) {
			System.out.println("Above45Yearsage & Below45Yearsage element are displayed ");

		} else {

			System.out.println("Element is not displayed");

		}
		Boolean Aboveandbelow45age = above45yearsage.isDisplayed() && below45yearsage.isDisplayed();
		log.debug("Age Element status is " + Aboveandbelow45age);
	}

	public void verifyportableHome() throws Throwable {

		// Portabilitycarefreedomclick();
		String Expectedurl = "https://rhicluat.religarehealthinsurance.com/cms/public/portability/home";
		String Actualurl = BaseClass.driver.getCurrentUrl();
		if (Expectedurl.equals(Actualurl)) {

			System.out.println("URL is matched");
		}

		else {
			System.out.println("URL is not matched");
		}
		log.debug("Actual URL " + Actualurl);
	}

	public void verifyFAQslinkonfooterofPortability() {

		if (FAQ.isDisplayed()) {

			System.out.println("FAQ is displayed");
		} else {

			System.out.println("FAQ is not displayed");

		}
		Boolean FAQStatus = FAQ.isDisplayed();
		log.debug("FAQ Element Status is " + FAQStatus);

	}

	public void verifyFaQClickable() throws InterruptedException, IOException, AWTException {
		Thread.sleep(5000);
		FAQ.click();
		Thread.sleep(3000);
		log.debug("FAQ is Clicked");
		Thread.sleep(7000);
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		je.executeScript("arguments[0].scrollIntoView(true);", FAQCrossbutton);
		if (FAQCrossbutton.isDisplayed()) {

			System.out.println("FAQ link is clickable");

		}

		else {
			System.out.print("FAQ link is not  clickable");

		}
		Boolean FAQlink = FAQCrossbutton.isDisplayed();
		log.debug("FAQ Link status " + FAQlink);
	}

	public void verifyquestionnairesonFAQs() {

		if (question1.isDisplayed() && question2.isDisplayed())

			System.out.println("Questions are displayed");

		else {

			System.out.println("Questions are not  displayed");

		}

		Boolean Questions = question1.isDisplayed() && question2.isDisplayed();
		log.debug("Questions Status is" + Questions);

	}

	public void verifyheaderelement1() throws AWTException, InterruptedException {

		Robot robot = new Robot(); // Robot class throws AWT Exception
		Thread.sleep(2000); // Thread.sleep throws InterruptedException

		for (int i = 0; i <= 8; i++) {
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
		}
		String firstvalue = "The world without a worry !";
		String secondvalue = "With Religare as your insurer, its truly Ab health Hamesha!";
		String thirdvalue = "Life's uncertainties are inevitable. And that's the case with health too. With Assure, by your side, you can rest all your financial worries with Religare";

		travelInsurance.click();
		log.debug("Header trvelInsurance is clicked");
		String firstvalue1 = theworldwithout.getText();
		System.out.println(firstvalue1);
		Thread.sleep(1000);
		healthInsurance.click();
		log.debug("Header healthInsurance is clicked");
		String secondvalue1 = withReligare.getText();
		System.out.println(secondvalue1);

		Thread.sleep(1000);
		fixedBenifitInsurance.click();
		log.debug("Header fixedBenifit is clicked");
		String thirdvalue1 = lifes.getText();
		System.out.println(thirdvalue1);

		if (firstvalue1.equals(firstvalue) && secondvalue1.equals(secondvalue) && thirdvalue1.equals(thirdvalue)) {

			System.out.println("Header Links are working fine");
		} else {

			System.out.println("Header Links are not  working fine");

		}
		log.debug("Header Links are working fine");
	}

	public void carefreedompolicy() throws InterruptedException {
		above45yearsage.click();
		Thread.sleep(5000);
		log.debug("Above45Years option is clicked");

	}

	public void carepolicy() throws InterruptedException {
		below45yearsage.click();
		Thread.sleep(7000);
		log.debug("Below 45Years option is clicked");

	}

	public void verifyheaders() throws InterruptedException {

		clib.waitForPageToLoad();

		if (features.isDisplayed() && policyOption.isDisplayed() && claimprocess.isDisplayed())

		{
			System.out.println("Features,Policyoption,Claimprocess,Exclusion headers are displayed");

		}

		else {

			System.out.println("Features,Policyoption,Claimprocess,Exclusion headers are not displayed");

		}
		Boolean headers = features.isDisplayed() && policyOption.isDisplayed() && claimprocess.isDisplayed();
		log.debug("Headers Status is " + headers);

	}

	public void totalMember() throws Throwable {
		// clib.waitForPageToLoad();
		Thread.sleep(3000);

		String totalMember = elib.getExcelData("Portability", 0, 1);
		// String totalMember="one";

		// js.executeScript("window.scrollBy(0,1000)");

		if (totalMember.equalsIgnoreCase("one")) {

			decrementmember.click();

			decrementmember.click();

			decrementmember.click();
		}
		if (totalMember.equalsIgnoreCase("two")) {
			// js.executeScript("arguments[0].scrollIntoView();", decrementmember);
			Thread.sleep(5000);

			decrementmember.click();

			decrementmember.click();

		}

		if (totalMember.equalsIgnoreCase("three")) {
			js.executeScript("arguments[0].scrollIntoView();", decrementmember);

			decrementmember.click();

		}

		if (totalMember.equalsIgnoreCase("four")) {
			System.out.println("Four Members");
			;
		}

		if (totalMember.equalsIgnoreCase("five")) {

			incrementmember.click();
		}

		if (totalMember.equalsIgnoreCase("six")) {

			incrementmember.click();

			incrementmember.click();

		}
		log.debug("Selecting the Total Members from the Drop Down " + totalMember);

	}

	public void totalChildPlus() {

		// totalChildPlus.click();
	}

	public void totalChildMinus() {

		// totalChildMinus.click();
	}

	public void defaultfeatures() {

		System.out.println(getquotesinstantly.isSelected());
		if (getquotesinstantly.isDisplayed()) {
			System.out.println("by default features tab is selected");

		} else {

			System.out.println("by default features tab is not  selected");

		}
		Boolean Defaultfeature = getquotesinstantly.isDisplayed();
		log.debug("default features tab is selected" + Defaultfeature);

	}


	public void verifyCovertypecarefreedom() throws Throwable {

		List<WebElement> li = BaseClass.driver.findElements(By.xpath("(//div[@class='input_year'])[1]/div/div/input"));

		Thread.sleep(2000);
		// li.get(1).click();

		String policycovertype = elib.getExcelData("Portability", 0, 3);
		System.out.println("abhipsa  " + policycovertype);
		if (policycovertype.equals("Floater"))

		{

			li.get(1).click();
			childincrementmember.click();
		}

		Thread.sleep(3000);

		if (individualcovertype.isDisplayed() && floatercovertype.isDisplayed())

		{
			System.out.println("Individualcoverype & flotorcovertype are displayed");

		} else {

			System.out.println("Individualcoverype & flotorcovertype are not  displayed");
		}

		Boolean covertype = individualcovertype.isDisplayed() && floatercovertype.isDisplayed();
		log.debug("Covertype Status is " + covertype);

	}

	public void verifyCovertypecare() throws Throwable {

		List<WebElement> li = BaseClass.driver.findElements(By.xpath("(//div[@class='input_year'])[1]/div/div/input"));

		Thread.sleep(2000);
		// li.get(1).click();

		String policycovertype = elib.getExcelData("Portability", 0, 3);
		// System.out.println("abhipsa " + policycovertype);
		if (policycovertype.equals("Individual"))

		{

			// li.get(1).click();
			childincrementmember.click();

		}

		if (policycovertype.equals("Floater"))

		{

			li.get(1).click();

		}

		Thread.sleep(3000);

		if (individualcovertype.isDisplayed() && floatercovertype.isDisplayed())

		{
			System.out.println("Individualcoverype & flotorcovertype are displayed");

		} else {

			System.out.println("Individualcoverype & flotorcovertype are not  displayed");
		}

		Boolean covertype = individualcovertype.isDisplayed() && floatercovertype.isDisplayed();
		log.debug("Covertype Status is " + covertype);

	}
	public void verifyMemberdetails() throws InterruptedException {

		Thread.sleep(3000);

		if (totalmembers.isDisplayed())

		{
			System.out.println("Totalmembers are displayed");
			log.debug("Totalmembers are displayed");

		} else

			System.out.println("Totalmembers  are not displayed");
		log.debug("Totalmembers are displayed");
		Thread.sleep(3000);

		if (totalchildren.isDisplayed())

		{
			System.out.println("Totalchildren are displayed");
			log.debug("Totalchildren are displayed");

		} else {

			System.out.println("Totalchildren are not  displayed");
			log.debug("Totalchildren are not displayed");

		}

	}

	public void verifyMobilenumber() throws Throwable {

		if (mobilenumber.isDisplayed())

		{
			System.out.println("Mobile Number field is present");
			String mobile = elib.getExcelData("Portability", 1, 1);
			mobilenumber.sendKeys(mobile);
			log.debug("Entering the Mobile number as " + mobile);
			BaseClass.driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);

		}
	}

	public void verifyRelation() throws InterruptedException {

		{
			Thread.sleep(3000);

		}
		if (relation.isDisplayed())

		{

			System.out.println("Member details is  visible");
			log.debug("Member details is  visible");


		}

		else {

			System.out.println("Member details is not visible");
			log.debug("Member details is not visible");


		}

		Boolean memberdetails = relation.isDisplayed();
		log.debug("Member Details status is" + memberdetails);

	}

	public void Member1() throws Throwable {
		Thread.sleep(2000);
		Select sel = new Select(relation);
		String Relation = elib.getExcelData("Portability", 2, 1);
		sel.selectByVisibleText(Relation);
		log.debug("Selecting the Relation as " + Relation);
		String Suminsured = elib.getExcelData("Portability", 9, 1);
		SI.click();
		SI.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		String cumulativebonus = elib.getExcelData("Portability", 15, 1);
		CB.click();
		CB.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);

		DOB.click();
		String Month = elib.getExcelData("Portability", 20, 1);
		String Year = elib.getExcelData("Portability", 20, 2);
		clib.select(year, Year);
		log.debug("Entering the Year as " + Year);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);
		// day.click();
		String day = elib.getExcelData("Portability", 20, 3);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		Thread.sleep(5000);

	}

	public void Member2() throws Throwable {

		Thread.sleep(2000);
		clib.waitForPageToLoad();
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		WebElement relation2 = BaseClass.driver.findElement(By.xpath("(//div[@class='select_cont'])[2]/select"));
		// Select sel = new Select(relation2);
		String Relation = elib.getExcelData("Portability", 3, 1);
		// sel.Select(Relation);
		// sel.selectByIndex(2);
		clib.select(relation2, Relation);

		log.debug("Selecting the Relation as " + Relation);
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		WebElement SI2 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[3]/input"));
		String Suminsured = elib.getExcelData("Portability", 10, 1);
		SI2.click();
		SI2.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		WebElement CB2 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[4]/input"));
		String cumulativebonus = elib.getExcelData("Portability", 14, 1);
		CB2.click();
		CB2.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);
		WebElement DOB2 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td datepicker_cont'])[2]/input"));
		Thread.sleep(3000);
		DOB2.click();
		String Month = elib.getExcelData("Portability", 21, 1);
		String Year = elib.getExcelData("Portability", 21, 2);
		clib.select(year, Year);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(month, Month);
		log.debug("Entering the Year as " + Year);
		
	}
		/*log.debug("Entering the Year as " + Year);
		Thread.sleep(2000);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);
>>>>>>> 53f4bbe240e919ad85df7236a315893e47dab68c
		// day.click();
		String day = elib.getExcelData("Portability", 21, 3);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		Thread.sleep(5000);

	}*/

	public void Member3() throws Throwable {
		clib.waitForPageToLoad();
		WebElement relation3 = BaseClass.driver.findElement(By.xpath("(//div[@class='select_cont'])[3]/select"));
		Select sel = new Select(relation3);
		String Relation = elib.getExcelData("Portability", 4, 1);
		sel.selectByVisibleText(Relation);
		log.debug("Selecting the Relation as " + Relation);
		WebElement SI3 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[5]/input"));
		String Suminsured = elib.getExcelData("Portability", 11, 1);
		SI3.click();
		SI3.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		WebElement CB3 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[6]/input"));
		String cumulativebonus = elib.getExcelData("Portability", 15, 1);
		CB3.click();
		CB3.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);
		WebElement DOB3 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td datepicker_cont'])[3]/input"));
		DOB3.click();
		String Month = elib.getExcelData("Portability", 22, 1);
		String Year = elib.getExcelData("Portability", 22, 2);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(year, Year);
		log.debug("Entering the Year as " + Year);
		// day.click();
		String day = elib.getExcelData("Portability", 22, 3);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);

		Thread.sleep(5000);

	}

	public void Member4() throws Throwable {

		clib.waitForPageToLoad();
		WebElement relation4 = BaseClass.driver.findElement(By.xpath("(//div[@class='select_cont'])[4]/select"));
		Select sel = new Select(relation4);
		String Relation = elib.getExcelData("Portability", 5, 1);
		sel.selectByVisibleText(Relation);
		log.debug("Selecting the Relation as " + Relation);
		WebElement SI4 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[7]/input"));
		String Suminsured = elib.getExcelData("Portability", 12, 1);
		SI4.click();
		SI4.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		WebElement CB4 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[8]/input"));
		String cumulativebonus = elib.getExcelData("Portability", 16, 1);
		CB4.click();
		CB4.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		WebElement DOB4 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td datepicker_cont'])[4]/input"));
		DOB4.click();
		String Month = elib.getExcelData("Portability", 23, 1);
		String Year = elib.getExcelData("Portability", 23, 2);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(year, Year);
		log.debug("Entering the Year as " + Year);
		// day.click();
		String day = elib.getExcelData("Portability", 23, 1);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		Thread.sleep(5000);

	}

	public void Member5() throws Throwable {

		clib.waitForPageToLoad();
		WebElement relation5 = BaseClass.driver.findElement(By.xpath("(//div[@class='select_cont'])[5]/select"));
		Select sel = new Select(relation5);
		String Relation = elib.getExcelData("Portability", 6, 1);
		sel.selectByVisibleText(Relation);
		log.debug("Selecting the Relation as " + Relation);
		String Suminsured = elib.getExcelData("Portability", 13, 1);
		WebElement SI5 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[9]/input"));
		SI5.click();
		SI5.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		String cumulativebonus = elib.getExcelData("Portability", 17, 1);
		WebElement CB5 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[10]/input"));
		CB5.click();
		CB5.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		WebElement DOB5 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td datepicker_cont'])[5]/input"));
		DOB5.click();
		String Month = elib.getExcelData("Portability", 24, 1);
		String Year = elib.getExcelData("Portability", 24, 2);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(year, Year);
		log.debug("Entering the Year as " + Year);
		// day.click();
		String day = elib.getExcelData("Portability", 24, 3);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		// BaseClass.driver.findElement(By.xpath("//div[text()='" +
		// Integer.parseInt(day) + "']")).click();
		Thread.sleep(5000);

	}

	public void Member6() throws Throwable {

		clib.waitForPageToLoad();
		WebElement relation6 = BaseClass.driver.findElement(By.xpath("(//div[@class='select_cont'])[6]/select"));
		Select sel = new Select(relation6);
		String Relation = elib.getExcelData("Portability", 7, 1);
		sel.selectByVisibleText(Relation);
		log.debug("Selecting the Relation as " + Relation);
		WebElement SI6 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[11]/input"));
		String Suminsured = elib.getExcelData("Portability", 14, 1);
		SI6.click();
		SI6.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		String cumulativebonus = elib.getExcelData("Portability", 18, 1);
		WebElement CB6 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[12]/input"));
		CB6.click();
		CB6.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		WebElement DOB6 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td datepicker_cont'])[6]/input"));
		DOB6.click();
		String Month = elib.getExcelData("Portability", 25, 1);
		String Year = elib.getExcelData("Portability", 25, 2);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(year, Year);
		log.debug("Entering the Year as " + Year);
		// day.click();
		String day = elib.getExcelData("Portability", 25, 1);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		Thread.sleep(2000);
		clib.screenshotalert("PortabilityCarefreedomFirstPage");
		Thread.sleep(5000);

	}

	public void Member1forcare() throws Throwable {
		Thread.sleep(2000);
		Select sel = new Select(relation);
		String Relation = elib.getExcelData("Portability", 86, 1);
		sel.selectByVisibleText(Relation);
		log.debug("Selecting the Relation as " + Relation);
		String Suminsured = elib.getExcelData("Portability", 92, 1);
		SI.click();
		SI.sendKeys(Suminsured);
		log.debug("Entering the SumInsured as" + Suminsured);
		String cumulativebonus = elib.getExcelData("Portability", 98, 1);
		CB.click();
		CB.sendKeys(cumulativebonus);
		log.debug("Entering the CumulativeBonus as" + cumulativebonus);

		DOB.click();
		String Month = elib.getExcelData("Portability", 43, 1);
		String Year = elib.getExcelData("Portability", 43, 2);
		clib.select(month, Month);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(year, Year);
		log.debug("Entering the Year as " + Year);
		// day.click();
		String day = elib.getExcelData("Portability", 43, 3);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		Thread.sleep(5000);

	}

	public void Member2forcare() throws Throwable {

		Thread.sleep(2000);
		clib.waitForPageToLoad();
		WebElement relation2 = BaseClass.driver.findElement(By.xpath("(//div[@class='select_cont'])[2]/select"));
		// Select sel = new Select(relation2);
		String Relation = elib.getExcelData("Portability", 87, 1);
		// sel.Select(Relation);
		// sel.selectByIndex(2);
		clib.select(relation2, Relation);

		log.debug("Selecting the Relation as " + Relation);
		try {
			chatclose.click();
		} catch (Exception e) {
		}
		/*
		 * WebElement SI2 =
		 * BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[3]/input"));
		 * String Suminsured = elib.getExcelData("Portability", 10, 1); SI2.click();
		 * SI2.sendKeys(Suminsured); log.debug("Entering the SumInsured as" +
		 * Suminsured); WebElement CB2 =
		 * BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td'])[4]/input"));
		 * String cumulativebonus = elib.getExcelData("Portability", 14, 1);
		 * CB2.click(); CB2.sendKeys(cumulativebonus);
		 * log.debug("Entering the CumulativeBonus as" + cumulativebonus); try {
		 * chatclose.click(); } catch (Exception e) { }
		 */
		WebElement DOB2 = BaseClass.driver.findElement(By.xpath("(//td[@class='shown_td datepicker_cont'])[2]/input"));
		DOB2.click();
		String Month = elib.getExcelData("Portability", 44, 1);
		String Year = elib.getExcelData("Portability", 44, 2);
		clib.select(year, Year);
		log.debug("Entering the Month as " + Month);
		Thread.sleep(3000);

		clib.select(month, Month);
		log.debug("Entering the Year as " + Year);
		// day.click();
		String day = elib.getExcelData("Portability", 44, 3);
		BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();
		log.debug("Entering the Date as " + day);
		Thread.sleep(5000);

	}

	/*
	 * public void verifyDOB( ) throws Throwable {
	 * 
	 * DOB.click(); String Month=elib.getExcelData("Portability", 20, 1); String
	 * Year=elib.getExcelData("Portability", 21, 1); clib.select(month,Month);
	 * Thread.sleep(3000);
	 * 
	 * clib.select(year, Year); day.click(); Thread.sleep(5000);
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	public void findreligarepremium() {
		((JavascriptExecutor) BaseClass.driver).executeScript("arguments[0].click();", findReligarePremium);
		log.debug("findReligarePremium is clicked");
	}

	public void verifySIforcarefreedom() throws Throwable {

		// Thread.sleep(10000);
		// System.out.println(findReligarePremium.isDisplayed());
		Thread.sleep(9000);
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		// je.executeScript("window.scrollBy(0,-250)", "");
		/*
		 * JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		 * je.executeScript("arguments[0].click;",findReligarePremium);
		 */
		// Thread.sleep(2000);

		// findReligarePremium.click();
		Thread.sleep(5000);
		if (slider1.isDisplayed()) {
			System.out.println("Slider is displayed");
			log.debug("Slider is displayed");

		}

		else {
			System.out.println("Slider is not displyed");
			log.debug("Slider is not displayed");

		}

		String policycovertype = elib.getExcelData("Portability", 0, 3);
		if (policycovertype.equals("Individual")) {

			slider2.click();
			log.debug("Slider is moved to 7lack");

		}

	}

	public void verifySIforcare() throws Throwable {

		// Thread.sleep(10000);
		// System.out.println(findReligarePremium.isDisplayed());
		Thread.sleep(9000);
		JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		// je.executeScript("window.scrollBy(0,-250)", "");
		/*
		 * JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		 * je.executeScript("arguments[0].click;",findReligarePremium);
		 */
		// Thread.sleep(2000);

		// findReligarePremium.click();

		if (careslider1.isDisplayed()) {
			System.out.println("Slider is displayed");

		}

		else {
			System.out.println("Slider is not displyed");

		}

		String policycovertype = elib.getExcelData("Portability", 0, 3);
		if (policycovertype.equals("Individual")) {

			careslider1.click();
			log.debug("Slider is moved to 10 lack");
		}
		String policycovertype1 = elib.getExcelData("Portability", 0, 3);
		if (policycovertype.equals("Floater")) {

			careslider2.click();
			log.debug("Slider is moved to 15lack");
		}
	}

	public void verifytenureaddonspremiumtotalpremiumforcarefreedom() throws Throwable {

		clib.waitForPageToLoad();
		if (tenureoneyear.isDisplayed() && tenuretwoyear.isDisplayed() && tenurethreeyear.isDisplayed()) {

			System.out.println("Tenure one year,two year & three year are displayed");
		} else {

			System.out.println("Tenure one year,two year & three year are not displayed");

		}
		Boolean tenures = tenureoneyear.isDisplayed() && tenuretwoyear.isDisplayed() && tenurethreeyear.isDisplayed();
		log.debug("Tenures Status is " + tenures);

		List<WebElement> li = BaseClass.driver
				.findElements(By.xpath("//div[@class='port_to_tab']/div[2]/div/div/div/input"));
		String policycovertype = elib.getExcelData("Portability", 0, 3);
		if (policycovertype.equals("Individual")) {
			li.get(1).click();
			log.debug("Tenuretwo is clicked");
		}

		Thread.sleep(9000);

		if (careFreedompremium.isDisplayed() && careFreedomwithhomecarePremium.isDisplayed()) {

			System.out.println("careFreedompremium & careFreedomwithhomecarePremium are displayed ");
		} else {

			System.out.println("careFreedompremium & careFreedomwithhomecarePremium are not  displayed ");

			Boolean premium = careFreedompremium.isDisplayed() && careFreedomwithhomecarePremium.isDisplayed();
			log.debug("Premium Status is " + premium);

		}
		Thread.sleep(2000);
		if (policycovertype.equals("Floater")) {

			List<WebElement> li1 = BaseClass.driver
					.findElements(By.xpath("//div[@class='port_to_tab']/div[3]/div/div/div/label"));
			String policycovertype1 = elib.getExcelData("Portability", 0, 3);
			li.get(2);
		}

		if (totalPremium.isDisplayed()) {

			System.out.println("Total Premium is displayed");
			log.debug("Total Premium is displayed");

		} else {

			System.out.println("Total Premium is not displayed");
			log.debug("Total Premium is not displayed");
		}

		// clib.screenshotalert("PortabilityCarefreedomPremiumPage");
		// portNow.click();
	}

	public void verifytenureaddonspremiumtotalpremiumforcare() throws Throwable {

		clib.waitForPageToLoad();

		if (tenureoneyear.isDisplayed() && tenuretwoyear.isDisplayed() && tenurethreeyear.isDisplayed()) {

			System.out.println("Tenure one year,two year & three year are displayed");
		} else {

			System.out.println("Tenure one year,two year & three year are not displayed");

		}
		Boolean tenures = tenureoneyear.isDisplayed() && tenuretwoyear.isDisplayed() && tenurethreeyear.isDisplayed();
		log.debug("Tenures Status is " + tenures);

		Thread.sleep(9000);
		List<WebElement> li = BaseClass.driver
				.findElements(By.xpath("//div[@class='port_to_tab']/div[2]/div/div/div/input"));
		String policycovertype = elib.getExcelData("Portability", 0, 3);
		if (policycovertype.equals("Individual")) {
			li.get(1).click();
			log.debug("Tenuretwo is clicked");

			List<WebElement> li1 = BaseClass.driver
					.findElements(By.xpath("//div[@class='port_to_tab']/div[2]/div/div/div/input"));
			String policycovertype1 = elib.getExcelData("Portability", 0, 3);
			if (policycovertype.equals("Floater")) {
				li.get(2).click();
				log.debug("Tenurethree is clicked");

				if (carepremium.isDisplayed() && carewithunlimitedrecharge.isDisplayed()) {

					System.out.println("carepremium & carewithunlimitedrecharge are displayed ");
				} else {

					System.out.println("carepremium & carewithunlimitedrecharge are not  displayed ");

					Boolean premium = carepremium.isDisplayed() && carewithunlimitedrecharge.isDisplayed();

					log.debug("Premium Status is " + premium);
					if (policycovertype.equals("Floater")) {

						List<WebElement> li2 = BaseClass.driver
								.findElements(By.xpath("//div[@class='port_to_tab']/div[3]/div/div/div/label"));
						String policycovertype2 = elib.getExcelData("Portability", 0, 3);
						li.get(2);
					}

				}

			}
		}

	}

	public void careportnow() throws InterruptedException, IOException, AWTException {
		/*
		 * //JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		 * je.executeScript("arguments[0].scrollIntoView(true);",chatclose);
		 * chatclose.click();
		 */

		Thread.sleep(2000);
		String totalpremium = totalPremium.getText();
		log.debug("Total Premium is displayed" + totalpremium);
		/*
		 * Thread.sleep(3000); clib.screenshotalert("PortabilityCarePremiumPage");
		 */

		/*
		 * je.executeScript("arguments[0].scrollIntoView(true);",portNow);
		 * 
		 * Thread.sleep(3000);
		 * 
		 */
		Thread.sleep(3000);
		// clib.screenshotalert("PortabilityCarePremiumPage");

	}

	public void premium() throws InterruptedException, IOException, AWTException {
		/*
		 * //JavascriptExecutor je = (JavascriptExecutor) BaseClass.driver;
		 * je.executeScript("arguments[0].scrollIntoView(true);",chatclose);
		 * chatclose.click();
		 */

		Thread.sleep(2000);
		String totalpremium = totalPremium.getText();
		log.debug("Total Premium is displayed" + totalpremium);
		Thread.sleep(3000);
		// clib.screenshotalert("PortabilityCarefreedomPremiumPage");
		/*
		 * Thread.sleep(3000); clib.screenshotalert("PortabilityCarePremiumPage");
		 */

		/*
		 * je.executeScript("arguments[0].scrollIntoView(true);",portNow);
		 * 
		 * Thread.sleep(3000);
		 */

	}

	public void portnow() throws InterruptedException {
		// js.executeScript("arguments[0].scrollIntoView(true);",portNow);
		portNow.click();
		log.debug("Click on PortNow");

	}

	public void verifyproceedtobuttonredirection() {

		String Expectedurl = "https://rhicluat.religarehealthinsurance.com/cms/public/portability/policyform?portabilityref=IjIwMTgwNjA2MTgzNTMwIg==&careplan=carefreedom";
		String Actualurl = BaseClass.driver.getCurrentUrl();
		if (Expectedurl.equals(Actualurl)) {

			System.out.println("URL is matched");
			log.debug("URL is matched");
		}

		else {
			System.out.println("URL is not matched");

			log.debug("URL is not matched");

		}
	}
}
