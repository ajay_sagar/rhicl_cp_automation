package com.religare.objectrepository;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class AssureFirstPage {
	public static RemoteWebDriver driver;

	JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();

	WebDriverCommonLib clib = new WebDriverCommonLib();

	@FindBy(id = "txtIconMinus_id")
	WebElement totalMemberMinus;

	@FindBy(id = "txtIconPlus_id")
	WebElement totalMemberPlus;

	@FindBy(id = "select_skin_demo_enhance_8")
	WebElement ageDropDown;

	@FindBy(name = "ageGroupOfEldestMember")
	// ageGroupOfEldestMember
	WebElement ageDropDown9;

	@FindBy(id = "mobile")
	WebElement mobileNumber;

	@FindBy(id = "carebuynowimage") // carebuynowimage
	WebElement careBuyNowButton;

	@FindBy(id = "tenure1")
	WebElement tenure1;

	@FindBy(id = "tenure2")
	WebElement tenure2;
	// tenure1

	@FindBy(id = "tenure3")
	WebElement tenure3;

	@FindBy(xpath = "//span[@class='txtmembertxt']")
	WebElement totalMember;

	@FindBy(name = "ageGroupOfEldestMember")
	WebElement ageOfMemberDropdown;

	@FindBy(id = "checkn")
	WebElement termsAndConditionsCheckBox;

	@FindBy(xpath = "//div[@class = 'ui-slider-range ui-widget-header ui-corner-all ui-slider-range-min']")
	WebElement assureSlider;

	@FindBy(xpath = "//span[@class='ui-slider-handle ui-state-default ui-corner-all']")
	WebElement sliderAssure;

	@FindBy(xpath = "//div[@class = 'limit_progressbar_container_points']/span[2]")
	WebElement slider2;

	@FindBy(id = "assurecare")
	WebElement premiumCalculations;

	@FindBy(id = "terms")
	WebElement termsAndCondLink;

	@FindBy(id = "sumInsured1")
	WebElement sumInsuredEditQuote;

	@FindBy(id = "tenure_id")
	WebElement tenureIdEditQuote;

	@FindBy(xpath = "//div[@class='premiumContent']")
	WebElement premiumEditQuotePage;

	@FindBy(xpath = "//a[@class='editQuote']")
	WebElement editQuoteBtn;

	private String srcname;

	private String destname;

	// @FindBy(xpath ="//div[contains(@class, 'ui-slider-range ui-widget-header
	// ui-corner-all ui-slider-range-min')]"))

	public void assurePolicyPlan() throws Throwable {

		ExcelLib elib = new ExcelLib();

		String age = elib.getExcelData("Assure", 0, 1);

		String mob = elib.getExcelData("Assure", 1, 1);// select_skin_demo_enhance_9

		WebDriverCommonLib clib = new WebDriverCommonLib();

		clib.select(ageDropDown9, age);

		log.debug("Selecting the age of eldest Member as " + age);

		Thread.sleep(2300);

		try {

			mobileNumber.sendKeys(mob);
			log.debug("Entering the Mobile Number " + mob);
			Alert al = BaseClass.driver.switchTo().alert();

			WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

			wait.until(ExpectedConditions.alertIsPresent());

			al.accept();

		} catch (Exception e) {

			e.getMessage();
		}

		((JavascriptExecutor) BaseClass.driver).executeScript("scroll(0,250);");

		BaseClass.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void slider() throws Throwable {

		Thread.sleep(1500);

		WebElement slider = BaseClass.driver.findElement(By.xpath(
				"//div[@class='slider-range newslider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']/div[1]/span[3]"));

		Dimension dim = slider.getSize();

		int x = dim.getWidth();

		Actions actions = new Actions(BaseClass.driver);

		actions.clickAndHold(slider).moveByOffset(x - 100, 0).release().build().perform();

		log.debug("Moving the slider ");
	}

	public void tenure() throws Throwable {

		String tenure = elib.getExcelData("Assure", 2, 1);

		if (tenure.equalsIgnoreCase("two")) {

			tenure2.click();

		} else if (tenure.equalsIgnoreCase("three")) {

			tenure3.click();

		}
		log.debug("Selecting the Tenure ");
	}

	public void capture() throws Throwable {

		Thread.sleep(5000);

		WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("assurecare")));

		String ncb = BaseClass.driver.findElement(By.id("assurecare")).getAttribute("innerHTML");

		// log.debug("Your Premium is " + ncb );

		WebDriverWait wait3 = new WebDriverWait(BaseClass.driver, 20);

		wait3.until(ExpectedConditions.visibilityOfElementLocated(By.id("carebuynowimage")));

		BaseClass.driver.findElement(By.id("carebuynowimage")).click();

		log.debug("clicking on the buy now button");

		BaseClass.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		Thread.sleep(1500);

	}

	public void next() {
		clib.javaScriptScroll();

		careBuyNowButton.click();
		log.debug("clicking on the buy now button");
	}

	public void verifyAssureQuoteFileds() throws Throwable {

		clib.waitForPageToLoad();
		Thread.sleep(5000);
		Boolean totalMemberIsAvailable = totalMember.isDisplayed();
		elib.setExcelDataBoolean("Assure", 4, 3, totalMemberIsAvailable);
		log.debug("In Assure Firstpage Total member option is present " + totalMemberIsAvailable);
		Boolean ageOfMemberDropDownIsAvailable = ageOfMemberDropdown.isDisplayed();
		elib.setExcelDataBoolean("Assure", 5, 3, ageOfMemberDropDownIsAvailable);
		log.debug("In Assure Firstpage AgeofEledestmember option is present " + ageOfMemberDropDownIsAvailable);
		Boolean mobileNumberIsAvailable = mobileNumber.isDisplayed();
		elib.setExcelDataBoolean("Assure", 6, 3, mobileNumberIsAvailable);
		log.debug("In Assure Firstpage MobileNumber Textbox is present " + totalMemberIsAvailable);
		Boolean assureSliderIsAvailable = assureSlider.isEnabled();
		elib.setExcelDataBoolean("Assure", 7, 3, assureSliderIsAvailable);
		log.debug("In Assure Firstpage Slider option is present " + assureSliderIsAvailable);
		System.out.println(assureSliderIsAvailable);
		Boolean tenure1IsAvailable = tenure1.isEnabled();
		elib.setExcelDataBoolean("Assure", 8, 3, tenure1IsAvailable);
		log.debug("In Assure Firstpage Tenure1 radio button is enabled " + tenure1IsAvailable);
		Boolean tenure2IsAvailable = tenure2.isEnabled();
		elib.setExcelDataBoolean("Assure", 9, 3, tenure2IsAvailable);
		log.debug("In Assure Firstpage Tenure2 radio button is enabled " + tenure2IsAvailable);
		Boolean tenure3IsAvailable = tenure3.isEnabled();
		elib.setExcelDataBoolean("Assure", 10, 3, tenure3IsAvailable);
		log.debug("In Assure Firstpage Tenure3 radio button is enabled " + tenure3IsAvailable);
		Thread.sleep(3000);
		Boolean termsAndConditionsCheckBoxIsAvailable = termsAndConditionsCheckBox.isDisplayed();
		elib.setExcelDataBoolean("Assure", 11, 3, termsAndConditionsCheckBoxIsAvailable);
		log.debug("In Assure Firstpage termsAndConditionsCheckBox is present " + termsAndConditionsCheckBoxIsAvailable);
		Thread.sleep(3000);
		Boolean careBuyNowButtonIsAvailable = careBuyNowButton.isDisplayed();
		elib.setExcelDataBoolean("Assure", 12, 3, careBuyNowButtonIsAvailable);
		log.debug("In Assure Firstpage careBuyNow Button is present " + careBuyNowButtonIsAvailable);

	}

	public void ageOfMemberIsAvailable() throws Throwable {
		ageDropDown9.click();
		String valueInDropDownForAgeOfMember = ageOfMemberDropdown.getText();
		elib.setExcelData3("Assure", 15, 3, valueInDropDownForAgeOfMember);
		log.debug("In Assure Firstpage Following AgeofEldestmember range is present " + valueInDropDownForAgeOfMember);
	}

	public void verifySelectedAgeBandIsReflected() throws Throwable {
		String age = elib.getExcelData("Assure", 0, 1);
		Select sel = new Select(ageDropDown9);
		// clib.select(ageDropDown9, age);
		sel.selectByVisibleText(age);

		WebElement getSelectedOptions = sel.getFirstSelectedOption();
		String selectedOptions = getSelectedOptions.getText();
		elib.setExcelData3("Assure", 18, 3, selectedOptions);
		log.debug("In Assure Firstpage Selecting AgeofEldestmember as  " + age);

	}

	public void verifySlider() throws Throwable {
		slider2.click();
		// clib.Passscreenshot("AssureverifySlider", "AssureverifySliderPage");
		String sliderTextWhichToBeClicked = slider2.getText();
		elib.setExcelData3("Assure", 22, 3, sliderTextWhichToBeClicked);
		log.debug("In Assure Firstpage moving the slider ");
	}

	public void tenureSelection() throws Throwable {
		String mob = elib.getExcelData("Assure", 1, 1);
		mobileNumber.sendKeys(mob);
		tenure2.click();
		Thread.sleep(2500);
		String tenure2PremumCal = premiumCalculations.getText();
		// System.out.println(tenure2PremumCal);
		elib.setExcelData3("Assure", 28, 3, tenure2PremumCal);
		log.debug("In Assure Firstpage for Tenure3 Premium calculation is " +tenure2PremumCal) ;
		Thread.sleep(2000);
		jse.executeScript("arguments[0].scrollIntoView(true);", tenure3);
		tenure3.click();
		Thread.sleep(1500);
		String tenure3PremumCal = premiumCalculations.getText();
		elib.setExcelData3("Assure", 29, 3, tenure3PremumCal);
		log.debug("In Assure Firstpage for Tenure3 Premium calculation is " +tenure3PremumCal) ;
		tenure1.click();
		Thread.sleep(1500);
		clib.waitForPageToLoad();
		String tenure1PremumCal = premiumCalculations.getText();
		elib.setExcelData3("Assure", 27, 3, tenure1PremumCal);
		log.debug("In Assure Firstpage for Tenure1 Premium calculation is  " + tenure1PremumCal);
		String tenure = elib.getExcelData("Assure", 2, 1);

		if (tenure.equalsIgnoreCase("two")) {
			tenure2.click();

		} else if (tenure.equalsIgnoreCase("three")) {

			tenure3.click();

		}
		log.debug("In Assure Firrstpage Selecting the Tenure ");
	}

	public void pleaseAcceptTermsAndCondError() throws Throwable {

		termsAndConditionsCheckBox.click();
		careBuyNowButton.click();
		log.debug("In Assureclicking on the buy now button");
		Thread.sleep(3000);
		clib.screenshotalert("AssureTermsandconditionerrormsg");
		Alert al = BaseClass.driver.switchTo().alert();
		String errorMessagePopUp = al.getText();
		elib.setExcelData3("Assure", 33, 3, errorMessagePopUp);
		log.debug("In Assure  Firstpage Capturing errormessage for terms & condition " + errorMessagePopUp);
		Thread.sleep(3000);
		// WebDriverWait wait = new WebDriverWait(BaseClass.driver, 10);
		// wait.until(ExpectedConditions.alertIsPresent());
		al.accept();
	}

	public void termsAndCondLink() throws Throwable {

		Boolean isEnabledCheck = termsAndCondLink.isEnabled();
		elib.setExcelDataBoolean("Assure", 35, 3, isEnabledCheck);
		log.debug("In Assure Firstpage Terms & Condition is enabled " +isEnabledCheck );

		termsAndCondLink.click();
		log.debug("In Assure Firstpage Terms & Condition is clicked ");

		Set<String> set = BaseClass.driver.getWindowHandles();
		// Iterating the windows
		Iterator<String> it = set.iterator();
		// Getting parentWindow ID
		String parentWin = it.next();
		// Getting childWindowIDh
		String childWin = it.next();
		BaseClass.driver.switchTo().window(childWin);

		String titleTermsAndCond = BaseClass.driver.getCurrentUrl();
		elib.setExcelData3("Assure", 37, 3, titleTermsAndCond);
		log.debug("In Asssure Firstpage Terms & Condition Title is captured as " + titleTermsAndCond);
		// System.out.println(titleTermsAndCond);
		BaseClass.driver.close();
		BaseClass.driver.switchTo().window(parentWin);

	}

	public void checkGetQuoteAndBuyNowButton() throws Throwable {

		termsAndConditionsCheckBox.click();
		clib.waitForPageToLoad();
		Boolean careNowButtonIsEnabled = careBuyNowButton.isEnabled();
		elib.setExcelDataBoolean("Assure", 41, 3, careNowButtonIsEnabled);
		log.debug("In Assure FirstPage BuyNow button is enabled " + careNowButtonIsEnabled);
		careBuyNowButton.click();
		log.debug("clicking on the buy now button");
		String titleProposalDetails = BaseClass.driver.getCurrentUrl();
		elib.setExcelData3("Assure", 42, 3, titleProposalDetails);
		log.debug("In ProposalDetails Title is captured as " + titleProposalDetails );
	}

	public void ageOfEldestMemberVerify() throws Throwable {
		Thread.sleep(3000);
		careBuyNowButton.click();
		Alert al = BaseClass.driver.switchTo().alert();
		Thread.sleep(3000);
		clib.screenshotalert("Assureegeofeldestmembererrormsg");
		String errorMessagePopUp = al.getText();
		log.debug("In Assure Firstpage Captureing errormessage for ageofeldest member " + errorMessagePopUp);
		elib.setExcelData3("Assure", 45, 3, errorMessagePopUp);
		al.accept();

	}

	public void verifyMobileNumber() throws Throwable {
		Thread.sleep(3000);
		String age = elib.getExcelData("Assure", 0, 1);
		Select sel = new Select(ageDropDown9);
		// clib.select(ageDropDown9, age);
		sel.selectByVisibleText(age);
		careBuyNowButton.click();
		Alert al = BaseClass.driver.switchTo().alert();
		Thread.sleep(3000);
		clib.screenshotalert("AssureMobilenoerrormessage");
		String errorMessagePopUp = al.getText();
		log.debug("In Assure FirstPage Captureing errormessage for mobilenumber " + errorMessagePopUp);
		elib.setExcelData3("Assure", 48, 3, errorMessagePopUp);
		// WebDriverWait wait = new WebDriverWait(BaseClass.driver, 10);
		// wait.until(ExpectedConditions.alertIsPresent());
		Thread.sleep(3000);
		al.accept();
	}

	public void verifyDetailsPresentInEditQuotePageProposalDetails() throws Throwable {

		Boolean ageOfEldestMemIsPresentEditQuote = ageDropDown9.isDisplayed();
		elib.setExcelDataBoolean("Assure", 52, 3, ageOfEldestMemIsPresentEditQuote);
		log.debug("In Proposer details edit quote Age of Eldest member is present " + ageOfEldestMemIsPresentEditQuote);
		Boolean sumInsuredIsPresentEditQuote = sumInsuredEditQuote.isDisplayed();
		elib.setExcelDataBoolean("Assure", 53, 3, sumInsuredIsPresentEditQuote);
		log.debug("In Proposer details edit quote SI is present " + sumInsuredIsPresentEditQuote);
		Boolean tenureIsPresentEditQuote = tenureIdEditQuote.isDisplayed();
		elib.setExcelDataBoolean("Assure", 54, 3, tenureIsPresentEditQuote);
		log.debug("In proposer details edit quote Tenure is present " + tenureIsPresentEditQuote);
		Boolean premiumIsPresentEditQuote = premiumEditQuotePage.isDisplayed();
		elib.setExcelDataBoolean("Assure", 55, 3, premiumIsPresentEditQuote);
		log.debug("In proposer details edit quote Premium is present " + premiumIsPresentEditQuote);
		Boolean editQuoteIsPresentEditQuote = editQuoteBtn.isDisplayed();
		elib.setExcelDataBoolean("Assure", 56, 3, editQuoteIsPresentEditQuote);
		log.debug("In proposer details edit quote EditQuote is present " + editQuoteIsPresentEditQuote);
		// Boolean toolTipIsPresentEditQuote = ageDropDown9.isDisplayed();
		// elib.setExcelDataBoolean("Assure", 57, 2, toolTipIsPresentEditQuote);

	}

	public void verifyDetailsPresentInEditQuotePageInsuredDetails() throws Throwable {

		Boolean ageOfEldestMemIsPresentEditQuote = ageDropDown9.isDisplayed();
		elib.setExcelDataBoolean("Assure", 130, 3, ageOfEldestMemIsPresentEditQuote);
		log.debug("In insured details edit quote Age of Eldest member present is present " + ageOfEldestMemIsPresentEditQuote);
		Boolean sumInsuredIsPresentEditQuote = sumInsuredEditQuote.isDisplayed();
		elib.setExcelDataBoolean("Assure", 131, 3, sumInsuredIsPresentEditQuote);
		log.debug("In insued details edit quote SI is present " + sumInsuredIsPresentEditQuote);
		Boolean tenureIsPresentEditQuote = tenureIdEditQuote.isDisplayed();
		elib.setExcelDataBoolean("Assure", 132, 3, tenureIsPresentEditQuote);
		log.debug("In insured details edit quote Tenure is present " + tenureIsPresentEditQuote);
		Boolean premiumIsPresentEditQuote = premiumEditQuotePage.isDisplayed();
		elib.setExcelDataBoolean("Assure", 133, 3, premiumIsPresentEditQuote);
		log.debug("In insured details edit quote Premium is present " + premiumIsPresentEditQuote);
		Boolean editQuoteIsPresentEditQuote = editQuoteBtn.isDisplayed();
		elib.setExcelDataBoolean("Assure", 134, 3, editQuoteIsPresentEditQuote);
		log.debug("In insured details edit quote EditQuote is present" + editQuoteIsPresentEditQuote);
		// Boolean toolTipIsPresentEditQuote = ageDropDown9.isDisplayed();
		// elib.setExcelDataBoolean("Assure", 57, 2, toolTipIsPresentEditQuote);

	}

	public void verifyDetailsPresentInEditQuotePageMedicalHist() throws Throwable {

		Boolean ageOfEldestMemIsPresentEditQuote = ageDropDown9.isDisplayed();
		elib.setExcelDataBoolean("Assure", 179, 3, ageOfEldestMemIsPresentEditQuote);
		log.debug("In Medicalhistory edit quote Age of Eldest member is present " + ageOfEldestMemIsPresentEditQuote);
		Boolean sumInsuredIsPresentEditQuote = sumInsuredEditQuote.isDisplayed();
		elib.setExcelDataBoolean("Assure", 180, 3, sumInsuredIsPresentEditQuote);
		log.debug("In Medicalhistory edit quote SI is present " + sumInsuredIsPresentEditQuote);
		Boolean tenureIsPresentEditQuote = tenureIdEditQuote.isDisplayed();
		elib.setExcelDataBoolean("Assure", 181, 3, tenureIsPresentEditQuote);
		log.debug("In Medicalhistory edit quote Tenure is present " + tenureIsPresentEditQuote);
		Boolean premiumIsPresentEditQuote = premiumEditQuotePage.isDisplayed();
		elib.setExcelDataBoolean("Assure", 182, 3, premiumIsPresentEditQuote);
		log.debug("In Medicalhistory edit quote Premium is present " + premiumIsPresentEditQuote);
		Boolean editQuoteIsPresentEditQuote = editQuoteBtn.isDisplayed();
		elib.setExcelDataBoolean("Assure", 183, 3, editQuoteIsPresentEditQuote);
		log.debug("In Medicalhistory edit quote EditQuote is present " + editQuoteIsPresentEditQuote);
		// Boolean toolTipIsPresentEditQuote = ageDropDown9.isDisplayed();
		// elib.setExcelDataBoolean("Assure", 57, 2, toolTipIsPresentEditQuote);

	}

	public void getCurrentURLAssure() throws Throwable {

		String currentURl = BaseClass.driver.getCurrentUrl();
		if (currentURl.contains(""))
			elib.setExcelData3("Assure", 2, 3, currentURl);
		log.debug("In Assure Firstpage Captching Current URL " + currentURl );

	}

}
