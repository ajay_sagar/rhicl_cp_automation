package com.religare.objectrepository;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.WebDriverCommonLib;

public class ProposerDetailPage {
	Logger log = Logger.getLogger("devpinoyLogger");
	WebDriverCommonLib clib = new WebDriverCommonLib();

	ExcelLib elib = new ExcelLib();
	
	WebDriverWait wait = new WebDriverWait(BaseClass.driver, 10);

	JavascriptExecutor jse = (JavascriptExecutor) BaseClass.driver;

	@FindBy(id = "ValidPanCard")

	WebElement panCard;

	// @FindBy(xpath ="//div[@id='proposer_details_divid']/div[1]/div[1]/label[1]")

	@FindBy(id = "maleIcon")

	// @FindBy(xpath = "//span[text()='Male']")

	WebElement maleIcon;

	// @FindBy(xpath="//label[@class = 'femaleIcon' and @for =
	// 'ValidTitle-2']")//label[@class='maleIcon']

	// @FindBy(xpath =".//*[@id='proposer_details_divid']/div[1]/div[1]/label[2]")

	// @FindBy (xpath =
	// "//div[@id='proposer_details_divid']/div[1]/div[1]/label[2]")

	@FindBy(id = "femaleIcon")

	WebElement femaleIcon;

	@FindBy(id = "ValidFName")

	WebElement fillFirstNameTextBox;

	@FindBy(id = "ValidLName")

	WebElement fillLastNameTextBox;

	@FindBy(id = "datepicker")

	WebElement datePicker;

	@FindBy(xpath = "//select[@class ='ui-datepicker-month']")

	WebElement monthPickerDropDown;

	@FindBy(xpath = "//select[@class ='ui-datepicker-year']")

	WebElement yearPickerDropDown;

	// @FindBy(xpath = "//a[@class='ui-state-default' and text()='20']")
	//
	// WebElement pickDate;

	@FindBy(id = "ValidEmail")

	WebElement emailTextBox;

	@FindBy(id = "NomineeName")

	WebElement nomineeNameTextBox;

	@FindBy(id = "nomineeRelation")

	WebElement nomineeRelationDropDown;

	@FindBy(id = "ValidAddressOne")

	WebElement validAddressOneTextBox;

	@FindBy(id = "ValidAddressTwo")

	WebElement validAdressTwoTextBox;

	@FindBy(id = "landmark")

	WebElement landmarkTextBox;

	@FindBy(id = "ValidPinCode")

	WebElement validpinCodeTextBox;

	@FindBy(id = "ValidCityName")

	WebElement validCityNameDropDown;

	@FindBy(id = "submit_proposer_details")

	WebElement submitBtn;

	@FindBy(id = "studentproposal")

	WebElement studExpnext;

	public void fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation() throws Throwable {

		String genderIcon = elib.getExcelData("fillExploreDetails", 0, 1);

		String firstName = elib.getExcelData("fillExploreDetails", 1, 1);

		String lastName = elib.getExcelData("fillExploreDetails", 2, 1);

		String day = elib.getExcelData("fillExploreDetails", 3, 1);
		String month = elib.getExcelData("fillExploreDetails", 3, 2);
		String year = elib.getExcelData("fillExploreDetails", 3, 3);

		String emailId = elib.getExcelData("fillExploreDetails", 4, 1);

		String nomineeName = elib.getExcelData("fillExploreDetails", 5, 1);

		String nomineeRelation = elib.getExcelData("fillExploreDetails", 6, 1);

		String date = elib.getExcelData("fillExploreDetails", 13, 1);

		// clib.waitForElementPresent(maleIcon);
		// clib.waitForElementPresent(femaleIcon);
		//
		// clib.waitForPageToLoad();

		if (genderIcon.equals("male")) {

			jse.executeScript("arguments[0].click();", maleIcon);
		}

		else if (genderIcon.equalsIgnoreCase("female")) {

			femaleIcon.click();
		}
		 log.debug("Selecting the gender as " +genderIcon);
		fillFirstNameTextBox.sendKeys(firstName);
		 log.debug("Entering the First name as " +firstName);
		fillLastNameTextBox.sendKeys(lastName);
		 log.debug("Entering the lastname  as" +lastName);
		 BaseClass.driver.findElement(By.id("datepicker")).click();
			WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
			Select d3 = new Select(dp3);
			d3.selectByVisibleText(month);
			
			WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
			Select d4 = new Select(dp4);
			d4.selectByVisibleText(year);
			
			BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();	
			log.debug("Entering the date of birth ");

		emailTextBox.sendKeys(emailId);
		 log.debug("Entering the emailId as" +emailId);
		nomineeNameTextBox.sendKeys(nomineeName);
		 log.debug("Entering the nomineeName as" +nomineeName);
		Thread.sleep(2000);

		try {

			BaseClass.driver.findElement(By.id("from")).sendKeys(date);
			;
		} catch (Exception e) {
			e.getMessage();
		}
		 log.debug("Selecting the date as" +date);
		if (nomineeRelation.equalsIgnoreCase("wife")) {

			jse.executeScript("arguments[0].value='WIFE'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("mother")) {

			jse.executeScript("arguments[0].value='MOTH'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("daughter")) {

			jse.executeScript("arguments[0].value='UDTR'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("son")) {

			jse.executeScript("arguments[0].value='SONM'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("father")) {

			jse.executeScript("arguments[0].value='FATH'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("bother")) {

			jse.executeScript("arguments[0].value='BOTH'", nomineeRelationDropDown);
		}

		else if (nomineeRelation.equalsIgnoreCase("sister")) {

			jse.executeScript("arguments[0].value='SIST'", nomineeRelationDropDown);
		}
		log.debug("Entering the nomineeRelation as" +nomineeRelation);

	}
	 
	public void fillDetailsAddress() throws Throwable {

		String addressOne = elib.getExcelData("fillExploreDetails", 7, 1);

		String addressTwo = elib.getExcelData("fillExploreDetails", 8, 1);

		String landmark = elib.getExcelData("fillExploreDetails", 9, 1);

		String pincode = elib.getExcelData("fillExploreDetails", 10, 1);

		String city = elib.getExcelData("fillExploreDetails", 11, 1);

		validAddressOneTextBox.sendKeys(addressOne);
		 log.debug("Entering the addressOne as" +addressOne);
		validAdressTwoTextBox.sendKeys(addressTwo);
		 log.debug("Entering the addressTwo as" +addressTwo);
		landmarkTextBox.sendKeys(landmark);
		 log.debug("Entering the landmark as" +landmark);
		validpinCodeTextBox.sendKeys(pincode);
		 log.debug("Entering the pincode as" +pincode);
		Thread.sleep(2500);

		if (city.equalsIgnoreCase("Bangalore North")) {

			jse.executeScript("arguments[0].value='Bangalore North'", validCityNameDropDown);
		}

		else if (city.equalsIgnoreCase("Bangalore")) {

			jse.executeScript("arguments[0].value='Bangalore'", validCityNameDropDown);
		}

		else if (city.equalsIgnoreCase("BANGALORE NORTH")) {

			jse.executeScript("arguments[0].value='BANGALORE NORTH'", validCityNameDropDown);
		}
		 log.debug("Entering the city as" +city);
	}
	
	public void nextStudExp() {
		wait.until(ExpectedConditions.visibilityOf(submitBtn));
	//	clib.waitForElementPresent(submitBtn);
		clib.waitForPageToLoad();
		studExpnext.click();
		 log.debug("Clicking on the Submit button" );
	}

	public void nextClick() {
		wait.until(ExpectedConditions.visibilityOf(submitBtn));
		//clib.waitForElementPresent(submitBtn);
		clib.waitForPageToLoad();
		submitBtn.click();
		 log.debug("Clicking on the Submit button" );
	}

	public void insuredMember1() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 0, 1);

		
		try {

			BaseClass.driver.findElement(By.id("passport-1")).sendKeys(passport);
		} catch (Exception e) {
			e.getMessage();
		}
		 log.debug("Entering the passport as" +passport);
	}

	public void insuredMember2() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 1, 1);

		String firstName = elib.getExcelData("InsuredDetails", 22, 1);

		String lastName = elib.getExcelData("InsuredDetails", 29, 1);

		String day = elib.getExcelData("InsuredDetails", 36, 1);
		String month = elib.getExcelData("InsuredDetails", 36, 2);
		String year = elib.getExcelData("InsuredDetails", 36, 3);

		String relation = elib.getExcelData("InsuredDetails", 15, 1);

		WebElement relation2 = BaseClass.driver.findElement(By.id("relationCd-2"));

		Select sel = new Select(relation2);

		sel.selectByValue(relation);
		 log.debug("selecting the relation as" +relation);
		BaseClass.driver.findElement(By.id("firstNamecd-2")).sendKeys(firstName);
		 log.debug("Entering the firstName as" +firstName);
		clib.waitForPageToLoad();

		BaseClass.driver.findElement(By.id("lastNamecd-2")).sendKeys(lastName);
		 log.debug("Entering the lastName as" +lastName);
		 BaseClass.driver.findElement(By.id("datepicker")).click();
			WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
			Select d3 = new Select(dp3);
			d3.selectByVisibleText(month);
			
			WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
			Select d4 = new Select(dp4);
			d4.selectByVisibleText(year);
			
			BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();	
			log.debug("Entering the date of birth ");
		try {

			BaseClass.driver.findElement(By.id("passport-2")).sendKeys(passport);
			 log.debug("Entering the passport as" +passport);
		} catch (Exception e) {
			e.getMessage();
		}

	}

	public void insuredMember3() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 2, 1);

		String firstName = elib.getExcelData("InsuredDetails", 23, 1);

		String lastName = elib.getExcelData("InsuredDetails", 30, 1);

		String day = elib.getExcelData("InsuredDetails", 37, 1);
		String month = elib.getExcelData("InsuredDetails", 37, 2);
		String year = elib.getExcelData("InsuredDetails", 37, 3);

		String relation = elib.getExcelData("InsuredDetails", 16, 1);

		WebElement relation3 = BaseClass.driver.findElement(By.id("relationCd-3"));

		Select sel = new Select(relation3);

		sel.selectByValue(relation);
		 log.debug("Entering the relation as" +relation);
		BaseClass.driver.findElement(By.id("firstNamecd-3")).sendKeys(firstName);
		 log.debug("Entering the firstName as" +firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-3")).sendKeys(lastName);
		 log.debug("Entering the lastName as" +lastName);
		 BaseClass.driver.findElement(By.id("datepicker")).click();
			WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
			Select d3 = new Select(dp3);
			d3.selectByVisibleText(month);
			
			WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
			Select d4 = new Select(dp4);
			d4.selectByVisibleText(year);
			
			BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();	
			log.debug("Entering the date of birth ");
		try {

			BaseClass.driver.findElement(By.id("passport-3")).sendKeys(passport);
			log.debug("Entering the passport as" +passport);
		} catch (Exception e) {
			e.getMessage();
		}
		 
	}

	public void insuredMember4() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 3, 1);

		String firstName = elib.getExcelData("InsuredDetails", 24, 1);

		String lastName = elib.getExcelData("InsuredDetails", 31, 1);

		String day = elib.getExcelData("InsuredDetails", 38, 1);
		String month = elib.getExcelData("InsuredDetails", 38, 2);
		String year = elib.getExcelData("InsuredDetails", 38, 3);

		String relation = elib.getExcelData("InsuredDetails", 17, 1);

		WebElement relation4 = BaseClass.driver.findElement(By.id("relationCd-4"));

		Select sel = new Select(relation4);

		sel.selectByValue(relation);
		 log.debug("Entering the relation as" +relation);
		BaseClass.driver.findElement(By.id("firstNamecd-4")).sendKeys(firstName);
		 log.debug("Entering the firstName as" +firstName);

		BaseClass.driver.findElement(By.id("lastNamecd-4")).sendKeys(lastName);
		 log.debug("Entering the lastName as" +lastName);
		 BaseClass.driver.findElement(By.id("datepicker")).click();
			WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
			Select d3 = new Select(dp3);
			d3.selectByVisibleText(month);
			
			WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
			Select d4 = new Select(dp4);
			d4.selectByVisibleText(year);
			
			BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();	
			log.debug("Entering the date of birth ");
		try {
			BaseClass.driver.findElement(By.id("passport-4")).sendKeys(passport);
			 log.debug("Entering the passport as" +passport);
		} catch (Exception e) {
			e.getMessage();
		}
		
		// BaseClass.driver.findElement(By.id("submit_insured_details")).click();

	}

	public void insuredMember5() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 4, 1);

		String firstName = elib.getExcelData("InsuredDetails", 25, 1);

		String lastName = elib.getExcelData("InsuredDetails", 32, 1);

		String day = elib.getExcelData("InsuredDetails", 39, 1);
		String month = elib.getExcelData("InsuredDetails", 39, 2);
		String year = elib.getExcelData("InsuredDetails", 39, 3);

		String relation = elib.getExcelData("InsuredDetails", 18, 1);

		WebElement relation5 = BaseClass.driver.findElement(By.id("relationCd-5"));

		Select sel = new Select(relation5);

		sel.selectByValue(relation);
		 log.debug("Selecting the relation as" +relation);
		BaseClass.driver.findElement(By.id("firstNamecd-5")).sendKeys(firstName);
		 log.debug("Entering the firstName as" +firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-5")).sendKeys(lastName);
		 log.debug("Entering the lastName as" +lastName);
		 BaseClass.driver.findElement(By.id("datepicker")).click();
			WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
			Select d3 = new Select(dp3);
			d3.selectByVisibleText(month);
			
			WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
			Select d4 = new Select(dp4);
			d4.selectByVisibleText(year);
			
			BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();	
			log.debug("Entering the date of birth "); 

		try {
			BaseClass.driver.findElement(By.id("passport-5")).sendKeys(passport);
			log.debug("Entering the passport as" +passport);
		} catch (Exception e) {
			e.getMessage();
		}
		 
		// BaseClass.driver.findElement(By.id("submit_insured_details")).click();

	}

	public void insuredMember6() throws Throwable {

		String passport = elib.getExcelData("InsuredDetails", 5, 1);

		String firstName = elib.getExcelData("InsuredDetails", 26, 1);

		String lastName = elib.getExcelData("InsuredDetails", 33, 1);

		//String dob = elib.getExcelData("InsuredDetails", 40, 1);

		String day = elib.getExcelData("InsuredDetails", 40, 1);
		String month = elib.getExcelData("InsuredDetails", 40, 2);
		String year = elib.getExcelData("InsuredDetails", 40, 3);
		String relation = elib.getExcelData("InsuredDetails", 19, 1);

		WebElement relation6 = BaseClass.driver.findElement(By.id("relationCd-6"));

		Select sel = new Select(relation6);

		sel.selectByValue(relation);

		BaseClass.driver.findElement(By.id("firstNamecd-6")).sendKeys(firstName);
		 log.debug("Entering the firstName as" +firstName);
		BaseClass.driver.findElement(By.id("lastNamecd-6")).sendKeys(lastName);
		 log.debug("Entering the lastName as" +lastName);
		
		 BaseClass.driver.findElement(By.id("datepicker")).click();
			WebElement dp3 = BaseClass.driver.findElement(By.className("ui-datepicker-month"));
			Select d3 = new Select(dp3);
			d3.selectByVisibleText(month);
			
			WebElement dp4 = BaseClass.driver.findElement(By.className("ui-datepicker-year"));
			Select d4 = new Select(dp4);
			d4.selectByVisibleText(year);
			
			BaseClass.driver.findElement(By.xpath("//a[text()='" + Integer.parseInt(day) + "']")).click();	
			log.debug("Entering the date of birth "); 
			
		try {
			BaseClass.driver.findElement(By.id("passport-6")).sendKeys(passport);
			 log.debug("Entering the passport as" +passport);
		} catch (Exception e) {
			e.getMessage();
		}
		
		// BaseClass.driver.findElement(By.id("submit_insured_details")).click();}
		// catch(Exception e) {e.getMessage();}

	}

	public void next() {
		BaseClass.driver.findElement(By.id("submit_insured_details")).click();
		 log.debug("Clicking on the submit button");
	}

	public void panCard() throws Throwable {

		String pancard = elib.getExcelData("fillExploreDetails", 12, 1);
		panCard.sendKeys(pancard);
		 log.debug("Entering the pancard as" +pancard);
	}

}
