package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.FillDetailsExplore;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;
import com.religare.objectrepository.SecureClick;
import com.religare.objectrepository.SecurePloicyplan;

@Listeners(com.religare.genericlib.SampleListener.class)

public class SecureTest extends BaseClass {

	Logger log = Logger.getLogger("devpinoyLogger");
	WebDriverCommonLib clib = new WebDriverCommonLib();

	@Test(priority = 1)

	public void moveToSecureTest() throws Throwable {

		// clib.Passscreenshot("Religarehome", "ReligareHomePage");
		SecureClick click = PageFactory.initElements(BaseClass.driver, SecureClick.class);

		click.clickOnSecure();
		log.debug("***************Automating the Secure Module*****************");
		log.debug("Clicking on the Secure Module");
	}

	@Test(priority = 2)

	public void secureFirstpageTest() throws Throwable {
		SecurePloicyplan plan = PageFactory.initElements(BaseClass.driver, SecurePloicyplan.class);

		clib.waitForPageToLoad();

		Thread.sleep(5000);

		plan.age();

		plan.income();

		Thread.sleep(2500);

		plan.mobile();

		// plan.income();

		plan.typeofemployement();

		plan.sliders();

		plan.tensure();

		// plan.totalpremium();

		Thread.sleep(2000);
		// clib.Passscreenshot("SecureFirst", "SecureFirstPage");

	}

	@Test(priority = 3)

	public void secureProposerdetailsTest() throws Throwable {
		SecurePloicyplan plan = PageFactory.initElements(BaseClass.driver, SecurePloicyplan.class);

		plan.buy();
		// }
		//
		// @Test
		//
		// public void fillProposalAndInsuredDetailsExploreTest() throws Throwable {

		clib.waitForPageToLoad();

		com.religare.genericlib.FillDetailsExplore fdx = PageFactory.initElements(BaseClass.driver,
				com.religare.genericlib.FillDetailsExplore.class);

		fdx.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		fdx.fillDetailsAddress();

		Thread.sleep(3500);

		try {

			fdx.panCard();

		} catch (NoSuchElementException e) {

			e.getMessage();

		}

		// clib.Passscreenshot("SecureProposer's", "SecureProposer'sPage");
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}

	@Test(priority = 4)

	public void secureInsureddetailsTest() throws Throwable {
		BaseClass.driver.findElement(By.id("proposal")).click();
		log.debug("Clicking on the Submit button");
		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.insuredMember1();

		try {

			idp.insuredMember2();

			idp.insuredMember3();

			idp.insuredMember4();

			idp.insuredMember5();

			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}
	}
	@Test(priority = 5)

	public void secureMedicalhistoryTest() throws Throwable {
		FillDetailsExplore fd = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		clib.waitForPageToLoad();
		// clib.Passscreenshot("SecureInsured", "SecureInsuredPage");

		fd.secureinsuredpage();

		// fd.Nextjoytomorrowinsuredscreen();

		clib.waitForPageToLoad();

		//
		// @Test
		//
		// public void medHist() throws Throwable {
		//
		// clib.waitForPageToLoad();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		Thread.sleep(4000);

		mp.secureMedicalHistory();
		
	}
	@Test(priority = 6)

	public void secureProposalsummaryTest() throws Throwable {
		
		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);
		mp.secureProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));
			
	}
	
	@Test(priority = 6)

	public void securePaymentTest() throws Throwable {
		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);

		payment.Applicationno();

		payment.Totalsecurepremium();

		payment.proceedsecure();

		clib.waitForPageToLoad();

		Thread.sleep(3000);

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);

		// payu.gateway("SecurePayment", "SecurePaymentPage", "SecureThankyou",
		// "SecureThankyouPage");

	}
	
	@Test(priority = 7)
	public void SecureThankyouTest() throws Throwable {
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.thankyou();

	}}
