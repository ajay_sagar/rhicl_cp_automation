package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.AssureClick;
import com.religare.objectrepository.AssureFirstPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;

@Listeners(com.religare.genericlib.SampleListener.class)

public class TestAssure extends BaseClass {

	WebDriverCommonLib clib = new WebDriverCommonLib();
	ExcelLib elib = new ExcelLib();
	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)

	public void verifyAssureClick() throws Throwable {

		// clib.Passscreenshot("Religarehome", "ReligareHomePage");
		log.debug("***************Automating the Assure Module*****************")

		for (int i = 0; i <= 204; i++) {
			elib.setExcelData3(i, "Assure", 3);
		}

		clib.getCurrentURL(1, 2);
		clib.waitForPageToLoad();

		AssureClick click = PageFactory.initElements(BaseClass.driver, AssureClick.class);
		click.clickOnAssure();
		log.debug("Clicking on the Assure Module");

	}

	@Test(priority = 2)

	public void fillquoteboxAssure() throws Throwable {

		AssureFirstPage afp = PageFactory.initElements(BaseClass.driver, AssureFirstPage.class);
		afp.getCurrentURLAssure();
		afp.verifyAssureQuoteFileds();
		afp.ageOfEldestMemberVerify();
		afp.verifyMobileNumber();
		afp.ageOfMemberIsAvailable();
		afp.verifySelectedAgeBandIsReflected();
		afp.verifySlider();
		afp.tenureSelection();
		clib.waitForPageToLoad();
		Thread.sleep(3500);
		afp.pleaseAcceptTermsAndCondError();
		afp.termsAndCondLink();

	}

	@Test(priority = 3)
	public void fillproposerdetailsAssure() throws Throwable {

		AssureFirstPage afp = PageFactory.initElements(BaseClass.driver, AssureFirstPage.class);

		afp.checkGetQuoteAndBuyNowButton();

		clib.waitForPageToLoad();

		afp.verifyDetailsPresentInEditQuotePageProposalDetails();

		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		pdp.verifyErrorMessage();
		pdp.isInputPresentInProposalDetailsPage();
		pdp.verifyRadioBtn();
		pdp.verifyFirstName();
		pdp.verifyLastName();
		pdp.verifyMobileNumber();
		pdp.clearMobileNumberDataAndReset();
		pdp.verifyDOB();
		pdp.verifyDOBSelect();
		pdp.verifyEmailAddress();
		pdp.verifyNomineeTextbox();
		pdp.valueOfDropDownAndIsClickable();
		pdp.verifyRelationDropDown();
		pdp.verifFillAddressOne();
		pdp.verifFillAddressTwo();
		pdp.verifyLandmark();
		pdp.verifyPinCode();
		clib.waitForPageToLoad();
		Thread.sleep(3000);
		pdp.verifyStateAutoPinCode();
		clib.waitForPageToLoad();
		Thread.sleep(5000);
		pdp.citySelect();

	}

	@Test(priority = 4)
	public void fillInsuredetailsAssure() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		pdp.submitBTN();

		clib.waitForPageToLoad();
		clib.waitForPageToLoad();
		pdp.verifyDetailsFORInsuredDetails();
		AssureFirstPage afp = PageFactory.initElements(BaseClass.driver, AssureFirstPage.class);
		afp.verifyDetailsPresentInEditQuotePageInsuredDetails();
		clib.waitForPageToLoad();
		clib.waitForPageToLoad();
		Thread.sleep(3200);
		pdp.verifySelfRelationDropDown();
		pdp.getSelfName();

		pdp.verifyNegativErrorMessageInsured();

		clib.waitForPageToLoad();
		Thread.sleep(1100);
		clib.waitForPageToLoad();
		pdp.selectRelationInsured();
		pdp.verifyFirstNameInsured();
		pdp.verifyDOBInsuredDetails();
		pdp.verifyHeightAndFeetInsuredDetails();

	}

	@Test(priority = 5)
	public void fillMedicalhistoryAssure() throws Throwable {

		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		pdp.getURLOfMedHistPageAssure();

		clib.waitForPageToLoad();
		AssureFirstPage afp = PageFactory.initElements(BaseClass.driver, AssureFirstPage.class);
		afp.verifyDetailsPresentInEditQuotePageMedicalHist();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);
		clib.waitForPageToLoad();
		mp.checkDisclaimerCheckBoxAvailableInMedicalHistoryAssure();
		mp.getMedicalText();
		mp.getHealthQuestionText();
		mp.ge12QuestionsTextAssure();
		mp.assureMedicalhistory();	
		
	}

	@Test(priority = 6)
	public void ProposalsummaryAssure() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		mp.assuremedicalhistorysubmit();
		mp.getCurrentURLProposalSummaryAssure();

		pdp.getProposalSummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));
	}

	@Test(priority = 7)
	
	public void assurePaymentTest() throws Throwable {

		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);

		payment.proceedassure();

		clib.waitForPageToLoad();

		Thread.sleep(3000);

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);

		payu.gateway();

	}

	@Test(priority = 8)
	public void assureThankyouTest() {
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();

}
