package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;

import com.religare.objectrepository.PayU;

import com.religare.objectrepository.RenewalMethods;

@Listeners(com.religare.genericlib.SampleListener.class)

public class RenewalUpgrade extends BaseClass {
	WebDriverCommonLib clib = new WebDriverCommonLib();

	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)
	public void upgrade() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		log.debug("***************Automating the script for 'Upgrade' Module*********************");
		re.clickRenewals();

	}

	@Test(priority = 2)
	public void upgradeRenewalFirstpage() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.renewalFirstpage();
	}

	@Test(priority = 3)
	public void upgradeRenewaldashboard() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.renewalFirstpagenext();
	}

	@Test(priority = 4)
	public void upgraautoRenewalAddons() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.upgraautoRenewalAddons();
	}

	@Test(priority = 5)
	public void upgraautoRenewalTenure() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.upgraautoRenewalTenure();

	}

	@Test(priority = 6)
	public void upgraautoRenewalInsureddetails() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.upgraautoRenewalInsureddetails();

	}

	@Test(priority = 7)
	public void upgraautoRenewalproposalsummary() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.upgraautoRenewalMedicalhistorysubmit();
		WebElement ele = driver.findElement(By.xpath("//div[@class='col-md-6 holder_details'][1]/div[1]/div[1]/p[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));
		
	}
	@Test(priority = 8)
	public void upgraautoRenewalPayment() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.Renewnewpolicy();
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.gateway();
		
	}
	
	@Test(priority = 9)
	public void upgraautoRenewalThankyouTest() {
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();

	}

	
/*	@Test(priority = 8)
	public void upgraautoRenewalPayment() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.upgraautoRenewalproposalsummary();

	}*/
	
}




/*	@Test(priority = 1)
	public void Continue_With_Existing_Sum_Insured() throws Throwable {
		BaseClass b = PageFactory.initElements(BaseClass.driver, BaseClass.class);
		b.launchBrowser();
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		log.debug(
				"**************Automating the script for 'Continue With Existing Sum Insured' Module*********************");
		re.clickRenewals();
		re.renewalFirstpage();
		re.renewalFirstpagenext();
		re.siauto();
	}
}*/

/*	@Test(priority = 3)
	public void Quick_Renew() throws Throwable {
		BaseClass b = PageFactory.initElements(BaseClass.driver, BaseClass.class);
		b.launchBrowser();
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		log.debug("**************Automating the script for 'Quick Renew' Module*********************");
		re.Clickrenewals();
		re.Renewaldetailpage();
		re.Renewpage();

		//
	}*/