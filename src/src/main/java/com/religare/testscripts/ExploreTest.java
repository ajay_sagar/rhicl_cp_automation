package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.ExploreClick;
import com.religare.objectrepository.ExploreFirstPage;
import com.religare.objectrepository.MedicalHistoryPage;
import com.religare.objectrepository.PayU;

@Listeners(com.religare.genericlib.SampleListener.class)

public class ExploreTest extends BaseClass {
	Logger log = Logger.getLogger("devpinoyLogger");
	ExcelLib elib = new ExcelLib();

	WebDriverCommonLib clib = new WebDriverCommonLib();

	@Test(priority = 1)

	public void clickOnexploreTest() throws IOException {
		// clib.Passscreenshot("Religarehome", "ReligareHomePage");
		ExploreClick home = PageFactory.initElements(BaseClass.driver, ExploreClick.class);

		home.moveToExplore();
		log.debug("***************Automating the Explore Module*****************");
		log.debug("Clicking on the Explore Module");

	}

	@Test(priority = 2)
	public void exploreFirstPageTest() throws Throwable {

		clib.waitForPageToLoad();

		ExploreFirstPage exp = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);

		exp.slider();

		exp.multiTripType();

		exp.month();

		Thread.sleep(1500);

		exp.day();

		Thread.sleep(1500);

		exp.totalMember();

		exp.ageOfMember1();

		try {

			exp.ageOfMember2();

			exp.ageOfMember3();

			exp.ageOfMember4();

			exp.ageOfMember5();

			exp.ageOfMember6();
		}

		catch (Exception e) {

			e.getMessage();
		}

		String number = elib.getExcelData("testSpecificData", 0, 1);
		;

		exp.phoneNumber(number);

		Thread.sleep(2000);

		exp.pedRadioBtn();

		try {

			exp.premiumBtn();

			Alert al = BaseClass.driver.switchTo().alert();

			al.accept();
		}

		catch (Exception e) {

			e.getMessage();

		}

	}

	// exp.agreeCheckBox();

	// exp.agreeCheckBox();

	// clib.Passscreenshot("ExploreFirst", "ExploreFirstPage");
	@Test(priority = 3)
	public void exploreProposerdetailsTest() throws Throwable {

		ExploreFirstPage exp = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);

		Thread.sleep(2000);

		try {

			exp.careBuyBtn();

			Alert al = BaseClass.driver.switchTo().alert();

			al.accept();

			Thread.sleep(2000);

			clib.waitForPageToLoad();

			exp.careBuyBtn();
		}

		catch (Exception e) {

			e.getMessage();
		}
		log.debug("Clicking on the Buy Now button");

		clib.waitForPageToLoad();

		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		pdp.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		Thread.sleep(1500);

		pdp.fillDetailsAddress();

		// Thread.sleep(3500);

		Thread.sleep(1500);

		try {

			pdp.panCard();

		} catch (NoSuchElementException e) {

			e.getMessage();

		}
		Thread.sleep(1500);

	}

	// clib.Passscreenshot("ExploreProposer's", "ExploreProposer'sPage");

	@Test(priority = 4)
	public void exploreInsureddetailsTest() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		pdp.nextClick();

		Thread.sleep(7000);

		FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);

		idp.insuredMember1();

		try {

			idp.insuredMember2();
			idp.insuredMember3();
			idp.insuredMember4();
			idp.insuredMember5();
			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

		clib.waitForPageToLoad();

	//	clib.Passscreenshot("ExploreInsured", "ExploreInsuredPage");
		idp.next();

		Thread.sleep(2500);

		try {

			BaseClass.driver.findElement(By.id("premiumPopupOk")).click();
		}

		catch (Exception e) {

			e.getMessage();
		}

		clib.waitForPageToLoad();

	}

	@Test(priority = 5)
	public void exploreMedicalhistoryTest() throws Throwable {

		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);

		String totalMember = elib.getExcelData("testSpecificData", 8, 1);

		if (totalMember.equalsIgnoreCase("one")) {

			Thread.sleep(3000);

			emh.medHist();
		}

		else if (totalMember.equalsIgnoreCase("two")) {

			clib.waitForPageToLoad();

			Thread.sleep(10000);

			emh.preExistingDiseaseYesMember();
			;

			emh.diagnosedAndhospitalizedMember1();

			emh.diagnosedAndhospitalizedMember2();

			emh.travelPloicyMember1();

			emh.travelPloicyMember2();

			emh.next();
		}

		else if (totalMember.equalsIgnoreCase("three")) {

			emh.medHist();

		} else if (totalMember.equalsIgnoreCase("four")) {

			emh.medHist();
		} else if (totalMember.equalsIgnoreCase("five")) {

			emh.medHist();
		} else if (totalMember.equalsIgnoreCase("six")) {

			emh.medHist();
		}
	}

	@Test(priority = 6)
	public void exploreProposalsummaryTest() throws Throwable {

		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		emh.exploreProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));
	}

	@Test(priority = 7)
	public void explorePaymentgatewayTest() throws Throwable {
		Thread.sleep(5000);

		clib.waitForPageToLoad();
		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);

		emh.exploreProceedtoPay();
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.gateway();

	}

	@Test(priority = 8)
	public void exploreThankyouTest() throws Throwable {
		PayU pay = PageFactory.initElements(BaseClass.driver, PayU.class);
		pay.thankyou();


	}

}
