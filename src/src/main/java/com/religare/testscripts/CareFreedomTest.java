package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.CareFreedomClick;
import com.religare.objectrepository.CarefreedomPoliPlan;
import com.religare.objectrepository.InsuredDetailsPage;
import com.religare.objectrepository.MedistPage;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;

@Listeners(com.religare.genericlib.SampleListener.class)

public class CareFreedomTest extends BaseClass {

	WebDriverCommonLib clib = new WebDriverCommonLib();

	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)

	public void moveToCareFreedomTest() throws Throwable {

		// clib.Passscreenshot("Religarehome", "ReligareHomePage");

		CareFreedomClick click = PageFactory.initElements(BaseClass.driver, CareFreedomClick.class);

		click.clickOnCareFreedom();
		log.debug("***************Automating the CareFreedom Module*****************");
		log.debug("Clicking on the Carefreedom Module");

		clib.waitForPageToLoad();

		CarefreedomPoliPlan plan = PageFactory.initElements(BaseClass.driver, CarefreedomPoliPlan.class);

		clib.waitForPageToLoad();

		Thread.sleep(5000);

		plan.TotalMembersAge();

		try {

			plan.mobile();

			Alert al = BaseClass.driver.switchTo().alert();

			WebDriverWait wait = new WebDriverWait(BaseClass.driver, 20);

			wait.until(ExpectedConditions.alertIsPresent());

			al.accept();
		}

		catch (Exception e) {
			e.getMessage();
		}

		plan.slider();

		plan.tensure();

		// plan.totalpremium();

		/*Thread.sleep(6000);

		clib.waitForPageToLoad();*/

		// clib.Passscreenshot("CareFreedomFirst", "CareFreedomFirstPage");

		// plan.buy();

	}

	// plan.buy();
	// }
	//

	@Test(priority = 2)

	public void carefreedomfillProposorsTest() throws Throwable {

		CarefreedomPoliPlan plan = PageFactory.initElements(BaseClass.driver, CarefreedomPoliPlan.class);
		
		Thread.sleep(6000);

		clib.waitForPageToLoad();
		plan.buy();

		clib.waitForPageToLoad();

		//Thread.sleep(3000);

		com.religare.genericlib.FillDetailsExplore fdx = PageFactory.initElements(BaseClass.driver,
				com.religare.genericlib.FillDetailsExplore.class);

		fdx.fillDetailsGenderNameDateEmailNomineeNameAndNomineeRelation();

		fdx.fillDetailsAddress();

		Thread.sleep(3500);

		try {

			fdx.panCard();

		} catch (NoSuchElementException e) {

			e.getMessage();

		}

		// clib.Passscreenshot("CareFreedomProposer's", "CareFreedomProposer'sPage");

		/*
		 * fdx.nextClick();
		 * 
		 * clib.waitForPageToLoad();
		 */

	}

	@Test(priority = 3)
	public void carefreedomInsureddetailsTest() throws Throwable {

		com.religare.genericlib.FillDetailsExplore fdx = PageFactory.initElements(BaseClass.driver,
				com.religare.genericlib.FillDetailsExplore.class);

		fdx.nextClick();

		clib.waitForPageToLoad();

		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);

		idp.insuredMember1();

		try {

			clib.waitForPageToLoad();
			idp.insuredMember2();
			clib.waitForPageToLoad();
			clib.waitForPageToLoad();
			clib.waitForPageToLoad();
			idp.insuredMember3();
			clib.waitForPageToLoad();
			idp.insuredMember4();
			clib.waitForPageToLoad();
			idp.insuredMember5();
			clib.waitForPageToLoad();
			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

		// clib.Passscreenshot("CareFreedomInsured", "CareFreedomInsuredPage");
	/*	idp.next();

		clib.waitForPageToLoad();

		//
		// @Test
		//
		// public void medHist() throws Throwable {
		//
		clib.waitForPageToLoad();*/

	}

	@Test(priority = 4)
	public void carefreedomMedicalhistoryTest() throws Throwable {
		
		InsuredDetailsPage idp = PageFactory.initElements(BaseClass.driver, InsuredDetailsPage.class);
		
		idp.next();

		clib.waitForPageToLoad();
		
		clib.waitForPageToLoad();

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		clib.waitForPageToLoad();

		Thread.sleep(4000);

		mp.carefreedomMedicalhistory();

	}

	@Test(priority = 5)
	public void carefreedomProposalsummaryTest() throws InterruptedException {

		MedistPage mp = PageFactory.initElements(BaseClass.driver, MedistPage.class);

		mp.carefreedomProposalsummary();
		WebElement ele = driver.findElement(By.xpath("(//div[@class='applicationNoTable'])[2]/table/tbody/tr[1]/th[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));

	}

	@Test(priority = 6)
	public void carefreedomPaymentGatewayTest() throws InterruptedException {
		Payment payment = PageFactory.initElements(BaseClass.driver, Payment.class);
		//
		// payment.ApplicationNocarecarefreedom();
		//
		// payment.Totalpremiumcarefreedom();

		payment.proceedCareFreedom();

		clib.waitForPageToLoad();

		Thread.sleep(3000);

	 PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
	//
	 payu.gateway();
	//

	 }


	@Test(priority = 7)
	public void carefreedomThankyoutTest() {

		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();
	}

}
