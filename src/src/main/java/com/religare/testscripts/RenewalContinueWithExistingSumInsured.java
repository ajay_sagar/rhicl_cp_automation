package com.religare.testscripts;

import static org.testng.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.PayU;
import com.religare.objectrepository.Payment;
import com.religare.objectrepository.RenewalMethods;
@Listeners(com.religare.genericlib.SampleListener.class)

public class RenewalContinueWithExistingSumInsured extends BaseClass{
	WebDriverCommonLib clib = new WebDriverCommonLib();

	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)
	public void Continue_With_Existing_Sum_InsuredRenewalFirstPage() throws Throwable {
		//BaseClass b = PageFactory.initElements(BaseClass.driver, BaseClass.class);
	//	b.launchBrowser();
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		log.debug(
				"**************Automating the script for 'Continue With Existing Sum Insured' Module*********************");
		re.clickRenewals();
		re.renewalFirstpage();
		//re.renewalFirstpagenext();
		//re.siauto();*/
	}
	
	@Test(priority = 2)
	public void Continue_With_Existing_Sum_InsuredRenewaldashboard() throws Throwable {
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
	    re.renewalFirstpagenext();
	}
	
	@Test(priority = 3)
	public void Continue_With_Existing_Sum_InsuredRenewalAddon() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.continueSIautoRenewal();
		re.continueAddonautoRenewal();
	}
	
	@Test(priority = 4)
	public void Continue_With_Existing_Sum_InsuredRenewalTenure() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.continueTenureautoRenewal();
	}
	@Test(priority = 5)
	public void Continue_With_Existing_Sum_InsuredRenewalInsureddetails() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.continueInsureddetailsautoRenewal();
	}
	
	@Test(priority = 6)
	public void Continue_With_Existing_Proposalsummary() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
	    re.continueautoRenewalMedicalhistorysubmit();
		WebElement ele = driver.findElement(By.xpath("//div[@class='col-md-6 holder_details'][1]/div[1]/div[1]/p[1]"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Application No.";
		assertTrue(str.equals(Expectedresult));
	
	}
	
	@Test(priority = 7)
	public void Continue_With_Existing_Sum_InsuredRenewalPayment() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.Renewnewpolicy();
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.gateway();
		
	}
	
	@Test(priority = 8)
	public void Continue_With_Existing_Sum_InsuredRenewalThankyouTest() {
		PayU payu = PageFactory.initElements(BaseClass.driver, PayU.class);
		payu.thankyou();

	}
}


	public void Continue_With_Existing_Sum_InsuredRenewalPayment() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.continueautoRenewalpayment();
	
	}
	
	/*@Test(priority = 7)
	public void Continue_With_Existing_Sum_InsuredRenewalPayment() throws Throwable {
		
		RenewalMethods re = PageFactory.initElements(BaseClass.driver, RenewalMethods.class);
		re.payment();
}*/
	
}


