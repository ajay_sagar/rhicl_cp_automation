package com.religare.testscripts;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.WebDriverCommonLib;
import com.religare.objectrepository.PortabilityMethod;
import com.religare.objectrepository.Portabilityform;

@Listeners(com.religare.genericlib.SampleListener.class)

public class PortabilityCare extends BaseClass {

	WebDriverCommonLib clib = new WebDriverCommonLib();
	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority = 1)

	public void verifyportabilityHomepage() throws Throwable {

		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
		log.debug("***************Automating the portability Carefreedom module*****************");

		portableobject.Portabilitypage();

		portableobject.verifyportableHome();

	}

	@Test(priority = 2)

	public void verifyFAQ() throws InterruptedException, IOException, AWTException {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.verifyFaQClickable();

		portableobject.verifyFAQslinkonfooterofPortability();

		portableobject.verifyquestionnairesonFAQs();

	}

	@Test(priority = 3)
	public void verifyheaderelement() throws AWTException, InterruptedException {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.verifyheaderelement1();

	}

	/*
	 * @Test(priority = 5 ,enabled=false) public void Portabilitydashboard() throws
	 * InterruptedException { PortabilityMethod portableobject =
	 * PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
	 * 
	 * 
	 * }
	 */

	@Test(priority = 4)
	public void careportabilityfillquotebox1() throws Throwable {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);
		portableobject.carepolicy();
		Thread.sleep(3000);

		portableobject.totalMember();
		// portableobject.verifyheaders();
		Thread.sleep(3000);

		portableobject.defaultfeatures();

		portableobject.verifyCovertypecare();

		portableobject.verifyMemberdetails();
		portableobject.verifyMobilenumber();
		portableobject.verifyRelation();
		try {
			portableobject.Member1forcare();
			Thread.sleep(2000);

			portableobject.Member2forcare();
			Thread.sleep(1500);

			portableobject.Member3();

			Thread.sleep(1500);

			portableobject.Member4();

			Thread.sleep(1500);

			portableobject.Member5();

			Thread.sleep(1500);

			portableobject.Member6();

		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Test(priority = 5)
	public void careportabilityfillquotebox2() throws Throwable {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.findreligarepremium();

		portableobject.verifySIforcare();

		portableobject.verifytenureaddonspremiumtotalpremiumforcare();

		portableobject.premium();

	}

	@Test(priority = 6)
	public void carefillproposerdetails() throws Throwable {
		PortabilityMethod portableobject = PageFactory.initElements(BaseClass.driver, PortabilityMethod.class);

		portableobject.portnow();

		Thread.sleep(5000);

		portableobject.verifyproceedtobuttonredirection();
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.fillproposersdetails();

	}
	@Test(priority = 7)
	public void carefillexistinginsurancepolicydetails() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.proposersdetailssave();
		Thread.sleep(2000);

		try {

			portableform.existinginsurancePolicyCaresinglemember();
			Thread.sleep(1000);
			portableform.existinginsurancePolicyCaremembertwo();
			Thread.sleep(1000);

		portableform.existinginsurancePolicyCaresinglemember();

		} catch (Exception e) {
			e.getMessage();

		}

	}

	// }

	@Test(priority = 8)
	public void carefreedompreviousinsurancepolicy() throws Throwable {

		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.existinginsurancePolicySave();

		portableform.previousinsurancepolicyforcare();

	}

	@Test(priority = 9)
	public void carefreedompreviouspolicy() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.previousinsurancepolicysave();

		portableform.previousinsurancepolicyforcare();
	}

	@Test(priority = 10)
	public void carefreedomrenewalnotice() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.previouspolicysave();

		portableform.Renewalnotice();

	}

	@Test(priority = 11)
	public void careclaim() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.renewalnoticesave();

		portableform.Claims();

	}

	@Test(priority = 12)
	public void careMedicalhistory() throws InterruptedException, IOException, AWTException {

		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.claimsave();

		portableform.Next();

		clib.waitForPageToLoad();

		portableform.Healthquestions();

	}

	@Test(priority = 13)
	public void careProposalsummary1() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);

		portableform.SubmitHealthquestions();

		portableform.SubmitPolicy();
		WebElement ele = driver.findElement(By.xpath("//div[@class='col-md-3 proposer_fields'][1]/label"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Total Members";
		assertTrue(str.equals(Expectedresult));
		portableform.Proposalsummary();

	}

	@Test(priority = 14)
	public void careProposalsummary2() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.confirmPolicy();
		WebElement ele = driver.findElement(By.xpath("//div[@class='col-md-3 proposer_fields'][1]/label"));
		String str = ele.getText();
		System.out.println(str);
		String Expectedresult = "Total Members";
		assertTrue(str.equals(Expectedresult));

		clib.waitForPageToLoad();

	}

	@Test(priority = 15)
	public void carePayment() throws Throwable {
		Portabilityform portableform = PageFactory.initElements(BaseClass.driver, Portabilityform.class);
		portableform.Proceedtopay();
	}

}


