
package com.religare.testscripts;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.religare.genericlib.BaseClass;
import com.religare.genericlib.ExcelLib;
import com.religare.genericlib.FillDetailsExplore;
import com.religare.genericlib.WebDriverCommonLib;

import com.religare.objectrepository.ExploreClick;
import com.religare.objectrepository.ExploreFirstPage;
import com.religare.objectrepository.MedicalHistoryPage;
import com.religare.objectrepository.PayU;

@Listeners(com.religare.genericlib.SampleListener.class)
public class TestExploreSaveandContinue extends BaseClass {
	WebDriverCommonLib clib = new WebDriverCommonLib();
	ExcelLib elib = new ExcelLib();
	Logger log = Logger.getLogger("devpinoyLogger");

	@Test(priority=1)
	public void ExploreFirstpage() throws Throwable {
	
		ExploreClick click = PageFactory.initElements(BaseClass.driver, ExploreClick.class);
		click.moveToExplore();
		log.debug("Clicking on the Explore Module");
		
		ExploreFirstPage afp = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		afp.getCurrentURLAssure();
		afp.VerifyTripType();
		afp.verifyStartdate();
		afp.verifyenddate();
		afp.verifyAgeoftravel();
		afp.verifyTotalmember();
		afp.verifymobile();
		afp.VerifyRegion();
		afp.verifyslider();
		//((JavascriptExecutor) driver).executeScript("scroll(0,500);");
		Thread.sleep(3500);
		afp.verifyRadiobutton();
		afp.verifyTermandconditioncheckbox();
		afp.verifyGetquoteandbuy();
		afp.verifyingquotepagewithouttermsandcondition();	
		afp.verifyingquotepagewithoutmobilenumber();
		afp.verifyingquotepagewithoutStrtdate();
		
		//String tripType = afp.getTripType();
		//String travelingTo = afp.getTravelingTo();
		
		
		
		/*if (tripType.equals("Single") && (travelingTo.equals("Asia") || travelingTo.equals("Africa"))) {
			afp.verifyingquotepagewithoutenddate();
			}else {}*/
		
		afp.VerifyingBuyNowbuttonisclickable();
		afp.verifyfilldetailpage();
		
	}
	
	@Test(priority=2)
	public void Exploreeditquotepage() throws Throwable {
		ExploreFirstPage afp = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		//afp.verifyeditquotepage();
		
		
	}
	@Test(priority=3)
	public void FillDetailsExplore() throws Throwable {
		FillDetailsExplore pdp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
		//pdp.verifyErrorMessage();
		pdp.isInputPresentInProposalDetailsPage();
		pdp.verifyRadioBtn();
		pdp.verifyFirstName();
		pdp.verifyLastName();
		pdp.verifyMobileNumber();
		pdp.clearMobileNumberDataAndReset();
		pdp.verifyDOB();
		pdp.verifyDOBSelect();
		pdp.verifyEmailAddress();
		pdp.verifyNomineeTextbox();
		pdp.valueOfDropDownAndIsClickable();
		pdp.verifyRelationDropDown();
		pdp.verifFillAddressOne();
		pdp.verifFillAddressTwo();
		pdp.verifyLandmark();
		pdp.verifyPinCode();
		clib.waitForPageToLoad();
		Thread.sleep(3000);
		pdp.verifyStateAutoPinCode();
		clib.waitForPageToLoad();
		Thread.sleep(5000);
		pdp.panCard();
		pdp.citySelect();
		pdp.submitBTN();

		clib.waitForPageToLoad();
		//clib.waitForPageToLoad();
		pdp.verifyinsureddetailpage();
	}
		@Test(priority = 4)
		public void verifyingeachfieldininsureddetailpage() throws InterruptedException, IOException {
			ExploreFirstPage edit = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
			//edit.verifyeditquotepage();
			
			FillDetailsExplore ins = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
			ins.verifyinsuredmember1();
			try {
			ins.verifyinsuredmember2();
			ins.verifyinsuredmember3();
			ins.verifyinsuredmember4();
			ins.verifyinsuredmember5();
			ins.verifyinsuredmember6();
		}catch(Exception e) {}
		
		}
		@Test(priority = 5)
		public void errormessageInsureddetailpage() throws IOException, InterruptedException{
			FillDetailsExplore fill = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
			fill.next();
			fill.verifyerrormessageinsureddetailpage();
			fill.verifyerrorforinvalidnamedobpassport();
	
		}
   @Test(priority =6)
     public void fillinsuredetailspage() throws Throwable {
	   FillDetailsExplore idp = PageFactory.initElements(BaseClass.driver, FillDetailsExplore.class);
	   idp.insuredMember1();

		try {

			idp.insuredMember2();
			idp.insuredMember3();
			idp.insuredMember4();
			idp.insuredMember5();
			idp.insuredMember6();

		}

		catch (Exception e) {

			e.getMessage();
		}

		clib.waitForPageToLoad();

		idp.next();
		clib.waitForPageToLoad();
		//clib.waitForPageToLoad();
		idp.verifyredirectingtomedicalpage();
		ExploreFirstPage edit = PageFactory.initElements(BaseClass.driver, ExploreFirstPage.class);
		edit.verifyeditquotepage();
   }
   @Test(priority =7)
   public void verifymedicalpage() throws Throwable {
	
		MedicalHistoryPage emh = PageFactory.initElements(BaseClass.driver, MedicalHistoryPage.class);
		Thread.sleep(3000);
		emh.verifyHealthQuestionnaire();
		emh.verifydisclaimerpoints();
		emh.verifybuttonsinmedicalpage();
		emh.medHist();
		emh.savemedical();
		emh.verifyemailpopup();
		emh.verifyemailidtextfield();
		emh.verifysndemail();
        emh.confirmmsg();

		
		
	
}}

		
	
	

